<?php

/*
 * Creado por luisda 28-09-2020
 */


include_once __DIR__.'/utilitarios_Class.php';     //para saber que pedo con los __Dir__ ver el archivo config.php, ese no lo utilizo pero vienen los link del proque es una buena idea usar __Dir__ en el include

class base_de_datos_Class{
    
    //hellooouuu motherfuker!!!
    
    
    protected $conexion;

    
    public function __construct() {
        
        try {
            $this->conexion = new PDO('mysql:host=127.0.0.1;dbname=kaliope_pedidos', 'kaliope_kaliope', 'ja2408922007');
            
        } catch (Exception $exc) {
            echo "Error".$exc->getMessage();
        }

        
        
    }
    
    public function getConexion(){
        return $this->conexion;
    }
    
    
    /**
     * 
     * @return \PDO
     */
    private function crearConexionAdmin(){
        try {
           return new PDO('mysql:host=127.0.0.1;dbname=kaliopec_kaliope', 'kaliopec_kaliope', 'ja2408922007');
            
        } catch (Exception $exc) {
            echo "Error".$exc->getMessage();
        }
        
    }
    
    
    
    
    /**
     * Valida si un usuario existe en nuestra base de datos
     * y ademas sus password es correcto
     * 
     * @param string $usuario <p> 
     * @param string $password <p>
     * 
     * @return bool True si el acceso es completo <p>
     *          exception si no se encuentra usuario u contraseña no coinciden<p>
     * 
     * 
     */
    public function usuarios_comprobarCredenciales($usuario,$password){
        
        $consultaUsuarios = $this->conexion->prepare("SELECT * FROM acceso_clientes WHERE usuario=?");
        $consultaUsuarios->execute([$usuario]);
        $usuarios = $consultaUsuarios->fetchAll();
        
        //print_r($usuarios);
        
        
        if(empty($usuarios)){
            throw new Exception("No existe ningun usuario registrado como $usuario");
        }else{
            $temporal = $usuarios[0]['password'];
            if(strcasecmp($temporal,$password)===0){                              
                return true;
                
            }else{
                throw new Exception("La contraseña para el usuario $usuario es incorrecta");
            }
            
        }
        
               
        
    }
    
    /**
     * 
     * @param type $usuario
     * @return int El numero de cuenta del usuario validado en kaliope Admin<p>
     *          si el metodo no encuentra el usuario, o el numero de cuenta no<p>
     *          existe en kaliope admin entonces retorna Exepciones
     *      
     * @throws Exception 
     */
    public function usuarios_consultarComprobarNumeroCuenta($usuario){
        /*
         * Consultaremos y devolveremos el numero de cuenta que coincida con el 
         * usuario y que tambien a su ves el numero de cuenta exista con la base de datos
         * de kaliope admin
         * 
         */
        
        $consulta = $this->conexion->prepare("SELECT no_cuenta FROM acceso_clientes WHERE usuario=?");
        $consulta->execute([$usuario]);
        $numeroCuenta = $consulta->fetchAll(PDO::FETCH_ASSOC);
        //print_r($numeroCuenta);
        
        if(!empty($numeroCuenta)){
            $numeroCuenta = $numeroCuenta[0]["no_cuenta"];      //saccamos del array bidimencional el numero de cuenta
            
            $conexionAdmin = $this->crearConexionAdmin()->prepare("SELECT no_cuenta FROM cuentas WHERE no_cuenta=?");
            $conexionAdmin->execute([$numeroCuenta]);
            $resultadoAdmin = $conexionAdmin->fetchAll();
            
            if(!empty($resultadoAdmin)){
                return $numeroCuenta;
            }else{
                throw new Exception("Error no se encuentra el numero de cuenta $numeroCuenta del usuario $usuario en base de datos servidor KaliopeAdmin");
            }
            
            
            
        }else{
            throw new Exception("Error no se encuentra usuario $usuario en base de datos servidor Kaliope.mx");
        }
           
                
    }
    
    /**
     * SE DEBE LLAMAR CUANDO YA SE HAYAN VALIDADO LAS CREDENCIALES
     * Guardara las variables necesarias para el inicio de sesion
     * @param string $usuario el usuario a que esta iniciando sesion
     * @param boolean $mantenerSesion true si el usuario quiere mantener la sesion iniciada en ese dispositivo <p> false si no
     * @param string $dispositivo el nombre del dispositivo por ejemplo samsung j4 o mozillaFirefox
     * @param string $uuid el numero unico de instancia almacenado en la app kaliope
     * @param string $fechaHora en formato que sql entienda yyyy-mm-dd hh:mm:ss
     * @return boolean false si el usuario esta iniciando en un dispositivo con uuid en donde anteriormente no ha iniciado sesion <p>
     *                  true si el usuario inicio sesion en un dispositivo antes registrado, en ese momento se actualizara la hora del inicio de sesion 
     *                      y ademas si el usuario eligio mantener sesion iniciada entonces actualizara ese campo
     */    
    public function usuarios_actualizarInfoInicioSesion($usuario,$mantenerSesion,$dispositivo, $uuid, $fechaHora){
        /*
         * Primero valoramos si el dispositvo por el que inicio sesion esta registrado ya en la base de datos
         * si se encuentra un dispositivo enotnces se preguntara si se desea mantener sesion
         * esto para que si el usuario ya no desea mantener la sesion abierta entonces se elimine de la tabla el dispositivo
         * y si la desea mantener entonces soloa ctualizamos el dispositivo
         * 
         * si no se encuentra el dispositivo en la tabla de sesiones entonces veremos si
         * el usuario quiere mantener la sesion de ser asi entonces añadimos el dispositivo a la tabla
         * si no la desea mantener entonces solo damos acceso
         * 
         */
        
        $sql = "SELECT * FROM usuarios_sesiones WHERE usuario=? AND dispositivo=? AND uuid=?";
        $consulta = $this->conexion->prepare($sql);
        $consulta->execute(array($usuario,$dispositivo,$uuid));
        $resultado = $consulta->fetchAll();
        
        if(!empty($resultado)){
            //si se encontro ya un dispositivo en la tabla sesiones, permitimos el paso y 
            
            
            if($mantenerSesion){
                //actualizamos los datos de la tabla solo si el usuario desea mantener la sesion iniciada
                $sql = "UPDATE usuarios_sesiones SET fecha_hora=?, mantener=? WHERE usuario=?";
                $this->conexion->prepare($sql)->execute(array($fechaHora, $mantenerSesion, $usuario));
            
                //echo "se ha encontrado un dispositivo con el mismo modelo y uuid, se han actualizado los datos";
            }else{
                //si ya no desea mantener la sesion en ese dispositivo entonces eliminamos el dispositivo de la tabla
                $sql = "DELETE FROM usuarios_sesiones WHERE usuario=? AND dispositivo=? AND uuid=?";
                $this->conexion->prepare($sql)->execute([$usuario,$dispositivo,$uuid]);
                //echo"Se ha encontrado un dispositivo con el mismo modelo y uuid pero el usuario ya no desea mantener la sesion iniciada en este dispositivo, eliminando dispositivo";
            }           
            
           
            
            
        }else if($mantenerSesion){
            //si no se encuentra dispositivo entonces comprobamos
            //si el usuario desea mantener la sesion en este dispositivo
            //de ser asi entonces agregamos ese dispositivo a la tabla sesiones
            //de no ser asi solo permitimos el acceso
            //
            //si el usuario desea mantener la sesion entonces agregamos a la tabla el dispositivo
                $sql = "INSERT INTO usuarios_sesiones (usuario, dispositivo, uuid, fecha_hora, mantener) VALUES (?,?,?,?,?)";
                $this->conexion->prepare($sql)->execute(array($usuario, $dispositivo, $uuid, $fechaHora, $mantenerSesion));
                //echo "no se encontro un dispositivo igual pero se desea mantener la sesion en este dispositivo, agregando a la tabla";
            
        }else{
            //si el usuario no desea mantener la sesion entonces damos acceso
            //echo "No se encontro el dispositivo pero tampoco el usuario desea mantener la sesion entonces no añadimos el dispositivo a la tabla";
        }
        
        
        
    }
    /**
     * si el movil esta preguntando si hay alguna sesion iniciada con este dispositivo dejaremos entrar si pedir credenciales
     * 
     * @param string $usuario
     * @param string $uuid
     * @param string $fechaHora
     * @return boolean true si se encontro ese codigo de dispositivo y usuario que solisito mantener la sesion iniciada, false si no
     */
    public function usuarios_comprobarMantenerSesion(string $usuario, string $uuid, string $fechaHora, string $dispositivo) {

        $consulta = $this->conexion->prepare("SELECT * FROM usuarios_sesiones WHERE uuid=? AND usuario=? AND dispositivo=?");

        $consulta->execute([$uuid, $usuario, $dispositivo]);
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        //print_r($resultado);
        /*
         Array
            (
                [id] => 1
                [usuario] => PAQUITA
                [dispositivo] => sdk_gphone_x86_arm
                [uuid] => 15670ac8-824c-4eab-ab6e-b97b338a69fa
                [fecha_hora] => 2021-04-12 18:25:03
                [mantener] => 1
            )
         */

        if (!empty($resultado)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function usuarios_actualizarHoraSesion(string $uuid, string $fechaHora){
        $statement = $this->conexion->prepare("UPDATE usuarios_sesiones SET fecha_hora=? WHERE uuid=?");
        $statement->execute([$fechaHora, $uuid]);
    }
    
    public function usuarios_eliminarSesionIniciada (string $usuario, string $uuid){
        $statement = $this->conexion->prepare("DELETE FROM usuarios_sesiones WHERE uuid=? AND usuario=?");       
        $statement->execute([$uuid,$usuario]);
    }

    /**
     * Retornamos cual es la cantidad de apoyo o de financiacion que kaliope dara a un cliente en particular
     * @param string $cuenta
     * @return array    <p>[porcentaje_apoyo_empresa] => 0.5
                        <p>[porcentaje_pago_cliente] => 0.5
     */
    public function usuarios_consultar_porcentaje_financiacion (string $cuenta){
        $consulta = $this->conexion->prepare("SELECT porcentaje_apoyo_empresa, porcentaje_pago_cliente FROM acceso_clientes WHERE no_cuenta=?");
        $consulta->execute([$cuenta]);
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        //print_r($resultado);
        /*
         * Array
            (
                [porcentaje_apoyo_empresa] => 0.5
                [porcentaje_pago_cliente] => 0.5
            )
         */
        
      
        return $resultado;
        
    }
    
    /**
     * Consulta los movimientos obtenemos credito dias, fecha futura y los retorna como array 
     * es decir del servidor kaliopeAdmin tabla movimientos obtenemos la ultima fecha futura en el sistema de esta clienta
     * @param int $numeroCuenta
     * @return array <p>nombre<p>zona<p>fecha<p>grado<p>credito<p>dias<p>puntos_disponibles
     * 
     */
    public function clientes_consultarCliente($numeroCuenta){
        
        
        $consulta = $this->crearConexionAdmin()->prepare("SELECT nombre, zona, fecha, grado, credito, dias, puntos_disponibles FROM movimientos WHERE cuenta=? ORDER BY folio DESC LIMIT 1");
        $consulta->execute([$numeroCuenta]);       
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        //print_r($resultado);
        /*
         Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020                   Es la fecha de visita siguiente a menos que se cree una fecha futura como el cachorrito sabe hacerlo xD
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
            )
         */
      
        if(empty($resultado)){echo "no se encontro la cuenta: $numeroCuenta en Kaliope Movimientos\n";}
        
        return $resultado;
    }
    
    /**
     * Lo usamos para que sea entregado a la app
     * Consulta los movimientos obtenemos credito dias, fecha futura, fecha cierre pedido etc del cliente y los retorna como array 
     * retorna tambien su fecha de cierre de pedido
     * <p>[fecha_cierre_pedido] la fecha futura visita (-) dias antes pedido que estan en la tabla de zonas
     * apartir de esta fecha ya no se deberia poder agregar piezas a un pedido y se deberia agregar a otro nuevo
     * @param int $numeroCuenta
     * @return array <p>[nombre]<p>[zona]<p>[fecha] de visita ultima encontrada en movimientos<p>[grado]<p>[credito]<p>[dias]<p>[puntos_disponibles]<p>[fecha_cierre_pedido] la fecha futura visita (-) dias antes pedido que estan en la tabla de zonas<p>
     * 
     */
    public function clientes_consultarClienteConFechaCierre($numeroCuenta){
        
       
        $resultado = $this->clientes_consultarCliente($numeroCuenta);
        //print_r($resultado);
        /*
         Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020                   Es la fecha de visita siguiente a menos que se cree una fecha futura como el cachorrito sabe hacerlo xD
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
            )
         */
        
        $fechaVisita = $resultado["fecha"];
        $fechaCierre = $this->clientes_consultarFechaCierrePedido($numeroCuenta);
        $resultado["fecha_cierre_pedido"] = $fechaCierre;
        $resultado["mensaje_cierre_pedido"]= utilitarios_Class::calcularMensajeCierrePedidoCliente($fechaCierre,$fechaVisita);
        /*
         * Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020                   Es la fecha de visita siguiente a menos que se cree una fecha futura como el cachorrito sabe hacerlo xD
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
                [fecha_cierre_pedido]=>21-08-2021  
                [mensaje_cierre_pedido":"Por ahora no puedes ingresar productos a tu carrito hasta el 26-08-2021, tu pedido fue cerrado el 21-08-2021 para ser entregado el 25-08-2021"
            )
         */
        
        return $resultado;
    }
    
    /**
     * Consultamos de kaliope admin la fecha futura de visita de nuestro cliente
     * esta fecha se obtiene del registro kaliope admin 
     * @param string $numeroCuenta
     * @return string fechaFutura dd-mm-yyyy
     */
    public function clientes_consultarFechaFuturaVisita($numeroCuenta){
            
        $resultado = $this->clientes_consultarCliente($numeroCuenta);
        //print_r($resultado);
        /*
         Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020                   Es la fecha de visita siguiente a menos que se cree una fecha futura como el cachorrito sabe hacerlo xD
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
            )
         */
        
    
        return $resultado['fecha'];
    }
    
    public function clientes_consultarZonaCliente($numeroCuenta){
        $info = $this->clientes_consultarCliente($numeroCuenta);
        
       
        return $info["zona"];
        
    }
    
    /**
     * Te indica hasta que fecha el cliente podra agregar piezas a su carrito, usando la fecha de visita del cliente
     * la fecha de cierre le damos el significado que a partir de esta fecha el cliente ya no puede agregar productos
     * si el cliente es de una zona de queretaro del viernes 23-07 tiene 6 dias de bloqueo entonces la fecha retornada
     * es 17-07-2021, quiere decir que todo lo que añada el cliente antes del 17-07 entrara al sistema
     * se podria decir que es el dia en que se cerrara el pedido del cliente en el almacen. 
     * Es bueno definir su significado de esta fecha devuelta, ya que se puede ver de muchas maneras diferentes. 
     * @param string $numeroCuenta 
     * @return String $fechaCierre      la fecha de visita del cliente - los dias de tolerancia del pedido
     */
    public function clientes_consultarFechaCierrePedido(string $numeroCuenta){
        $fechaFuturaVisita = $this->clientes_consultarFechaFuturaVisita($numeroCuenta);       
        
        return utilitarios_Class::restarDiasFechaString($fechaFuturaVisita, $this->clientes_consultarDiasAntesPedido($numeroCuenta));
    }
    
    
    /**
     * Consulta de la tabla los dias de tolerancia para que el cliente meta pedido antes de su visita
     * @param string $numeroCuenta
     * @return int
     */
    public function clientes_consultarDiasAntesPedido(string $numeroCuenta){
    //por lo pronto retornaremos la variable 4 dias en el futuro se consultara en base a la ruta del cliente
        
        $zona = $this->clientes_consultarZonaCliente($numeroCuenta);   
        $consulta = $this->crearConexionAdmin()->prepare("SELECT dias_antes_pedido FROM nombres_zonas WHERE nombre=?");
        $consulta->execute([$zona]);
        $resultado = $consulta->fetch();
        //print_r($resultado);
        /*
          Array
            (
                [dias_antes_pedido] => 5
                [0] => 5
            )
         */
        
        if(!empty($resultado)){
            return $resultado[0];
        }else{
            return 6;
        }
        
       
        
      
    }
    
        
    /**
     * Consulta los datos personales del cliente en el servidor KaliopeAdmin
     * @param int $numeroCuenta 
     * @return array los datos personales del cliente <p>
     * 
                              Array<p>
                                  (<p>
                                      [no_cuenta] => 1010<p>
                                      [0] => 1010<p>
                                      [nombre] => PAULA GARCIA GARCIA<p>
                                      [1] => PAULA GARCIA GARCIA<p>
                                  )<p>
     * 
     * @throws Exception si no se encuentra el numero de cuenta en la base de datos entonces retornamos una excepcion. <p>
     * no deberia de ocurrir porque eso indicaria una inconcistencia en las bases de datos porque en la de los usuarios
     * tendriamos un numero de cuenta apuntando a un cliente de kaliopeAdmin
     * 
     * 
     */
    public function clientes_consultarDatosCliente ($numeroCuenta){
        $sql = "SELECT * FROM cuentas WHERE no_cuenta=?";
        $consulta = $this->crearConexionAdmin()->prepare($sql);
        $consulta->execute([$numeroCuenta]);
        $resultado = $consulta->fetchAll();
        
        //print_r($resultado[0]);
        
        if(empty($resultado)){
            //si nombre esta vacio entonces devolvemos un array vacio
            throw new Exception("No existen datos del cliente con ese numero de cuenta, error interno del programa");
        }else{
            //si no esta vacio entonces sacamos del array bidimencional y solo retornamos un array con los datos del cliente
            return $resultado[0];
        }
            
        
    }
    
    
    
    /**
     * Para saber el nombre de la sucursal solo necesitamos la zona del cliente
     * en la tabla ya estan asignadas a A1 o Q2 o M3, eso nos dice a que sucursal pertenece
     * @param string $nombreZona
     * @return string $sucursal  ATLACOMULCO, MORELIA, QUERETARO, XXXXX
     */
    public function  sucursal_consultarSucursal(string $nombreZona){
        $statement = $this->crearConexionAdmin()->prepare("SELECT ruta FROM nombres_zonas WHERE nombre=?");
        $statement->execute([$nombreZona]);
        $resultado = $statement->fetch(PDO::FETCH_ASSOC);
        
        //print_r($nombreZona);
        //print_r($resultado);
        
        if(empty($resultado)){return "xxxxx";}
        
        
        $asignado = substr($resultado["ruta"],0,1);     
        //print_r($asignado);
        //A Q M
        
        
        
        switch ($asignado) {
            case "A":

                return "ATLACOMULCO";
                
            
            case "Q":


                return "QUERETARO";
            
            case "M":


                return "MORELIA";

            default:
                return "XXXXXX";
                
        }
        
        
        
    }
    
    /**
     * No encontre otro nombre porque no quiero revolvernos, quiero devolver el A1 A2 Q3 M4 etc
     * en la base de deatos se llama ruta, pero nosotros por ruta en todo el codigo estamos entendiendo
     * el nombre de la zona jeje
     * @param string $nombreZona
     * @return string A1 Q3 M4
     */
    public function  sucursal_consultarAsignado(string $nombreZona){
        $statement = $this->crearConexionAdmin()->prepare("SELECT ruta FROM nombres_zonas WHERE nombre=?");
        $statement->execute([$nombreZona]);
        $resultado = $statement->fetch(PDO::FETCH_ASSOC);
        
        //print_r($nombreZona);
        //print_r($resultado);
        
        if(empty($resultado)){return "xx";}
        
        return $resultado["ruta"]; //A1 A2
        
            
        //print_r($asignado);
        
        
        
    }
    
 
    
    
    
    
    /**
     * Consulta las categorias unicas existentes en la tabla de productos
     * @return array[][] Las categorias unicas existentes<p>
     * 
     * 
     *  
     */
    public function productos_consultarCategorias (){
        $consulta = $this->conexion->prepare("SELECT DISTINCT categoria FROM productos_detalles");
        $consulta->execute();
        $categorias = $consulta->fetchAll(PDO::FETCH_ASSOC);
        
        //print_r($categorias);
        /*
         * Array
            (
                [0] => Array
                    (
                        [categoria] => Sudadera
                    )

                [1] => Array
                    (
                        [categoria] => Ropa Interior
                    )

                [2] => Array
                    (
                        [categoria] => 
                        [0] => 
                    )

            )
         */
        
        
       
        return $categorias;
        
    }
    
    /**
     * 
     * @return array[] Contiene los id unicos de los productos<p>
     * Array<p>
            (<p>
                [0] => SM5898<p>
                [1] => ST5898<p>
                [2] => PD5898<p>                
     * )<p>
     */
    public function productos_consultarIdUnicoDelProducto(){
        $consulta = $this->conexion->prepare("SELECT DISTINCT id_producto FROM productos_detalles ");
        $consulta->execute();
        $idProductos = $consulta->fetchAll(PDO::FETCH_COLUMN);
        
        //print_r($idProductos);
        
        /*
         Array
            (
                [0] => SM5898
                [1] => ST5898
                [2] => PD5898
                [3] => PD5899
                [4] => PH5899
                [5] => PH56000
                [6] => PH56001
                [7] => SH1589
                [8] => SH1590
                [9] => SH1591
                [10] => SH1592
                [11] => PD8598
                [12] => PD8500
                [13] => M7891
                [14] => M7892
                [15] => B4598
                [16] => B4599
                [17] => B4600
                [18] => C4600
                [19] => C4601
                [20] => C4602
                [21] => P1451
                [22] => P1452
                [23] => P1453
                [24] => CM899
                [25] => CM900
                [26] => CM901
                [27] => CH9000
                [28] => CH9001
                [29] => CH9002
                [30] => CH9003
                [31] => BD1545
                [32] => BD1546
                [33] => BD1547
                [34] => BD1548
                [35] => BD1549
                [36] => BD1550
            )
         */
        
        return $idProductos;
    }
    
    /**
     * Crea un array que contiene las imagenes principales de nuestros productos sin ser repetidos 
     * @return array[][] el array bidimencional con 1 solo id de producto unico encontrado y con 
     *                  todos sus campos incluidas las url de imagenes
     */
    public function productos_crearImagenPrincipal(){
        /*
         * En este metodo consultaremos todos los productos existentes y nos encargaremos
         * de crear el array que contendra la informacion para que la app_kaliope muestre
         * en su imagen principal, por ejemplo la app tendra que mostrar
         * el unico modelo de sudadera con codigo sm5898 en imagen de baja resolucion
         * con 1 foto de portada y sus datos basicos a mostrar en la app kaliope
         * que seran precio y nombre
         * 
         * tendra que enviar en otro indice del array igual el otro producto con una foto de portada 
         * y la informacion basica del producto.
         * 
         * es decir este metodo le enviara la informacion necesaria a la app para que cree el menu 
         * principal de todos los codigos
         * 
         */
        
        
        
            $sql = "SELECT id_producto, descripcion, estado, precio_etiqueta, imagen1 FROM productos_detalles GROUP BY id_producto ORDER BY id ASC";
            
            $consulta = $this->conexion->prepare($sql);
            $consulta->execute();
            $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
            
            //print_r($resultado);
            /*
             * Array
                (
                    [0] => Array
                        (
                            [id_producto] => SM5898
                            [descripcion] => Sudadera dama
                            [estado] => ACTIVO
                            [precio_etiqueta] => 339
                            [imagen1] => fotos/SM5898-VERDE-1.jpg
                        )

                    [1] => Array
                        (
                            [id_producto] => ST5898
                            [descripcion] => Sueter dama
                            [estado] => ACTIVO
                            [precio_etiqueta] => 359
                            [imagen1] => fotos/ST5898-VERDE AGUA-1.jpg
                        )

                    [2] => Array
                        (
                            [id_producto] => PD5898
                            [descripcion] => Pantalon Dama
                            [estado] => ACTIVO
                            [precio_etiqueta] => 429
                            [imagen1] => fotos/PD5898-AZUL-1.jpg
                        )
             */
            
            
            
            
        
        
        
       
        
        return $resultado;
        
        
        
        
        
        
    }  
 
    public function productos_consultarDetallePrimerProducto($idProducto){
        /*
         * Queremos que la app de los clientes cuando entre a los detalles se conecte y consulte
         * por el id del modelo en el que hiso click y quiere ver los detalles, al entrar a los detalles
         * mostraremos por defecto las imagenes del primer registro con ese modelo que se encuentre
         * retornaremos todos los datos de ese primer registro encontrado.
         * 
         */
        
        
        
        $detalle_producto = $this->conexion->prepare(
            "SELECT * FROM productos_detalles WHERE id_producto=? LIMIT 1"
        );
        $detalle_producto->execute([$idProducto]);
        $resultado = $detalle_producto->fetch(PDO::FETCH_ASSOC);

        //print_r($resultado);
        /*
         *Array
            (
                [id] => 1
                [id_producto] => SM5898
                [descripcion] => Sudadera dama
                [detalles] => Luce sensacional con esta increible sudadera
                [estado] => ACTIVO
                [existencia] => 0
                [color] => Gris
                [noColor] => rgb(142, 142, 142)
                [talla] => UNT
                [precio_etiqueta] => 339
                [precio_vendedora] => 298
                [precio_socia] => 295
                [precio_empresaria] => 291
                [precio_inversionista] => 280
                [imagen1] => fotos/SM5898-VERDE-1.jpg
                [imagen2] => fotos/SM5898-VERDE-2.jpg
                [imagen3] => fotos/SM5898-VERDE-3.jpg
                [imagen_permanente] => fotos/SM5898-VERDE-1.jpg
                [categoria] => 
            )
         */

        return $resultado;
    }
    
    private function productos_consultarDetallePrimerProductoPorColor($idProducto,$noColor){
        /*
         * 
         * 
         */
        
        
        
        $detalle_producto = $this->conexion->prepare(
            "SELECT * FROM productos_detalles WHERE id_producto=? AND noColor=?"
        );
        $detalle_producto->execute([$idProducto, $noColor]);
        $resultado = $detalle_producto->fetch(PDO::FETCH_ASSOC);

        //print_r($resultado);
        /*
         *Array
            (
                [id] => 1
                [id_producto] => SM5898
                [descripcion] => Sudadera dama
                [detalles] => Luce sensacional con esta increible sudadera
                [estado] => ACTIVO
                [existencia] => 0
                [color] => Gris
                [noColor] => rgb(142, 142, 142)
                [talla] => UNT
                [precio_etiqueta] => 339
                [precio_vendedora] => 298
                [precio_socia] => 295
                [precio_empresaria] => 291
                [precio_inversionista] => 280
                [imagen1] => fotos/SM5898-VERDE-1.jpg
                [imagen2] => fotos/SM5898-VERDE-2.jpg
                [imagen3] => fotos/SM5898-VERDE-3.jpg
                [imagen_permanente] => fotos/SM5898-VERDE-1.jpg
                [categoria] => 
            )
         */

        return $resultado;
    }
  
    /**
     * Devuelve un resumen de los colores existentes por determinado modelo
     * este metodo ayuda a la app kaliope ya que quiere saber la imagen principal
     * del producto de cada color existente para mostrarlo en el spinner de seleccion
     * ademas añadimos el campo de existencias disponibles de ese color
     * @param String $idProducto
     * @return array[[]] bidimencional con los distintos colores encontrados para ese producto
     * 
     */
    public function productos_consultarColoresParaApp($idProducto){
        /*
         * En esta funcion obtendremos solo 1 imagen del id del producto seleccionado 
         * de cada color, y solo la imagen 1, esto nos servira para que la app kaliope
         * muestre en el selector de color en lugar de cuadritos de color
         * muestre 1 sola imagen de cada color del producto
         * 
         * primero consultamos por el id del producto, y pedimos que sean colores unicos no importa si hay 3 registros
         * rosas de diferente talla solo devolvera un registro con una imagen rosa
         * 
         * vamos a añadir a cada item el campo calculado de sus existencias totales
         * este campo no esta en base de datos sino que se calcula con un metodo
         * esto nos ayuda porque directamente ya le estamos enviando al metodo que llame a este
         * cuantas existencias hay de ese modelo de producto y de ese color en especifico sin clasificar las talals
         * es decir no importa si en color ROSA hay talla mediana con 10pz y en chica solo 5 y grande 3
         * las existencias totales de este color seran 18pz que seran devueltas en esta consulta
         * 
         */
        
        
        $detalle_producto = $this->conexion->prepare(
            "SELECT color, noColor, imagen1 FROM productos_detalles WHERE id_producto=? GROUP BY noColor ORDER BY id ASC"
        );
        $detalle_producto->execute([$idProducto]);
        $resultado = $detalle_producto->fetchAll(PDO::FETCH_ASSOC);

        //print_r($resultado);
        /*
         * 
           Array
            (
                [0] => Array
                    (
                        [color] => Gris
                        [noColor] => rgb(142, 142, 142)
                        [imagen1] => fotos/SM5898-VERDE-1.jpg
                    )

                [1] => Array
                    (
                        [color] => Rosa
                        [noColor] => rgb(240, 74, 141)
                        [imagen1] => fotos/SM5898-ROSA-1.jpg
                    )

                [2] => Array
                    (
                        [color] => Negro
                        [noColor] => rgb(13, 13, 13)
                        [imagen1] => fotos/SM5898-NEGRO-1.jpg
                    )

                [3] => Array
                    (
                        [color] => Azul
                        [noColor] => rgb(135, 182, 205)
                        [imagen1] => fotos/SM5898-AZUL-1.jpg
                    )

            )
         */
        
        
        //hasta arriba todo normal son los datos que entrega la base de datos
        //añadimos a cada item su campo calculado con las existencias totales esta es adicional no se encuentra en base de datos se calcula
        //lo recorro con un for normal para añadir directo sobre el array original el nuevo item en cambio si fuera foreach tendria que crear un array auxiliar
        for ($index = 0; $index < count($resultado); $index++) {
            $noColorSeleccionado = $resultado[$index]['noColor'];                           //obtenemos el codigo de color que estamos en este momento para usarlo como parametro
            $resultado[$index]["existencias"] = $this->productos_consultarExistenciasPorModeloColor($idProducto, $noColorSeleccionado);         //al llamar a una posicion que no existe en este caso "existencias" el comportamiento del array es añadir ese item hasta el final nuevo
        }
        //print_r($resultado);        
        /*
         * Array
            (
                [0] => Array
                    (
                        [color] => Gris
                        [noColor] => rgb(142, 142, 142)
                        [imagen1] => fotos/SM5898-VERDE-1.jpg
                        [existencias] => 45
                    )

                [1] => Array
                    (
                        [color] => Rosa
                        [noColor] => rgb(240, 74, 141)
                        [imagen1] => fotos/SM5898-ROSA-1.jpg
                        [existencias] => 55
                    )

                [2] => Array
                    (
                        [color] => Negro
                        [noColor] => rgb(13, 13, 13)
                        [imagen1] => fotos/SM5898-NEGRO-1.jpg
                        [existencias] => 10
                    )

                [3] => Array
                    (
                        [color] => Azul
                        [noColor] => rgb(135, 182, 205)
                        [imagen1] => fotos/SM5898-AZUL-1.jpg
                        [existencias] => 10
                    )

            )
         */
        
        
        
        return $resultado;
    }
    
    /**
     * Devuelve solo los noColor diferentes existentes en cada modelo
     * ayuda para manejar consultas mas pequeñas o para la app kaliope
     * ayuda para el modo offline porque desde el inicio sabremos cuantos
     * colores hay en un determinado producto con una cadena muy corta y ordenada
     * @param String $idProducto
     * @return array [] array simple solo con los noColor existentes de cada modelo
     */
    public function productos_consultarNoColoresSimple($idProducto){
        /*
         * Queremos obtener solo el numero de color existentes de un 
         * determinado modelo en un array simple
         * 
         */
        
        
        $detalle_producto = $this->conexion->prepare(
            "SELECT color, noColor FROM productos_detalles WHERE id_producto=? GROUP BY noColor ORDER BY id ASC"
        );
        $detalle_producto->execute([$idProducto]);
        $resultado = $detalle_producto->fetchAll(PDO::FETCH_COLUMN);
        print_r($resultado);
        /*
         * Array
            (
                [0] => rgb(142, 142, 142)
                [1] => rgb(240, 74, 141)
                [2] => rgb(13, 13, 13)
                [3] => rgb(135, 182, 205)
            )
         */

        return $resultado;
    }

    public function productos_consutarDetallePorColor($idProducto, $noColorSeleccionado){
        /*
         * Consultaremos solamente las 3 fotos por el color RGB del producto es lo unico que nos interesa consultar
         */
        
        
        $consulta = $this->conexion->prepare(
                "SELECT imagen1, imagen2, imagen3 FROM productos_detalles WHERE id_producto=? AND noColor=?"
                );
        $consulta->execute([$idProducto, $noColorSeleccionado]);
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);                            //en teoria solo se recibira un resultado
        //print_r($resultado);
        /*
         Array
            (
                [imagen1] => fotos/SM5898-ROSA-1.jpg
                [imagen2] => fotos/SM5898-ROSA-2.jpg
                [imagen3] => fotos/SM5898-ROSA-3.jpg
            ) 
         */
        
        return $resultado;      
             
    }
    
    /**
     * Devuelve las diferentes tallas disponibles por un id de producto
     * @param type $idProducto
     */
    public function productos_consultarTallasPorModelo($idProducto){
        $consulta = $this->conexion->prepare(
                "SELECT DISTINCT talla FROM productos_detalles WHERE id_producto=?"
                );
        $consulta->execute([$idProducto]);
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);                            
        //print_r($resultado);
        /*
         * Array
            (
                [0] => Array
                    (
                        [talla] => UNT
                    )

                [1] => Array
                    (
                        [talla] => G
                    )

            )
         */
        return $resultado;
    }
    
    public function productos_consultarTallasPorColor($idProducto, $noColorSeleccionado){
        /*
         * Consultamos las tallas que tenga ese producto en ese color y tambien sus existencias
         */
        
        $consulta = $this->conexion->prepare(
                "SELECT DISTINCT talla FROM productos_detalles WHERE id_producto=? AND noColor=?"
                );
        $consulta->execute([$idProducto, $noColorSeleccionado]);
        $tallas = $consulta->fetchAll(PDO::FETCH_ASSOC);  //quiero el array bidimencional intencionalmente lo encesito para los metodos de abajo. lo que si es que solo queiro los valores asociativos por eso el fetch assoc                          
        //print_r($tallas);
        /*
         * Array
            (
                [0] => Array
                    (
                        [talla] => UNT
                    )

                [1] => Array
                    (
                        [talla] => G
                    )

            )
         */
        
        //le calculamos las existencias a esa talla por color
        for ($index = 0; $index < count($tallas); $index++) {
                $tallaEnCurso = $tallas[$index]['talla'];              //obtenemos 1 talla para usarla como parametro
                $tallas[$index]['existencias'] = $this->productos_consultarExistenciasPorModeloColorTalla($idProducto, $noColorSeleccionado, $tallaEnCurso);   //al hacer referencia a una posiciond e un array que no existe en ete caso existencias, los arrays en php añaden el nuevo item, ver mi apunte en apintes de programacion PHP
            }
            
        //print_r($tallas);
        /*
         * Array
            (
                [0] => Array
                    (
                        [talla] => UNT
                        [existencias] => 40
                    )

                [1] => Array
                    (
                        [talla] => M
                        [existencias] => 15
                    )

            )
         */
        return $tallas;
        
    }
 
    /**
     * Devuelve el total de existencias de un codigo de producto sin importar
     * cuantos colores tiene ni cuantas tallas
     * @param String $idProducto
     * @return int   $existenciasTotales 
     */
    public function productos_consultarExistenciasPorModelo($idProducto){
        /*
         * Buscamos saber cuantas existencias totales tiene un determinado modelo
         * sin importar cuantos colores hay y cuantas tallas.
         * 
         */
        
        $consulta = $this->conexion->prepare(
                "SELECT existencia FROM productos_detalles WHERE id_producto=?"
                );
        $consulta->execute([$idProducto]);
        $resultado = $consulta->fetchAll(PDO::FETCH_COLUMN);
        
        //print_r($resultado);        
        /*
         * Array
            (
                [0] => 5
                [1] => 40
                [2] => 10
                [3] => 10
                [4] => 40
                [5] => 15
            )
         */
        
        return array_sum($resultado);
                
        
    }
    
    /**
     * Consulta las existencias totales que tiene un color de un determinado producto
     * sin importar las tallas
     * @param type $id_producto
     * @param type $noColorSeleccionado
     * @return int $existencias totales por color sin importar tallas
     */
    public function productos_consultarExistenciasPorModeloColor($id_producto, $noColorSeleccionado){
        /*
         * Queremos saber cuantas existencias totales de este producto en determinado color existen
         * sin importar las tallas,
         * pro ejemplo en la sudadera modelo sm5898 hay 5 colores entre ellos el naranja
         * y claro ese naranja diferentes tallas pero eso no nos importa aqui, de todas esas tallas
         * diferentes, cuantas existencias hay en total.
         * 
         */
        $consulta = $this->conexion->prepare(
                "SELECT existencia FROM productos_detalles WHERE id_producto=? AND noColor=?"
                );
        $consulta->execute([$id_producto, $noColorSeleccionado]);
        $resultado = $consulta->fetchAll(PDO::FETCH_COLUMN);
        
        //print_r($resultado);
        /*
         * Array
            (
                [0] => 5
                [1] => 40
            )
         * 
         */
        
        return array_sum($resultado);        
        
        
        
    }
    
    /**
     * Devuelve las existencias de un modelo determinado un color determinado y talla en especifico
     * 
     * @param String $id_producto
     * @param String $noColorSeleccionado
     * @param String $tallaSeleccionada
     * @return int
     */
    public function productos_consultarExistenciasPorModeloColorTalla($id_producto, $noColorSeleccionado, $tallaSeleccionada){
        /*
         * 
         * Queremos saber cuantas existencias tiene una talla en especifico de un modelo y colores especifico
         * asi por ejemplo del sueter sm5898 del color rosa y la talla mediana cuantas piezas tienen
         */
        
        $consulta = $this->conexion->prepare(
                "SELECT existencia FROM productos_detalles WHERE id_producto=? AND noColor=? AND talla=?"
                );
        $consulta->execute([$id_producto, $noColorSeleccionado, $tallaSeleccionada]);
        $resultado = $consulta->fetchAll(PDO::FETCH_COLUMN);
        
        //print_r($resultado);
        /*
         * Array
            (
                [0] => 10
           )
         */
        
        return array_sum($resultado);
    }
    
    /**
     * Devuelve las existencias de un determinado modelo filtrado por Talla
     * @param String $id_producto
     * @param String $tallaSeleccionada
     * @return int
     */
    public function productos_consultarExistenciasPorModeloTalla($id_producto, $tallaSeleccionada){
        /*
         * 
         * Queremos saber cuantas existencias tiene una talla en especifico de un modelo
         * asi por ejemplo del sueter sm5898 la talla mediana cuantas piezas tienen sin importar colores
         */
        
        $consulta = $this->conexion->prepare(
                "SELECT existencia FROM productos_detalles WHERE id_producto=? AND talla=?"
                );
        $consulta->execute([$id_producto, $tallaSeleccionada]);
        $resultado = $consulta->fetchAll(PDO::FETCH_COLUMN);
        
        //print_r($resultado);
        /*
         * Array
            (
                [0] => 5
                [1] => 40
                [2] => 10
            )
         */
        
        return array_sum($resultado);
    }
    
    public function productos_existenciasIncrementarExistencias($id_producto, $no_color, $talla, $cantidad){
        $statetment = $this->conexion->prepare("UPDATE productos_detalles SET existencia=existencia+'$cantidad' WHERE id_producto=? AND noColor=? AND talla=?");
        $statetment->execute([$id_producto, $no_color, $talla]);
    }
    
    
    
    public function productos_existenciasDecrementarExistencias($id_producto, $no_color, $talla, $cantidad){
        $statetment = $this->conexion->prepare("UPDATE productos_detalles SET existencia=existencia-'$cantidad' WHERE id_producto=? AND noColor=? AND talla=?");
        
        return $statetment->execute([$id_producto, $no_color, $talla]);
        
    }
    
   /**
    * Consultamos de la tabla principal de los productos detalles, si un producto en esepecifico tiene existencias suficientes para
    * surtir un pedido
    * @param String $id_producto
    * @param String $no_color
    * @param String $talla
    * @param String $cantidad
    * @return boolean true si hay cantidades suficientes <br> false si no hay cantidades para surtir el producto
    */
    public function productos_existenciasComprobar($id_producto, $no_color, $talla, $cantidad){
        /*
         * Comprobamaos que existan existencias suficientes de un producto en especifico
         */
        
       $consulta = $this->conexion->prepare("SELECT existencia FROM productos_detalles WHERE id_producto=? AND noColor=? AND talla=?");
       $consulta->execute([$id_producto, $no_color, $talla]);
       $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
       //print_r($existencia);
       /*
        * Array
        (
            [existencia] => 10
        )
        */
       
       
       //si la cantidad por algun motivo es 0 o menor a 0 que no deberia ocurrir retornamos false
       //porque si no este metodo retornaria true si la cantidad es 0 y las existencias son 0
       //aunque no creo que ocurra nunca pero pues lo pongo por si las moscas
       if($cantidad<=0){return false;}
       
       
       if(!empty($resultado)){
           //si se ha encontrado el prodcuto en el detalle de producos, en teoria siempre deberiamos estar aqui a menos que
           //el codigo que se busque sea incorrecto o se halla eliminado de la lista de productos pero no del carrito de los cleintes
           //y se este queriendo volver a comprobar existencias de un prodcuto inexistente
           
           $existencia = $resultado["existencia"];
           
           
           //si la cantidad que se quiere comprobar es menor o igual a las existencias que hay en el sistema de productos
           //entonces retornara true,
           //si la cantidad que se queire comprobar es mayor que las existencias que tenemos en los productos
           //retornamos false para que el metodo que llame a este sepa que no hay existencias suficientes para suplir el pedido
           return ($cantidad <= $existencia);
            
           
           
                  
           
       }else{
           //si el resultado esta vacio significa que el codigo no existe ya en la tabla de prodcutos
           //o el color o la talla etc esto solo podria ocurrir por datos corrompidos, o porque
           //se elimino de la tabla productos detalles el producto que aun se quiere modificar en la tabla de carrito
          return false;
       }
       
       
    }
 
    /**
     * Devuelve una vision mas detallada de los colores de un producto
     * por ejemplo si tiene 3 colores, de cada color devuelve
     * su color, noColor, imagen1, existencias de ese color, 
     * y tallasExistentes(talla, existencias)
     * las tallas se devuelven en un array con su talla correspondiente y sus existencias
     * de cada talla filtradas en ese color
     * 
     * todo en un mismo metodo y array
     * @param String $idProducto
     * @return $array [][]
         
     */
    private function productos_consultarDatosColorGeneralesPorIdProducto($idProducto){
        /*
         * Formaremos un array un poco largo porque debera ser muy detallado
         * se consultara un producto en especifico por ejemplo SM5898
         * se debera responder en un solo array lo siguiente
         * 
         * colores disponibles, con su color, noColor imagen1 y existencias totales para ese color-
         * dentro de cada color sus tallas disponibles y dentro de esas tallas sus existencias por talla
         * hare manual abajo en este comentario el array el como quiero que quede
         * 
         * array(
         *      
         *      [0]=>array
         *             (
         *              [color] => Rosa
                        [noColor] => rgb(240, 74, 141)
                        [imagen1] => fotos/SM5898-ROSA-1.jpg
         *              [existencias] => 58
         *              [tallas]=>array
         *                           (
         *                              [0]=>array(
         *                                      [talla]=>M
         *                                      [existencias]=>5
         *                                      ) 
         *                              [1]=>array(
         *                                      [talla]=>G
         *                                      [existencias]=>10
         *                                      ) 
         *                              [2]=>array(
         *                                      [talla]=>CH
         *                                      [existencias]=>15
         *                                      ) 
         *                           )                                  
         *              )
         * 
         * 
         *      [1]=>array
         *             (
         *              [color] => Verde
                        [noColor] => rgb(250, 40, 141)
                        [imagen1] => fotos/SM5898-VERDE-1.jpg
         *              [existencias] => 40
         *              [tallas]=>array
         *                           (
         *                              [0]=>array(
         *                                      [talla]=>CH
         *                                      [existencias]=>8
         *                                      ) 
         *                              [1]=>array(
         *                                      [talla]=>G
         *                                      [existencias]=>4
         *                                      ) 
         *                              
         *                           )                                  
         *              )
         *          
         * 
         * )
         * 
         */
        
        
        $colores = $this->productos_consultarColoresParaApp($idProducto);
        //print_r($colores);
        /*Aqui obtenemos la base la informacionde los colores por cada modelo
         * Array
            (
                [0] => Array
                    (
                        [color] => Gris
                        [noColor] => rgb(142, 142, 142)
                        [imagen1] => fotos/SM5898-VERDE-1.jpg
                        [existencias] => 45
                    )

                [1] => Array
                    (
                        [color] => Rosa
                        [noColor] => rgb(240, 74, 141)
                        [imagen1] => fotos/SM5898-ROSA-1.jpg
                        [existencias] => 55
                    )
         * )
         */
        
        //recorremos el array con un for normal para modificar directo el array principal
        for ($index = 0; $index < count($colores); $index++) {
            $noColorSeleccionado = $colores[$index]['noColor']; //obtemnemos el node color que se esta recorriendo para usarlo como parametro

            //obtenemos el array de las tallas que hay en este color
            $tallas = $this->productos_consultarTallasPorColor($idProducto, $noColorSeleccionado);
            //print_r($tallas);
            /*Por cada color que existe obtenemos las tallas que hay y existencias
             Array
                (
                    [0] => Array
                        (
                            [talla] => UNT
                            [existencias] => 5
                        )

                    [1] => Array
                        (
                            [talla] => G
                            [existencias] => 40
                        )

                )
             */
 
            //ahora que ya tenemos el array tallas con sus existencias correspondientes para cada color vamos a añadir el array tallas al array principal
            //debido a que el key tallas no existe en el array principal este sera agregado
            $colores[$index]["tallas"]=$tallas;
        }
        
        
        
        
        //print_r($colores);
        //y voilaaaa!!!! jajajaja no seas mamon.
        //ya quedo exacto como lo queriamos al inicio
        //ahora se puede codificar ese array en un json sin problemas para la app
        /*este seria el array devuelto por este metodo
         * Array
                (
                    [0] => Array
                        (
                            [color] => Gris
                            [noColor] => rgb(142, 142, 142)
                            [imagen1] => fotos/SM5898-VERDE-1.jpg
                            [existencias] => 45
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 5
                                        )

                                    [1] => Array
                                        (
                                            [talla] => G
                                            [existencias] => 40
                                        )

                                )

                        )

                    [1] => Array
                        (
                            [color] => Rosa
                            [noColor] => rgb(240, 74, 141)
                            [imagen1] => fotos/SM5898-ROSA-1.jpg
                            [existencias] => 55
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 40
                                        )

                                    [1] => Array
                                        (
                                            [talla] => M
                                            [existencias] => 15
                                        )

                                )

                        )
         */
        
        return $colores;
        
        
    }
    /**
     * Consulta todos los productos existentes y devuelve un array
     * con un producto en cada indice
     * cada producto cotiene id_producto, detalles, estado, existenciasTotales,Categoria,ArrayTallas
     * (el cual contiene a su vez un array con todas las tallas disponibles de ese producto y sus existencias sin importar color)
     * @return array [][]
     */
    private function productos_consultarDatosGenerales(){
        /*
         * Queremos obtener un unico array, en donde estaran todos y cada uno de los productos
         * cada producto tendra como datos generales su id de producto, detalles, estado, categoria
         * y tendra ademas los datos computados de todas las existencias que hay de ese producto sin importar color ni tallas
         * ademas entregara todas las tallas que hay sin importar su color, y las existencias totales de esas tallas 
         * esto sera para inicio porque en otro metodo
         * usaremos el array que devuelva este metodo pero donde meteremos los detalles que computamos en el metodo de arriba de PorIDProducto
         * esto para solo si quieres datos mas generales usar uno u otro metodo o el metodo mas complejo
         * 
         * 
         * 
         * 
          array(
              
               [0]=>array
                      (                       
                       [id_producto]=>SM5898
                       [detalles]=>luce sensacional con esta sudadera
                       [estado]=>activo
                       [existencias totales]=>120                                                   estas existencias son las totales sin importar talla ni color
                       [categoria]=>ROPA INTERIOR
                       [tallas]=>array
                                 (
                                     [0]=>array(
                                            [talla]=>M
                                            [existencias]=>5                                        seran las existencias de esa talla en todos los colores
                                              ) 
                                     [1]=>array(
                                            [talla]=>G
                                            [existencias]=>10
                                            ) 
                                     [2]=>array(
                                            [talla]=>CH
                                            [existencias]=>15
                                            ) 
                                  )
                       )
          
          
          
                [1]=>array
                      (                       
                       [id_producto]=>pd5899
                       [detalles]=>luce sensacional con esta sudadera
                       [estado]=>activo
                       [existencias totales]=>120                                                   estas existencias son las totales sin importar talla ni color
                       [categoria]=>ROPA INTERIOR
                       [tallas]=>array
                                 (
                                     [0]=>array(
                                            [talla]=>M
                                            [existencias]=>5                                        seran las existencias de esa talla en todos los colores
                                              ) 
                                     [1]=>array(
                                            [talla]=>G
                                            [existencias]=>10
                                            ) 
                                     [2]=>array(
                                            [talla]=>CH
                                            [existencias]=>15
                                            ) 
                                  )
                       )

                   
          
           )
         * 
         * 
         * 
         * 
         * 
         */
        
            $consulta = $this->conexion->prepare(
                    "SELECT id_producto, descripcion, estado, precio_etiqueta, precio_vendedora, precio_empresaria, imagen1, imagen2, imagen3, categoria"
                    . " FROM productos_detalles WHERE existencia!=0 GROUP BY id_producto ORDER BY id ASC"
                    );
            $consulta->execute();
            $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
            
            //print_r($resultado);
            /*yA TENGO LA PRIMER PARTE DEL ARRAY
             * Array
                    (
                        [0] => Array
                            (
                                [id_producto] => SM5898
                                [descripcion] => Sudadera dama
                                [estado] => ACTIVO
                                [precio_etiqueta] => 339
                                [precio_vendedora] => 298
                                [precio_empresaria] => 291
                                [imagen1] => fotos/SM5898-VERDE-1.jpg
                                [imagen2] => fotos/SM5898-VERDE-2.jpg
                                [imagen3] => fotos/SM5898-VERDE-3.jpg
                                [categoria] => sudadera
                            )

                        [1] => Array
                            (
                                [id_producto] => ST5898
                                [descripcion] => Sueter dama
                                [estado] => ACTIVO
                                [precio_etiqueta] => 359
                                [precio_vendedora] => 310
                                [precio_empresaria] => 301
                                [imagen1] => fotos/ST5898-VERDE AGUA-1.jpg
                                [imagen2] => fotos/ST5898-VERDE AGUA-2.jpg
                                [imagen3] => fotos/ST5898-VERDE AGUA-3.jpg
                                [categoria] => 
                            )
             *      )
             * 
             */
            
            
            
            
            //ahora añadimos el campo existencias sin importar talla ni color
            for ($index = 0; $index < count($resultado); $index++) {
                $idProducto = $resultado[$index]["id_producto"];            //por cada iteracion obtenemos el modelo del producto en iteracion
                $resultado[$index]["existencias"] = $this->productos_consultarExistenciasPorModelo($idProducto);        //al llamar a una posicion de array inexistente en este caso "existencias" php añade esa key al array con el valor deseado
            
                
                
                //consultamos las tallas para ese modelo y sus existencias
                $tallas = $this->productos_consultarTallasPorModelo($idProducto);
                for ($index1 = 0; $index1 < count($tallas); $index1++) {
                    //consultamos las existencias de cada talla
                    $tallaEnCurso = $tallas[$index1]["talla"];
                    $tallas[$index1]["existencias"] = $this->productos_consultarExistenciasPorModeloTalla($idProducto, $tallaEnCurso);      //añadimos el calculo al array de tallas
                    
                }
                
                
                
                
                
                $resultado[$index]["tallas"]= $tallas;      //añadimos el array tallas al array principal
                
            }
            //print_r($resultado);
            /*Ahora ya tenemos la columna existencia y tallas con los datos añadidos
             * Array
                    (
                        [0] => Array
                            (
                                [id_producto] => SM5898
                                [descripcion] => Sudadera dama
                                [estado] => ACTIVO
                                [precio_etiqueta] => 339
                                [precio_vendedora] => 298
                                [precio_empresaria] => 291
                                [imagen1] => fotos/SM5898-VERDE-1.jpg
                                [imagen2] => fotos/SM5898-VERDE-2.jpg
                                [imagen3] => fotos/SM5898-VERDE-3.jpg
                                [categoria] => sudadera
                                [existencias] => 120
                                [tallas] => Array
                                    (
                                        [0] => Array
                                            (
                                                [talla] => UNT
                                                [existencias] => 55
                                            )

                                        [1] => Array
                                            (
                                                [talla] => G
                                                [existencias] => 50
                                            )

                                        [2] => Array
                                            (
                                                [talla] => M
                                                [existencias] => 15
                                            )

                                    )

                            )

                        [1] => Array
                            (
                                [id_producto] => ST5898
                                [descripcion] => Sueter dama
                                [estado] => ACTIVO
                                [precio_etiqueta] => 359
                                [precio_vendedora] => 310
                                [precio_empresaria] => 301
                                [imagen1] => fotos/ST5898-VERDE AGUA-1.jpg
                                [imagen2] => fotos/ST5898-VERDE AGUA-2.jpg
                                [imagen3] => fotos/ST5898-VERDE AGUA-3.jpg
                                [categoria] => 
                                [existencias] => 40
                                [tallas] => Array
                                    (
                                        [0] => Array
                                            (
                                                [talla] => UNT
                                                [existencias] => 40
                                            )

                                    )

                            )
             * 
             *  )
             */
            
            
            
            return $resultado;            
    
    }
    
    /**
     * Devuelve del producto la informacion total general
     * devuelve su <br> id_producto <br> descripcion <br> estado <br> precioEtiqueta <br> precio_vendedora
     * <br>precio socia <br> precio Empresaria <br> imagen1 <br> imagen2 <br> imagen3 <br> categoria <br> existenciasTotales
     * <p>tallas=> es un array con las tallas y existencias que tiene este modelo sin importar color
     * <p>colores=>es un array con los datos detallados color pero solo 1 foto <br> noColor <br> imagen1 <br> existencias <br> tallas=> talla, existencias por color
     * @param int $idProducto
     * @return array[array[]]
     */
    public function productos_consultarDatosGeneralesDetalladosPorIdProducto($idProducto){
        /*
         * Queremos obtener un unico array, con la informacion general de solo un producto
         * tendra como datos generales su id de producto, detalles, estado, categoria
         * y tendra ademas los datos computados de todas las existencias que hay de ese producto sin importar color ni tallas
         * ademas entregara todas las tallas que hay sin importar su color, y las existencias totales de esas tallas 
         * y entregaremos tambien todos los colores que tiene disponibles con sus respectivas existencias y tallas
         * 
         * 
         * 
         * 
          array(
              
               [0]=>array
                      (                       
                       [id_producto]=>SM5898
                       [detalles]=>luce sensacional con esta sudadera
                       [estado]=>activo
                       [existencias totales]=>120                                                   estas existencias son las totales sin importar talla ni color
                       [categoria]=>ROPA INTERIOR
                       [tallas]=>array
                                 (
                                     [0]=>array(
                                            [talla]=>M
                                            [existencias]=>5                                        seran las existencias de esa talla en todos los colores
                                              ) 
                                     [1]=>array(
                                            [talla]=>G
                                            [existencias]=>10
                                            ) 
                                     [2]=>array(
                                            [talla]=>CH
                                            [existencias]=>15
                                            ) 
                                  )
                        [colores]=>
                                    Array(    
                                    [0]=>array
                                     (
                                      [color] => Rosa
                                       [noColor] => rgb(240, 74, 141)
                                       [imagen1] => fotos/SM5898-ROSA-1.jpg
                                      [existencias] => 58
                                      [tallas]=>array
                                                   (
                                                      [0]=>array(
                                                              [talla]=>M
                                                              [existencias]=>5
                                                              ) 
                                                      [1]=>array(
                                                              [talla]=>G
                                                              [existencias]=>10
                                                              ) 
                                                      [2]=>array(
                                                              [talla]=>CH
                                                              [existencias]=>15
                                                              ) 
                                                   )                                  
                                      )
                         
                         
                                    [1]=>array
                                           (
                                            [color] => Verde
                                             [noColor] => rgb(250, 40, 141)
                                             [imagen1] => fotos/SM5898-VERDE-1.jpg
                                            [existencias] => 40
                                            [tallas]=>array
                                                         (
                                                            [0]=>array(
                                                                    [talla]=>CH
                                                                    [existencias]=>8
                                                                    ) 
                                                            [1]=>array(
                                                                    [talla]=>G
                                                                    [existencias]=>4
                                                                    ) 

                                                         )                                  
                                            )
                       )
          
           )
         * 
         * 
         * 
         * 
         * 
         */
        
            $consulta = $this->conexion->prepare(
                    "SELECT id_producto, descripcion,detalles, estado, precio_etiqueta, precio_vendedora, precio_empresaria, imagen1, imagen2, imagen3, categoria"
                    . " FROM productos_detalles WHERE id_producto=? GROUP BY id_producto"
                    );
            $consulta->execute([$idProducto]);
            $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
            
            //print_r($resultado);
            /*yA TENGO LA PRIMER PARTE DEL ARRAY
             * Array
                    (
                        [0] => Array
                            (
                                [id_producto] => SM5898
                                [descripcion] => Sudadera dama
                                [estado] => ACTIVO
                                [precio_etiqueta] => 339
                                [precio_vendedora] => 298
                                [precio_empresaria] => 291
                                [imagen1] => fotos/SM5898-VERDE-1.jpg
                                [imagen2] => fotos/SM5898-VERDE-2.jpg
                                [imagen3] => fotos/SM5898-VERDE-3.jpg
                                [categoria] => sudadera
                            )
                        
             *      )
             * 
             */
            
            if(!empty($resultado)){
                $resultado = $resultado[0];         //ahora tenemos un array unico y no bidimencional
                //ahora añadimos el campo existencias sin importar talla ni color
            
                $idProducto = $resultado["id_producto"];            //por cada iteracion obtenemos el modelo del producto en iteracion
                $resultado["existencias"] = $this->productos_consultarExistenciasPorModelo($idProducto);        //al llamar a una posicion de array inexistente en este caso "existencias" php añade esa key al array con el valor deseado
            
                
                
                //consultamos las tallas para ese modelo y sus existencias
                $tallas = $this->productos_consultarTallasPorModelo($idProducto);
                for ($index = 0; $index < count($tallas); $index++) {
                    //consultamos las existencias de cada talla
                    $tallaEnCurso = $tallas[$index]["talla"];
                    $tallas[$index]["existencias"] = $this->productos_consultarExistenciasPorModeloTalla($idProducto, $tallaEnCurso);      //añadimos el calculo al array de tallas                    
                }
                
                $resultado["tallas"]= $tallas;      //añadimos el array tallas al array principal
                
           
            //print_r($resultado);
            /*Ahora ya tenemos la columna existencia y tallas con los datos añadidos
              Array
                   (
                       [id_producto] => SM5898
                       [descripcion] => Sudadera dama
                       [estado] => ACTIVO
                       [precio_etiqueta] => 339
                       [precio_vendedora] => 298
                       [precio_empresaria] => 291
                       [imagen1] => fotos/SM5898-VERDE-1.jpg
                       [imagen2] => fotos/SM5898-VERDE-2.jpg
                       [imagen3] => fotos/SM5898-VERDE-3.jpg
                       [categoria] => sudadera
                       [existencias] => 120
                       [tallas] => Array
                           (
                               [0] => Array
                                   (
                                       [talla] => UNT
                                       [existencias] => 55
                                   )

                               [1] => Array
                                   (
                                       [talla] => G
                                       [existencias] => 50
                                   )

                               [2] => Array
                                   (
                                       [talla] => M
                                       [existencias] => 15
                                   )

                           )

                   )
             */
            
            
            //ahora vamos a añadir la columna colores
            $resultado["colores"] = $this->productos_consultarDatosColorGeneralesPorIdProducto($idProducto);
            
            //print_r($resultado);
            /*LISTO!!
             Array
                (
                    [id_producto] => SM5898
                    [descripcion] => Sudadera dama
                    [estado] => ACTIVO
                    [precio_etiqueta] => 339
                    [precio_vendedora] => 298
                    [precio_empresaria] => 291
                    [imagen1] => fotos/SM5898-VERDE-1.jpg
                    [imagen2] => fotos/SM5898-VERDE-2.jpg
                    [imagen3] => fotos/SM5898-VERDE-3.jpg
                    [categoria] => sudadera
                    [existencias] => 120
                    [tallas] => Array
                        (
                            [0] => Array
                                (
                                    [talla] => UNT
                                    [existencias] => 55
                                )

                            [1] => Array
                                (
                                    [talla] => G
                                    [existencias] => 50
                                )

                            [2] => Array
                                (
                                    [talla] => M
                                    [existencias] => 15
                                )

                        )

                    [colores] => Array
                        (
                            [0] => Array
                                (
                                    [color] => Gris
                                    [noColor] => rgb(142, 142, 142)
                                    [imagen1] => fotos/SM5898-VERDE-1.jpg
                                    [existencias] => 45
                                    [tallas] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [talla] => UNT
                                                    [existencias] => 5
                                                )

                                            [1] => Array
                                                (
                                                    [talla] => G
                                                    [existencias] => 40
                                                )

                                        )

                                )

                            [1] => Array
                                (
                                    [color] => Rosa
                                    [noColor] => rgb(240, 74, 141)
                                    [imagen1] => fotos/SM5898-ROSA-1.jpg
                                    [existencias] => 55
                                    [tallas] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [talla] => UNT
                                                    [existencias] => 40
                                                )

                                            [1] => Array
                                                (
                                                    [talla] => M
                                                    [existencias] => 15
                                                )

                                        )

                                )

                            [2] => Array
                                (
                                    [color] => Negro
                                    [noColor] => rgb(13, 13, 13)
                                    [imagen1] => fotos/SM5898-NEGRO-1.jpg
                                    [existencias] => 10
                                    [tallas] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [talla] => UNT
                                                    [existencias] => 10
                                                )

                                        )

                                )

                            [3] => Array
                                (
                                    [color] => Azul
                                    [noColor] => rgb(135, 182, 205)
                                    [imagen1] => fotos/SM5898-AZUL-1.jpg
                                    [existencias] => 10
                                    [tallas] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [talla] => G
                                                    [existencias] => 10
                                                )

                                        )

                                )

                        )

                ) 
             */
            }
            
            
            
            
            
            
            
            
            
            return $resultado;            
    
    }
   
    /**
     * Devuelve todos los productos con su informacion detallada
     * devuelve su: <br> id_producto <br> descripcion <br> estado <br> precioEtiqueta <br> precio_vendedora
     * <br>precio socia <br> precio Empresaria <br> imagen1 <br> imagen2 <br> imagen3 <br> categoria <br> existenciasTotales
     * <p>tallas=> es un array con las tallas y existencias que tiene este modelo sin importar color
     * <p>colores=>es un array con los datos detallados color pero solo 1 foto <br> noColor <br> imagen1 <br> existencias <br> tallas=> talla, existencias por color
     * @return array[][]
     */
    public function productos_consultarDatosGeneralesDetallados(){
        /*
         * Usaremos los dos metodos superiores para hacer un array
         * mas grande y mas detallado sobre cada producto,
         * 
         * la base sera el array entregado por el metodo productos_consultarDatosGenerales
         * pero ahora añadiremos otro index que sera colores
         * ese index colores contendra el array entregado por el metodo productos_consultarDatosGeneralesPorIdProducto
         * 
         * array(
              
               [0]=>array
                      (                       
                       [id_producto]=>SM5898
                       [detalles]=>luce sensacional con esta sudadera
                       [estado]=>activo
                       [existencias totales]=>120                                                   estas existencias son las totales sin importar talla ni color
                       [categoria]=>ROPA INTERIOR
                       [tallas]=>array
                                 (
                                     [0]=>array(
                                            [talla]=>M
                                            [existencias]=>5                                        seran las existencias de esa talla en todos los colores
                                              ) 
                                     [1]=>array(
                                            [talla]=>G
                                            [existencias]=>10
                                            ) 
                                     [2]=>array(
                                            [talla]=>CH
                                            [existencias]=>15
                                            ) 
                                  )
                        [colores]=> 
                            Array(    
                                    [0]=>array
                                     (
                                      [color] => Rosa
                                       [noColor] => rgb(240, 74, 141)
                                       [imagen1] => fotos/SM5898-ROSA-1.jpg
                                      [existencias] => 58
                                      [tallas]=>array
                                                   (
                                                      [0]=>array(
                                                              [talla]=>M
                                                              [existencias]=>5
                                                              ) 
                                                      [1]=>array(
                                                              [talla]=>G
                                                              [existencias]=>10
                                                              ) 
                                                      [2]=>array(
                                                              [talla]=>CH
                                                              [existencias]=>15
                                                              ) 
                                                   )                                  
                                      )
                         
                         
                                    [1]=>array
                                           (
                                            [color] => Verde
                                             [noColor] => rgb(250, 40, 141)
                                             [imagen1] => fotos/SM5898-VERDE-1.jpg
                                            [existencias] => 40
                                            [tallas]=>array
                                                         (
                                                            [0]=>array(
                                                                    [talla]=>CH
                                                                    [existencias]=>8
                                                                    ) 
                                                            [1]=>array(
                                                                    [talla]=>G
                                                                    [existencias]=>4
                                                                    ) 

                                                         )                                  
                                            )
                        )
          
          
          
              */
        
        
        $arrayBase = $this->productos_consultarDatosGenerales();
        //print_r($arrayBase);
        
        
        for ($index = 0; $index < count($arrayBase); $index++) {
            //por cada producto en curso vamos a consultar su detalle de colores
            $idProducto = $arrayBase[$index]["id_producto"];
            $arrayBase[$index]["colores"] = $this->productos_consultarDatosColorGeneralesPorIdProducto($idProducto);         //al no existir la posicion colores se agrega sola 
            
            
        }
        //print_r($arrayBase);
        /*Obtenemos el array deseado esta muy largo
         * Array
(
    [0] => Array
        (
            [id_producto] => SM5898
            [descripcion] => Sudadera dama
            [estado] => ACTIVO
            [precio_etiqueta] => 339
            [precio_vendedora] => 298
            [precio_empresaria] => 291
            [imagen1] => fotos/SM5898-VERDE-1.jpg
            [imagen2] => fotos/SM5898-VERDE-2.jpg
            [imagen3] => fotos/SM5898-VERDE-3.jpg
            [categoria] => sudadera
            [existencias] => 120
            [tallas] => Array
                (
                    [0] => Array
                        (
                            [talla] => UNT
                            [existencias] => 55
                        )

                    [1] => Array
                        (
                            [talla] => G
                            [existencias] => 50
                        )

                    [2] => Array
                        (
                            [talla] => M
                            [existencias] => 15
                        )

                )

            [colores] => Array
                (
                    [0] => Array
                        (
                            [color] => Gris
                            [noColor] => rgb(142, 142, 142)
                            [imagen1] => fotos/SM5898-VERDE-1.jpg
                            [existencias] => 45
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 5
                                        )

                                    [1] => Array
                                        (
                                            [talla] => G
                                            [existencias] => 40
                                        )

                                )

                        )

                    [1] => Array
                        (
                            [color] => Rosa
                            [noColor] => rgb(240, 74, 141)
                            [imagen1] => fotos/SM5898-ROSA-1.jpg
                            [existencias] => 55
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 40
                                        )

                                    [1] => Array
                                        (
                                            [talla] => M
                                            [existencias] => 15
                                        )

                                )

                        )

                    [2] => Array
                        (
                            [color] => Negro
                            [noColor] => rgb(13, 13, 13)
                            [imagen1] => fotos/SM5898-NEGRO-1.jpg
                            [existencias] => 10
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 10
                                        )

                                )

                        )

                    [3] => Array
                        (
                            [color] => Azul
                            [noColor] => rgb(135, 182, 205)
                            [imagen1] => fotos/SM5898-AZUL-1.jpg
                            [existencias] => 10
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => G
                                            [existencias] => 10
                                        )

                                )

                        )

                )

        )

    [1] => Array
        (
            [id_producto] => ST5898
            [descripcion] => Sueter dama
            [estado] => ACTIVO
            [precio_etiqueta] => 359
            [precio_vendedora] => 310
            [precio_empresaria] => 301
            [imagen1] => fotos/ST5898-VERDE AGUA-1.jpg
            [imagen2] => fotos/ST5898-VERDE AGUA-2.jpg
            [imagen3] => fotos/ST5898-VERDE AGUA-3.jpg
            [categoria] => 
            [existencias] => 40
            [tallas] => Array
                (
                    [0] => Array
                        (
                            [talla] => UNT
                            [existencias] => 40
                        )

                )

            [colores] => Array
                (
                    [0] => Array
                        (
                            [color] => VERDE AGUA
                            [noColor] => rgb(200, 235, 231)
                            [imagen1] => fotos/ST5898-VERDE AGUA-1.jpg
                            [existencias] => 10
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 10
                                        )

                                )

                        )

                    [1] => Array
                        (
                            [color] => BEIGE
                            [noColor] => rgb(220, 202, 188)
                            [imagen1] => fotos/ST5898-BEIGE-1.jpg
                            [existencias] => 10
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 10
                                        )

                                )

                        )

                    [2] => Array
                        (
                            [color] => GRIS
                            [noColor] => rgb(122, 125, 130)
                            [imagen1] => fotos/ST5898-GRIS-1.jpg
                            [existencias] => 10
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 10
                                        )

                                )

                        )

                    [3] => Array
                        (
                            [color] => BLANCO
                            [noColor] => rgb(248, 248, 248)
                            [imagen1] => fotos/ST5898-BLANCO-1.jpg
                            [existencias] => 10
                            [tallas] => Array
                                (
                                    [0] => Array
                                        (
                                            [talla] => UNT
                                            [existencias] => 10
                                        )

                                )

                        )

                )

        )
         * 
         */
        
        
        
        return $arrayBase;
        
    }
    
    /**
     * Consultamos el costo de distribucion de un producto en el grado especifico de un cliente
     * @param string $numeroCuenta
     */
    public function producto_consultarPrecioDistribucion(string $numeroCuenta, $id_producto, $noColorSeleccionado, $tallaSeleccionada){
        
        
      $datosCliente = $this->clientes_consultarCliente($numeroCuenta);
      $grado=$datosCliente['grado'];
      //print_r($datosCliente);
      //print_r($grado);
      
      
      
      $consulta = $this->conexion->prepare(
                "SELECT precio_empresaria, precio_socia, precio_vendedora FROM productos_detalles WHERE id_producto=? AND noColor=? AND talla=?"
                );
        $consulta->execute([$id_producto, $noColorSeleccionado, $tallaSeleccionada]);
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        //print_r($resultado);
        
      if ($grado == "EMPRESARIA") {
            return $resultado['precio_empresaria'];
        } else if ($grado == "SOCIA") {
            return $resultado['precio_socia'];
        } else if ($grado == "VENDEDORA") {
            return $resultado['precio_vendedora'];
        }
      
       
        
    }
    

    
    
    
    
    
    /**
     * =======Usamos sentencias preparadas======https://diego.com.es/sentencias-preparadas-en-php
     * Inserta a la base de datos de carrito un producto que el cliente agrego a su pedido
     * gestiona en base a su numero de cuenta la fecha de entrega que le corresponde
     * @param String $numeroCuenta
     * @param String $id_producto
     * @param String $colorSeleccionado
     * @param String $tallaSeleccioanda
     * @param String $cantidadSeleccionada
     * @return bool $insersionCorrecta si se ingreso correctamente la informacion a la base de datos retorna true si no false
     */
    public function carrito_agregarProducto($numeroCuenta, $id_producto, $colorSeleccionado, $tallaSeleccioanda, $cantidadSeleccionada){
        /*
         * 
         * consultar el grado del cliente para determinar que precio se manejara al inicio
         * 
         */
        
        $datosCliente = $this->clientes_consultarCliente($numeroCuenta);
        
        /*
         * Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020               -->es la fecha de visita siguiente programada a menos que se cree una fecha futura deberia funcionar
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
            )
         */
        //print_r($datosCliente);
        
        //consultamos los datos del producto
        $datosProducto = $this->productos_consultarDetallePrimerProductoPorColor($id_producto, $colorSeleccionado);
        //print_r($datosProducto);
        /*
         * Array
            (
                [id] => 1
                [id_producto] => SM5898
                [descripcion] => Sudadera dama
                [detalles] => Luce sensacional con esta increible sudadera
                [estado] => ACTIVO
                [existencia] => 0
                [color] => Gris
                [noColor] => rgb(142, 142, 142)
                [talla] => UNT
                [precio_etiqueta] => 339
                [precio_vendedora] => 298
                [precio_socia] => 295
                [precio_empresaria] => 291
                [precio_inversionista] => 280
                [imagen1] => fotos/SM5898-VERDE-1.jpg
                [imagen2] => fotos/SM5898-VERDE-2.jpg
                [imagen3] => fotos/SM5898-VERDE-3.jpg
                [imagen_permanente] => fotos/SM5898-VERDE-1.jpg
                [categoria] => 
            )
         */
        
        $statetment = $this->conexion->prepare("INSERT INTO carritos_clientes "
                        . "(fecha_entrega_pedido, no_cuenta, nombre_cliente, credito_cliente, grado_cliente, ruta, asignado,"
                        . " id_producto, descripcion, talla, cantidad, color, no_color, precio_etiqueta, precio_vendedora,"
                        . "precio_socia, precio_empresaria, precio_inversionista, imagen_permanente, producto_confirmado, estado_producto,"
                        . "seguimiento_producto, plataforma, fecha_captura)"
                        . "VALUES"
                        . "(:fechaEntrega, :noCuenta, :nombre, :credito, :grado, :zona, :asignado,"
                        . ":idProducto, :descripcion, :talla, :cantidad, :color, :noColor, :precioEtiqueta, :precioVendedora,"
                        . ":precioSocia, :precioEmpresaria, :precioInversionista, :imagenPermanente, :productoConfirmado, :estadoProducto, "
                        . ":seguimientoProducto, :plataforma, :fecha_captura)");
        
        $statetment->bindParam(":fechaEntrega", $fecha);
        $statetment->bindParam(":noCuenta", $numeroCuenta);
        $statetment->bindParam(":nombre", $datosCliente["nombre"]);
        $statetment->bindParam(":credito", $datosCliente["credito"]);
        $statetment->bindParam(":grado", $datosCliente["grado"]);
        $statetment->bindParam(":zona", $nombreZona);
        $statetment->bindParam(":asignado", $asignado);
        
        $statetment->bindParam(":idProducto", $id_producto);
        $statetment->bindParam(":descripcion", $datosProducto["descripcion"]);
        $statetment->bindParam(":talla", $tallaSeleccioanda);
        $statetment->bindParam(":cantidad", $cantidadSeleccionada);
        $statetment->bindParam(":color", $datosProducto["color"]);
        $statetment->bindParam(":noColor", $datosProducto["noColor"]);
        $statetment->bindParam(":precioEtiqueta", $datosProducto["precio_etiqueta"]);
        $statetment->bindParam(":precioVendedora", $datosProducto["precio_vendedora"]);
        
        $statetment->bindParam(":precioSocia", $datosProducto["precio_socia"]);
        $statetment->bindParam(":precioEmpresaria", $datosProducto["precio_empresaria"]);
        $statetment->bindParam(":precioInversionista", $datosProducto["precio_inversionista"]);
        $statetment->bindParam(":imagenPermanente", $datosProducto["imagen_permanente"]);
        $statetment->bindParam(":productoConfirmado", $productoConfirmado);
        $statetment->bindParam(":estadoProducto", $estado);
        
        $statetment->bindParam(":seguimientoProducto", $mensajeProducto);
        $statetment->bindParam(":plataforma", $plataforma);
        $statetment->bindParam(":fecha_captura", $fechaCaptura);
        
        
               
        $productoConfirmado = "true";
        $estado = "CREDITO";
        $mensajeProducto = "El producto ha sido apartado en almacen";   
        $fecha = $this->carrito_crearFechaEntregaPedido($numeroCuenta);
        $plataforma = "appAndroid";
        $fechaCaptura = utilitarios_Class::fechaActualParaBasesDeDatos();        
        $nombreZona = $datosCliente["zona"];
        $asignado = $this->sucursal_consultarAsignado($nombreZona);
  
        
        
        if($statetment->execute()){
            //echo "Insersion correcta";
            return true;

        }else{
            //echo "Insersion incorrecta";
            return false;
        }
        
       
        
    }
    
    /**
     *Se encarga de gestionar si el producto que se añadira debera entrar en la misma fecha o una fecha nueva + los dias de credito del cliente
     * @param string $numeroCuenta
     * @return string $fechaEntrega 
     */
    public function carrito_crearFechaEntregaPedido ($numeroCuenta){
        
        
        //consultamos los datos del cliente para la fecha futura
        $stringFechaActual = utilitarios_Class::fechaActualYYYY_mm_dd()->format("d-m-Y"); //le hago un format para tener en claro los dias y no andar batallando con las horas, porque devuelve horas y minutos y eso afecta cuando haces una resta ya que puede marcarte 0 dias de diferencia pero aun tienes horas y hay que sumarle y se vuevle poco claro
        $stringFuturaVisita = $this->clientes_consultarFechaFuturaVisita($numeroCuenta);
        $stringFechaCierre = $this->clientes_consultarFechaCierrePedido($numeroCuenta); //solo como guia
        $diasAntesPedido = $this->clientes_consultarDiasAntesPedido($numeroCuenta);
        
        /*
        $stringFechaActual = "16-07-2021";
        $stringFuturaVisita = "21-07-2021";
        echo "fecha Actual: $stringFechaActual\n";
        echo "fecha futura visita: $stringFuturaVisita\n";
        echo "fecha cierre: $stringFechaCierre\n";
        echo "diasAntes pedido: $diasAntesPedido\n";
        */

        $fechaActual = date_create($stringFechaActual, timezone_open("America/Mexico_City"));
        $fechaEntrega = date_create($stringFuturaVisita, timezone_open("America/Mexico_City"));   //importante ambas fechas deben estar en la misma zona horaria         
        //print_r($fechaActual);
        //print_r($fechaEntrega);


        $dif = $fechaActual->diff($fechaEntrega);
        //print_r($dif);
        /* https://www.php.net/manual/es/class.dateinterval.php
         * si la fecha actual es menor a la fecha de entrega indica cuantos dias y el campo invert en 0 quiere decir son positivos como si a la fecha de entrega se le descontara la fecha actual
         * si la fecha actual es mayor a la fecha de entrega indica cuantos dias  el campo invert se pone en 1 es decir son numeros negativos, son los dias u horas pasados de la fecha de entrega
         * aqui por ejemplo preferi hacer el format de la fecha actual a que solo diera dia mes y año y no minutos y segundos nos entrega esto limpio
         * (
          [y] => 0
          [m] => 0
          [d] => 7
          [h] => 0
          [i] => 0
          [s] => 0
          [f] => 0
          [weekday] => 0
          [weekday_behavior] => 0
          [first_last_day_of] => 0
          [invert] => 0
          [days] => 7
          [special_type] => 0
          [special_amount] => 0
          [have_weekday_relative] => 0
          [have_special_relative] => 0
          )
         * si no nos entregaria lo de abajo
         * DateInterval Object
          (
          [y] => 0
          [m] => 0
          [d] => 6
          [h] => 9
          [i] => 22
          [s] => 20
          [f] => 0.926375
          [weekday] => 0
          [weekday_behavior] => 0
          [first_last_day_of] => 0
          [invert] => 0
          [days] => 93
          [special_type] => 0
          [special_amount] => 0
          [have_weekday_relative] => 0
          [have_special_relative] => 0
          )
         */
        $dias = $dif->d;


        //si la fecha actual es mayor a la fecha de entrega del ultimo producto por almenos 1 dia o minutos (los minutos ya no improtan porque hice el format a solo dias y meses en la fecha actual solo es para que veas el problema de incluir minutos y segudnso que no es problema pero se vuelve mas revoltoso)
        if ($dif->invert) {
            /* si invert es true(1) entonces la fecha actual es mayor a la fecha de entrega pero cuidado porque si una fecha esta dada 
             * con horas minutos 23-02-2020 09:42:35
             * y la otra esta como 23-02-2020 tambien afectara el invert. aunque en este caso no nos afecta ya porque preferi hacer el format arriba para que solo entregara dia mes y año

             * Si la fecha actual es mayor al menos por 1 segundo a la fecha de entrega dada por kaliope admin
             * quiere decir que los productos que se anexen al carrito ahora deberan de entrar con la fecha de futura visita + 14dias,
             *  porque el anterior ya va en camino de entrega o ya fue entregado
             * 
             * 

             */
            //echo "El ultimo pedido ya fue entregado y no se ha recibido informacion en administracion cuando el cliente ya esta añadiendo productos, se fija la fecha de entrega en 15 dias mas";
            return $fechaEntrega->add(new DateInterval("P14D"))->format("Y-m-d"); //entregamos en string con este formato para que sea aceptado por la base de datos
        } else {

            /*
             * la fecha actual es menor a la fecha de entrega ahora debemos ver por cuantos dias es menor a la fecha de entrega
             * si es menor por 6 di
             * o que la haya pasado, si la fecha actual es menor a la fecha de cierre por 1 dia, entonces el producto podra ser añadido a la fecha de visita
             * si la fecha actual es igual a fecha de cierre es decir 0 dias, entonces el producto sera añadido con la fecha de visita + 15 dias
             * si la fecha actual a pasado la de cierre es decir invert es 1 entonces el producto sera añadido con la fecha de visita + 15 dias
             */
            
            if ($dias <= $diasAntesPedido) {
                //echo "hoy es $stringFechaActual se esta entregando hoy o aun no se llega la fecha de entrega $stringFuturaVisita de este pedido, pero ya esta en dias bloqueo $diasAntesPedido  es decir ya estamos o pasamos la fecha $stringFechaCierre por tanto este producto ya no puede entrar al pedido del $stringFuturaVisita este producto se entregara en 15 dias mas";
                return $fechaEntrega->add(new DateInterval("P14D"))->format("Y-m-d"); //entregamos en string con este formato para que sea aceptado por la base de datos 
            } else {
                //echo "aun no se llega la fecha de entrega del ultimo pedido y aun estamos fuera de los dias de bloqueo, este pedido se entregara en la fecha mas reciente $stringFuturaVisita";
                return $fechaEntrega->format("Y-m-d"); //entregamos en string con este formato para que sea aceptado por la base de datos
            }
            
            
        }
    }
    
    public function carrito_sePuedeAgregarProducto($numeroCuenta){
        /*
         * Comprobaremos si podemos añadir un nuevo producto al carrito del cliente
         * esto en base a la fecha de entrega que el cliente tiene programada en su area administrativa
         * 
         * si la fecha actual en que el cliente esta intentando añadir un producto a su carrito es anterior
         * a su fecha de visita futura por los dias que imponga dias_antes_pedido entonces dejamos que añada su producto
         * 
         * si la fecha actual que añade su producto es menor a la fecha de visita futura, pero ya esta muy proxima tanto que dias_antes_pedido
         * es mayor a los dias de diferencia entonces no dejamos que el cliente añada su producto
         * 
         * si la fecha actual que añade su pedido son en el mismo dia de su fecha de entrega entonces prohibimos que se añada el producto
         * 
         * si la fecha actual es mayor a su fecha de visita futura tendremos que prohibir la entrada porque esto significa que
         * no se han recibido los datos en el area administrativa y no se ha creado una nueva fecha futura con datos recientes
         * 
         * la fecha actual no puede ser nunca mayor a su fecha de visita futura, porque se supone que cuando el agente llega a bodega y 
         * se recibe su informacion en automatico la fecha futura nueva sera mayor siempre a la fecha actual
         */
        //$fechaFuturaVisita = date_create("20-02-2021 00:00:00", timezone_open("America/Mexico_City"));
        $fechaFuturaVisita = date_create($this->clientes_consultarFechaFuturaVisita($numeroCuenta), timezone_open("America/Mexico_City"));
        $fechaActual = utilitarios_Class::fechaActualYYYY_mm_dd();
        //$fechaActual = date_create("16-02-2021 08:00:00", timezone_open("America/Mexico_City"));
        
        $fechaCierre = $this->clientes_consultarFechaCierrePedido($numeroCuenta);
        $stringFechaFuturaVisita = $fechaFuturaVisita->format("d-m-Y");                             //convertimos la fecha a string para mostrarla en mensaje
        

        //print_r($fechaFuturaVisita);
        //print_r($fechaActual);
       
       

        $dif = $fechaActual->diff($fechaFuturaVisita);
        /* si la fecha actual es menor a la fecha futura visita indica cuantos dias y el campo invert en 0 quiere decir son positivos como si a la fechaFuturaVisita se le descontara la fecha actual
         * si la fecha actual es mayor a la fechaFuturaVisita indica cuantos dias  el campo invert se pone en 1 es decir son numeros negativos, son los dias u horas pasados de la fechaVisitaFutura
         */
        //print_r($dif);
        /* https://www.php.net/manual/es/class.dateinterval.php
         * DateInterval Object
          (
          [y] => 0
          [m] => 3
          [d] => 1
          [h] => 9
          [i] => 22
          [s] => 20
          [f] => 0.926375
          [weekday] => 0
          [weekday_behavior] => 0
          [first_last_day_of] => 0
          [invert] => 1
          [days] => 93
          [special_type] => 0
          [special_amount] => 0
          [have_weekday_relative] => 0
          [have_special_relative] => 0
          )
         */
        $dias = $dif->days;
        $invert = $dif->invert;
        
        $diasAntesPedido = $this->clientes_consultarDiasAntesPedido($numeroCuenta);
        
        if($invert){
            /*
             * Si invert es true(1) significa que la fecha actual es mayor a la fechaVisitaFutura, incluso pueden estar el mismo dia pero
             * como la fecha actual es dada por el sistema esta tendra horas minutos y segundos mientras que la fechaFutura al ser dada
             * por la base de datos, sera creada con horas en 00:00:00
             * fecha futura 23-02-2021 00:00:00
             * fecha actual 23-02-2021 09:23:00
             * 
             * esto proboca que invert se coloque en true, indicando que son numeros negativos, porque la fecha actual es mayor a la futura
             * la diferencia es que $dias sera igual a 0, indicando que no hay diferencia de dias, pero si de horas por tanto validaremos
             * sobre $dias
             */
            
            if($dias>0){
                //si la fecha actual es mayor por almenos 1 dia a la fecha futura esto puede ocurrir porque aun no se recibe la informacion del agente
                
                //Prohibimos el ingreso del producto debido a que la fecha de actual no puede ser mayor a la fecha futura indicando la falta
                //de recibir datos del agente en el area administrativa

                throw new Exception("No se puede agregar el producto a tu carrito debido a que aun no se recibe la informacion de tu agente en oficina, por favor espera. (Si este mensaje persiste por mas de 1 dia por favor comuniquese a Kaliope)");
            }else{
                //si la fecha actual es mayor por horas a la fecha actual, es decir estamos en el mismo dia
                throw new Exception("No se puede agregar el producto a tu carrito. Tienes un pedido que sera entregado el dia de hoy, por favor espera    (Si nota que este mensaje persiste por mas de 1 dia por favor comuniquese a Kaliope)");
            }
        }else{
            //si invert es false(0) significa que la fecha actual que el cliente esta añadiendo su producto, es menor a la fecha de visita futura
            //comprobaremos que se amenor al minimo de dias indicada por dias_antes_pedido
            
            if($dias>=$diasAntesPedido){
                //si la diferencia de dias es mayor a dias antes pedido, indica que aun tenemos tiempo para agregar producto al carrito
                //no retornamos ninguna excepcion
            }else{
                //si la diferencia en dias ya esta muy cerca de su fecha de entrega entonces no permitimos la entrada del producto

                throw new Exception("No puedes agregar mas productos a tu carrito por ahora. Debes agregar los productos $diasAntesPedido".($diasAntesPedido>1?" dias":" dia")." antes de tu fecha de entrega. Tu pedido actual fue cerrado el $fechaCierre porque sera entregado el $stringFechaFuturaVisita. Es necesario esperar a que tu ultimo pedido sea entregado.");
            }
        }
    }
    
    public function carrito_consultarCarrito($numeroCuenta) {
        /*
         * Consultamos la fecha del pedido mas actual que existe en la tabla de carrito a ese numero de cliente
         * es decir el pedido mas reciente que existe 
         * 
         * Añadiremos a cada producto del carrito del cliente otros campos que antes calculabamos en la app kaliop
         * esto nos ayudara a tener menos codigo en la app kaliope.
         * 
         * añadiremos
         * ganancia -> no esta guardada por default en la tabla, aqui influyira el grado del cliente
         * gananciaInversionista-> se calcula una ganancia independiente en inversionista para obtener una comparacion entre credito y inversion
         * 
         */
        
        $fechaMasReciente = $this->carrito_consultarPedidoMasReciente($numeroCuenta);
        //obtenido la fecha del pedido lo usaremos para consutlar los productos que tengan ese numero de pedido
        
        $consulta = $this->conexion->prepare(
                "SELECT * FROM carritos_clientes WHERE no_cuenta=? AND fecha_entrega_pedido=?"
                );
        $consulta->execute([$numeroCuenta, $fechaMasReciente]);
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
        //print_r($resultado);
        /*
         * Array
            (
                [0] => Array
                    (
                        [id] => 100
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
                    )

                [1] => Array
                    (
                        [id] => 101
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
                        )
        )
         */
       
    
        if(empty($resultado)){
            return null;
        }
        

        for ($i=0; $i<sizeof($resultado); $i++) {
            $producto = $resultado[$i];
            $gradoCliente = $producto["grado_cliente"];
            $precioEtiqueta = $producto["precio_etiqueta"];
            $id_producto = $producto["id_producto"];
            $noColorSeleccionado = $producto["no_color"];
            $tallaSeleccionada = $producto["talla"];
                      
            
            
            
            
            $ganancia =0;
            $gananciaInversion=0;
            
            if($gradoCliente == "VENDEDORA"){
                $ganancia = $precioEtiqueta - $producto["precio_vendedora"];
                
            }else if($gradoCliente == "SOCIA"){
                $ganancia = $precioEtiqueta - $producto["precio_socia"];

            }else{
                $ganancia = $precioEtiqueta - $producto["precio_empresaria"];
                
            }
            
            $gananciaInversion = $precioEtiqueta - $producto["precio_inversionista"];
            
                        
            
            
            $producto["ganancia"] = $ganancia;
            $producto["ganancia_inversion"] = $gananciaInversion;
  
            
         
            $resultado[$i] = $producto;             //ingresamos el nuevo array con 2 items mas agregados al array principal
            
        }
        
        //print_r($resultado);
        /*
         * Array
            (
                [0] => Array
                    (
                        [id] => 100
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
         *              [ganancia] => 21
         *              [ganacia_inversion] => 32
                    )

                [1] => Array
                    (
                        [id] => 101
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
         *              [ganancia] => 21
         *              [ganacia_inversion] => 32

                        )
        )
         */
        
        return $resultado;
           
        
    }    
   
    /**
     * Devuelve la fecha del numero de pedido mas reciente del cliente
     * @param String $numeroCuenta 
     * @return string si hay productos entonces retornamos la fecha mas avanzado, <p> si no hay productos retornamos ""
     */
    public function carrito_consultarPedidoMasReciente($numeroCuenta){
        
        $consulta = $this->conexion->prepare(
                "SELECT fecha_entrega_pedido FROM carritos_clientes WHERE no_cuenta=? ORDER BY fecha_entrega_pedido DESC LIMIT 1"
                );
        
        $consulta->execute([$numeroCuenta]);
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
                
        //print_r($resultado);
        /*
         * Array
            (
                [fecha_entrega_pedido] => 2021-06-01
            )
         */
        
        if(!empty($resultado)){
            //si hay productos registrados en el carrito del cliente entonces retornamos el numero
            return $resultado["fecha_entrega_pedido"];
        }else{
            //si no hay ningun producto en el carrito retornamos 0;
            return 0;
        }
        
            
        
    }    
    
    public function carrito_editarProductoMetodoPago($idRenglonProducto, $metodoPago){
        $statement = $this->conexion->prepare("UPDATE carritos_clientes SET estado_producto=? WHERE id=?");
        
           
        if($statement->execute([$metodoPago, $idRenglonProducto])){
            return true;
        }else{
            return false;
        }
        
                
    }
    
    public function carrito_eliminarProducto($idRenglonProducto){
        $statement = $this->conexion->prepare("DELETE FROM carritos_clientes WHERE id=?");
        
        
        $detallesProducto = $this->carrito_consultarProductoPorRenglon($idRenglonProducto);   //debemos consultar la cantidad id_producto talla y color, claro esta antes de que se elimine el producto de la tabla del carrito osea antes del execute. ya que de ahi se obtienen estos datos. y obvio si se ejecuta primero el eliminar, cuando consulte ya no encontrara el producto

        if($statement->execute([$idRenglonProducto])){
            //si se elimina correctamente vamos a usar los datos que consultamos desde antes de eliminar para incrementar las exsitencias en el inventario
           $this->productos_existenciasIncrementarExistencias($detallesProducto["id_producto"], $detallesProducto["no_color"], $detallesProducto["talla"], $detallesProducto["cantidad"]);

           return true;
        }else{
            return false;
        }
        
        
         
        
        
    }
    
    /**
     * Auxiliar por actualizacion del sistema, necesito esto para actualizar las existencias al eliminar el producto
     * antes al eliminar no pasaba nada pero con la actualizacion necesitamos aumentar otra vez las existencias del sistema
     * el problema es que la app cuando elimina o edita algun producto solamente nos envia como parametro el $id de la base de datos
     * pero no envia el color ni id, ni tampoco la talla que son necesarias para poder usar el metodo que incrementa las existencias
     * por tanto deberemos consultar primero en el carrito esos datos con ese renglon de la tabla. guardarlos para despues
     * enviarlos al metodo que incrementa o decrementa existencias
     * @param int $idRenglonProducto
     * @return array[] <p> [id_producto] <p> [no_color] <p> [talla] <p> [cantidad]
     */
    public function carrito_consultarProductoPorRenglon(int $idRenglonProducto){
        
        $consulta = $this->conexion->prepare("SELECT id_producto, no_color, talla, cantidad FROM carritos_clientes WHERE id=?");
        $consulta->execute([$idRenglonProducto]);
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        //print_r($resultado);
        /*
         * Array
            (
                [id_producto] => PZ3003
                [no_color] => rgb(25, 20, 58)
                [talla] => UNT
                [cantidad] => 1
            )
         */
        return $resultado;       
       
        
    }
    
    /**
     * Calcula los totales del ultimo carrito del cliente
     * @param string $numeroCuenta
     * @return array[] <p> nombreCliente
     *                  <p>cuenta
     * <p>limite_credito
     * <p>grado
     * <p>dias
     * <p>ruta
     * <p>porcentaje_apoyo_empresa
     * <p>porcentaje_pago_cliente
     * <p>numero_pedido
     * <p>fecha_entrega
     * <p>fecha_pago_del_credito
     * <p>suma_cantidad
     * <p>suma_credito
     * <p>suma_inversion
     * <p>cantidad_sin_confirmar
     * <p>suma_productos_etiqueta
     * <p>suma_productos_inversion
     * <p>suma_productos_credito
     * <p>suma_ganancia_cliente
     * <p>diferencia_credito
     * <p>cantidad_pagar_cliente_credito
     * <p>pago_al_recibir
     * <p>mensaje_diferencia_credito
     * <p>mensaje_todo_inversion
     * <p>mensaje_resumido_puntos
     * <p>mensaje_completo_puntos
     * <p>mensaje_cantidad_sin_confirmar
     */
    public function carrito_calcularTotales($numeroCuenta) {
        /*
         * Este metodo lo creo Mi hermoano Jorge
         */
        
        $datos_pedido = $this->carrito_consultarCarrito($numeroCuenta);         //esta es la unica diferencia, añadi este metodo aqui
        //print_r($datos_pedido);
        /*
         * [1] => Array
                    (
                        [id] => 101
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
                        [diferencia_regalo] => 0
                        [puntos_tomados] => 0
                    )
         */        
    
        
          
            if($datos_pedido==null){
                return ['nombre' => "",
            'cuenta' => "",
            'limite_credito' => "",
            'grado' => "",
            'dias' => "",
            'ruta' => "",
            'porcentaje_apoyo_empresa' => "0",
            'porcentaje_pago_cliente' => "0",
            'fecha_entrega' => "",
            'fecha_pago_del_credito' => "",
            'suma_cantidad' => "",
            'suma_credito' => "",
            'suma_inversion' => "",
            'cantidad_sin_confirmar' => "",
            'suma_productos_etiqueta' => "",
            'suma_productos_inversion' => "",
            'suma_productos_credito' => "",
            'suma_ganancia_cliente' => "",
            'diferencia_credito' => "",
            'cantidad_pagar_cliente_credito' => "",
            'cantidad_financiar_empresa' => "",
            'pago_al_recibir' => "",
            'mensaje_diferencia_credito' => "",
            'mensaje_todo_inversion' => "",
            'mensaje_resumido_puntos' => "",
            'mensaje_completo_puntos' => "",
            'mensaje_cantidad_sin_confirmar' => ""
            ];
            }
            
       
        
        $datos_cliente = $this->clientes_consultarCliente($numeroCuenta);       //consultamos los datos del cliente en kaliopeAdmin para obtener la ruta y los dias de credito
        /*
         Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020                   Es la fecha de visita siguiente a menos que se cree una fecha futura como el cachorrito sabe hacerlo xD
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
            )
         */
        $porcentaje_financiacion = $this->usuarios_consultar_porcentaje_financiacion($numeroCuenta);
        $porcentaje_empresa = $porcentaje_financiacion["porcentaje_apoyo_empresa"];             //lo que financiara la emrpesa del total del credito 0.6  = 60%
        $porcentaje_cliente = $porcentaje_financiacion["porcentaje_pago_cliente"];              //lo que el cliente debera pagar al recibir su producto a credito 0.4 = 40%
        /*
         * Array
            (
                [porcentaje_apoyo_empresa] => 0.5
                [porcentaje_pago_cliente] => 0.5
            )
         */

        $suma_cantidad = 0;
        $suma_cantidad_credito = 0;                                             //la cantidad de piezas en credito
        $suma_cantidad_inversion = 0;                                           //la cantidad de piezas en inversion
        $suma_productos_etiqueta = 0;                                           //la suma del costo en etiqueta
        $suma_productos_credito = 0;                                            //el importe de los produictos a credito
        $suma_productos_inversion = 0;                                          //el importe de productos de inversion
        $suma_ganancia_cliente = 0;                                       //la ganancia del cliente por todo el pedido, dependiendo de su grado, y de cuantos productos tiene en inversion
        $suma_ganancia_todo_inversion = 0;                                  //calcularemos la ganancia de todos los productos sin importar credito, esto para crearle un mensaje donde diga Si cambiaras todo tu pedido a inversion ganarias $550 en lugar de 400
        $credito_cliente = $datos_pedido[0]['credito_cliente'];
        $nombre_cliente = $datos_pedido[0]['nombre_cliente'];
        $grado_cliente = $datos_pedido[0]['grado_cliente'];
        $fecha_temporal = date_create($datos_pedido[0]['fecha_entrega_pedido'], timezone_open("America/Mexico_City")) ; //quiero formatear la fecha de 2021-05-30  a 30-05-2021
        $fecha_entrega = $fecha_temporal->format("d-m-Y"); //formateamos la fecha
        $dias_credito = $datos_cliente["dias"];
        $zona = $datos_cliente["zona"];
        
       



        foreach ($datos_pedido as $producto) {

            $cantidad = $producto['cantidad'];
            $precio_etiqueta = $producto['precio_etiqueta'];
            $precio_vendedora = $producto['precio_vendedora'];
            $precio_socia = $producto['precio_socia'];
            $precio_empresaria = $producto['precio_empresaria'];
            $precio_inversionista = $producto['precio_inversionista'];
            $grado_cliente = $producto['grado_cliente'];

            if ($producto['estado_producto'] == 'AGOTADO') {
                //si el producto esta clasificado como agotado asignamos en 0 y no se sumara a ningun total
                $cantidad = 0;
                $precio_vendedora = 0;
                $precio_socia = 0;
                $precio_empresaria = 0;
            }
            
            
            $suma_cantidad += $cantidad;
            $importe_temporal_precio_etiqueta = $cantidad * $precio_etiqueta;           //en cada producto pueden ser 1 o 2 o 3 piezas, este total se le quitara el precio de distribucion para conocer la ganancia del cliente

            $suma_productos_etiqueta += $importe_temporal_precio_etiqueta;
            
            


            if ($producto['estado_producto'] == 'CREDITO') {

                $suma_cantidad_credito += $cantidad;                

                //dependiendo de que grado sea el cliente, calcularemos el total de su importe a credito, e iremos sumando tambien su ganancia
                
                switch ($grado_cliente) {
                    case 'VENDEDORA':
                        
                        $importe_temporal_precio_dis = $cantidad * $precio_vendedora;
                        $suma_productos_credito += $importe_temporal_precio_dis;
                        $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
                        break;
                    case 'SOCIA':
                        
                        $importe_temporal_precio_dis = $cantidad * $precio_socia;
                        $suma_productos_credito += $importe_temporal_precio_dis;                      
                        $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
                        break;
                    case 'EMPRESARIA':
                        $importe_temporal_precio_dis = $cantidad * $precio_empresaria;
                        $suma_productos_credito += $importe_temporal_precio_dis;                       
                        $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
                        break;
                    default:
                        $suma_productos_credito = 0;
                        break;
                }
            }
            
            
            

            if ($producto['estado_producto'] == 'INVERSION') {
                $suma_cantidad_inversion += $cantidad;
                
                $importe_temporal_precio_dis = $precio_inversionista * $cantidad;
                $suma_productos_inversion += $importe_temporal_precio_dis;        
                
                $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
            }
            
            //sumamos todo producto sin importar sea credito o inversion al total de inversion esto para resolver lo del mensaje para motivar al cleinte a cambiarse a inversion
            $importe_temporal_precio_dis = $precio_inversionista * $cantidad;
            $suma_ganancia_todo_inversion += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
            
            
        }
        
        
        
        // Calculamos si su ccredito a sobrepasado su limite de credito y en base a ello le enviaremos mensajes determinados      
        
        $diferencia_credito = $suma_productos_credito - $credito_cliente;

        if ($diferencia_credito < 0) {     
            // si el valor es negativo significa que el cliente NO ha sobre pasado su limite de credito
            $temporal = $diferencia_credito *- 1;
            $mensaje_diferencia_credito = "Aun dispones de $$temporal en tu credito Kaliope";

        }else if($diferencia_credito > 0){
            //si el importe en credito ha rebasado el limite de credito del cliente obtendremos un valor positivo en la diferencia de credito
            
            if($diferencia_credito<=100){
                $mensaje_diferencia_credito = "Tu total de credito ha pasado por $$diferencia_credito tu limite de credito, le recomendamos cambiar alguna pieza a Inversion o elimine productos";
            }else if($diferencia_credito>100){
                $mensaje_diferencia_credito = "Tu total de credito ha pasado por $$diferencia_credito tu limite de credito, deberas dar esa diferencia en efectivo al recibir tu paquete, tambien puedes cambiar un producto a Inversion o eliminar productos";
            }
        }
        
        
        
        
        
        
        //calculamos el mensaje de la ganancia a todo inversion
        //si el cliente tiene mercancia a credito añadida mostraremos los mensajes
        if($suma_cantidad_credito>=2){
            //si el cliente tiene 1 o 2 piezas añadidas en credito nos conviene enviarle el mensaje
            $mensaje_todo_inversion = "Si cambias toda tu compra a Inversion ganarás $$suma_ganancia_todo_inversion";
        }else{
            //si no tiene piezas a credito mostraremos el mensaje vacion
            $mensaje_todo_inversion = "";
        }
        

        
        //calculamos la cantidad que el cliente debe de pagar acorde a su porcentarje de financiacion REDONDEAMOS los numeros ya que en ocaciones saca 525.99999999542
        /*
         * Tenemos una manera erronea de calcular esto, pasa lo siguiente si el el cliente rebalsa su limite por ejemplo su credito es de 2400
         * y el cliente agrega 3102 en credito la diferencia de credito sera 702 que el cliente debera cubrir
         * ahora el financiamiento del cliente por ejemplo el 70% sera de 3102*.7=2171.5 quedando pendientes 930.6
         * el sistema le dira que debe de pagar 2171.5+702 = 2873, esto ya no es el 70% y ademas se supne que pagara el exceso que son 702 por lo tanto ya no deberian quedar 930.6 financiados
         * 
         * para corregir esto haremos lo siguiente, si el limite de credito no fue revalsado todo se queda tal cual esta bien calculado
         * pero si el limite de credito fue revalsado entonces ahora en lugar de multiplicar el total del credito por el 70% solo vamos a multiplicar
         * el limite de credito actual. es decir tendra que pagar el 70% de sus 2400, son 1680 quedando un restante de 2400*30% = 720 para pagarce en 15 dias
         * y ahora si tendra que generar un pago por su diferencia de 702 entonces el cliente ahora si tendra el 70% y las sumas concuerdan
         *      2400*70% = 1680 
         * +    2400*30% = 720
         * +    702 diferencia credito
         * _______________________________
         * 3102 que es el total de la mercancia a credito
         */
        if($diferencia_credito<=0){
            //si no hay diferencia de credito o el credito no ha sido revalsado dejamos todo el calculo por el total del credito
            $cantidadPagarClienteCredito = round(($suma_productos_credito*$porcentaje_cliente),0,PHP_ROUND_HALF_DOWN) ;
            $cantidadFinanciarEmpresa = round(($suma_productos_credito*$porcentaje_empresa),0,PHP_ROUND_HALF_DOWN) ;
        }else{
            //si el credito ya fue revalsado es decir tiene valores positivos usamos el limite de credito del cliente
            $cantidadPagarClienteCredito = round(($credito_cliente*$porcentaje_cliente),0,PHP_ROUND_HALF_DOWN) ;
            $cantidadFinanciarEmpresa = round(($credito_cliente*$porcentaje_empresa),0,PHP_ROUND_HALF_DOWN) ;
        }
        
        //calculamos el total de pago que el cliente debe dar al recibir el pedido cuidado porque la diferencia de credito sale en negativo si el cliente tiene aun credito disponible
        //entonces solo deberemos sumarlo si el valor es mayor a 0 es decir sobrepaso su limite de credito
        if($diferencia_credito>0){
            $pagoAlRecibir = $suma_productos_inversion + $diferencia_credito + $cantidadPagarClienteCredito;
        }else{
            $pagoAlRecibir = $suma_productos_inversion + $cantidadPagarClienteCredito;
        }
        
        //calculamos la fecha de vencimiendo del credito con la fecha de entrega del pedido sumando los dias de credito
        $fechaPagoDelCredito = utilitarios_Class::sumarDiasFecha($fecha_entrega,$dias_credito);
        
        


        $respuesta = [
            'nombre' => $nombre_cliente,
            'cuenta' => $numeroCuenta,
            'limite_credito' => $credito_cliente,
            'grado' => $grado_cliente,
            'dias' => $dias_credito,
            'ruta' => $zona,
            'porcentaje_apoyo_empresa' => $porcentaje_empresa,
            'porcentaje_pago_cliente' => $porcentaje_cliente,
            'fecha_entrega' => $fecha_entrega,
            'fecha_pago_del_credito' => $fechaPagoDelCredito,
            'suma_cantidad' => $suma_cantidad,
            'suma_credito' => $suma_cantidad_credito,
            'suma_inversion' => $suma_cantidad_inversion,
            'suma_productos_etiqueta' => $suma_productos_etiqueta,
            'suma_productos_inversion' => $suma_productos_inversion,
            'suma_productos_credito' => $suma_productos_credito,
            'suma_ganancia_cliente' => $suma_ganancia_cliente,
            'diferencia_credito' => $diferencia_credito,
            'cantidad_pagar_cliente_credito' => $cantidadPagarClienteCredito,
            'cantidad_financiar_empresa' => $cantidadFinanciarEmpresa,
            'pago_al_recibir' => $pagoAlRecibir,
            'mensaje_diferencia_credito' => $mensaje_diferencia_credito,
            'mensaje_todo_inversion' => $mensaje_todo_inversion,
        ];

        //print_r($respuesta);
        /*
         * Array
        (
            [nombre] => MONICA HERNANDEZ GARCIA
            [cuenta] => 4926
            [limite_credito] => 1400
            [grado] => VENDEDORA
            [dias] => 14
            [ruta] => EL PALMITO
            [porcentaje_apoyo_empresa] => 0.35
            [porcentaje_pago_cliente] => 0.65
            [fecha_entrega] => 2021-05-15
            [suma_cantidad] => 8
            [suma_credito] => 6
            [suma_inversion] => 2
            [cantidad_sin_confirmar] => 3
            [suma_productos_etiqueta] => 2822
            [suma_productos_inversion] => 560
            [suma_productos_credito] => 1800
            [suma_ganancia_cliente] => 462
            [diferencia_credito] => 400
            [cantidad_pagar_cliente_credito] => 1170
            [pago_al_recibir] => 2130
            [mensaje_diferencia_credito] => Tu total de credito ha pasado por $400 tu limite de credito, deberas dar esa diferencia en efectivo al recibir tu paquete, tambien puedes cambiar un producto a Inversion o eliminar productos
            [mensaje_todo_inversion] => Si pagaras todo tu pedido en Inversion ganarias $582
            
        )
         */
        return $respuesta;
    }
    
    /**DEPRECATED  YA NO SE USARA ELIMINAR CUANDO LO DESEES
     * Consultamos los productos que esten marcados sin confirmar y que no esten marcados como agotados
     * usando el numero de pedido mas reciente del cliente
     * @param string $numeroCuenta
     * @return array[[]] obtenemos todos los productos que no esten confirmados o agotados
     */
    public function carrito_consultar_sin_confirmar(string $numeroCuenta){
        $fechaPedidoMasReciente = $this->carrito_consultarPedidoMasReciente($numeroCuenta);
        
        $consulta = $this->conexion->prepare("SELECT cantidad, id_producto, descripcion, color, precio_etiqueta, imagen_permanente, estado_producto FROM carritos_clientes WHERE fecha_entrega_pedido=? AND no_cuenta=? AND producto_confirmado=? AND estado_producto<>?");
        $consulta->execute([$fechaPedidoMasReciente,$numeroCuenta, "false", "AGOTADO"]);            //en la tabla se esta guardando como varchar true or false, y filtramos que el producto no este sin confirmar pero como agotado
        $resultado = $consulta->fetchAll(PDO::FETCH_ASSOC);
        //print_r($resultado);
        /*
         * Array
            (
                [0] => Array
                    (
                        [cantidad] => 1
                        [id_producto] => PD5898
                        [descripcion] => Pantalon Dama
                        [color] => AZUL
                        [precio_etiqueta] => 429
                        [imagen_permanente] => fotos/PD5898-AZUL-1.jpg
                        [estado_producto] => INVERSION
                    )

                [1] => Array
                    (
                        [cantidad] => 1
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [color] => Gris
                        [precio_etiqueta] => 339
                        [imagen_permanente] => fotos/SM5898-VERDE-1.jpg
                        [estado_producto] => CREDITO
                    )

                [2] => Array
                    (
                        [cantidad] => 1
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [color] => Rosa
                        [precio_etiqueta] => 339
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [estado_producto] => CREDITO
                    )

            )
         */
        
        return $resultado;
    }
   /**DEPRECATED YA NO SE USARA ELIMINAR CUANDO LO DESEES
    * 
    * @param type $numeroCuenta
    * @return type
    */
   public function carrito_confirmarProducto($numeroCuenta){
       
       /*
        * deberemos corroborar primero las existencias de los prodcutos del carrito
        * si el producto tiene existencias entonces pasamos al metodo donde decrementamos el inventario
        * si no tiene existencias suficientes marcaremos el producto como agotado en la tabla del carrito
        */
       
       $carritoCliente = $this->carrito_consultarCarrito($numeroCuenta);
       
        /*
         * Array
            (
                [0] => Array
                    (
                        [id] => 100
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
                        [diferencia_regalo] => 0
                        [puntos_tomados] => 0
                    )

                [1] => Array
                    (
                        [id] => 101
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
                        [diferencia_regalo] => 0
                        [puntos_tomados] => 0
                    )
        )
         */
        
       //creamos un UPDATE para actualizar el producto en la tabla de carrito del cliente
       $statement = $this->conexion->prepare("UPDATE carritos_clientes SET producto_confirmado=?, estado_producto=?, seguimiento_producto=?"
                    . "WHERE id=?");
       
       
       
       
       $resultadosDelMetodo = [
           'contadorTotalPorConfirmar'=> 0,             
           'contadorAgotadosAntes'=>0];
       
        foreach ($carritoCliente as $producto) {
            $id = $producto["id"];
            $id_producto = $producto["id_producto"];
            $no_color = $producto["no_color"];
            $talla = $producto["talla"];
            $cantidad = $producto["cantidad"];
            $estado_producto = $producto["estado_producto"];                //almacenamos el estado del producto que es CREDITO O INVERSION porque en el update cuando se descuent acorrectamente del inventario lo debemos volver a poner tal cual esta
            $producto_confirmado = $producto["producto_confirmado"];
            
            if($producto_confirmado== "false"){
                $resultadosDelMetodo['contadorTotalPorConfirmar']+=$cantidad;           //solo contamos los productos que no esten confirmados aun
            }
              
            
            
            if ($producto_confirmado == "false") {
                if ($this->productos_existenciasComprobar($id_producto, $no_color, $talla, $cantidad)) {
                    //Si hay existencias suficientes para cubrir el pedido del cliente
                    $this->productos_existenciasDecrementarExistencias($id_producto, $no_color, $talla, $cantidad);
                    //si se descontaron correctamente las existencias del carrito                    
                    $statement->execute(["true", $estado_producto, "Producto confirmado", $id]);
                } else {
                    $statement->execute(["false", "AGOTADO", "Este producto se agoto, antes de que confirmaras tu pedido", $id]);
                    $resultadosDelMetodo['contadorAgotadosAntes']+=$cantidad;
                }
            }else{
                //echo "se encuentran productos ya confirmados $id_producto $no_color";
            }
        }
        
        
        
        
        return $resultadosDelMetodo;
       
       
   }
   
   /**
    * Queremos que este metodo nos retorne true o false, si la fehca que le mandemos es mayor a la fecha
    * de nuestro sistema actual, si es mayor la fecha del sistema a la fecha del pedido en la tabla carrito entonces
    * vamos a retornar true obligando a que el sistema retorne un mensaje indicando que el ultimo pedido ya no esta disponible.
    * @param string $fecha
    */
   public function carrito_compararFechaPedidoFinalizado(string $fecha){
       $fechaEntregaPedido = date_create($fecha, timezone_open("America/Mexico_City"));
       $stringFechaActual = utilitarios_Class::fechaActualYYYY_mm_dd()->format("Y-m-d");                   //le damos formato a string para que se borren los minutos y segundos de la fecha actual
       $fechaActual = date_create($stringFechaActual, timezone_open("America/Mexico_City"));
       
       //print_r($fechaEntregaPedido);
       /*
        * DateTime Object
            (
                [date] => 2021-04-20 00:00:00.000000
                [timezone_type] => 3
                [timezone] => America/Mexico_City
            )
        */       
       //print_r($stringFechaActual);
       //2021-04-27
       //print_r($fechaActual);
       /*
        * DateTime Object
            (
                [date] => 2021-04-27 00:00:00.000000
                [timezone_type] => 3
                [timezone] => America/Mexico_City
            )
        */

       $dif = $fechaActual->diff($fechaEntregaPedido);       
       //print_r($dif);
       /*
        *   DateInterval Object
            (
                [y] => 0
                [m] => 0
                [d] => 2
                [h] => 0
                [i] => 0
                [s] => 0
                [f] => 0
                [weekday] => 0
                [weekday_behavior] => 0
                [first_last_day_of] => 0
                [invert] => 0
                [days] => 2
                [special_type] => 0
                [special_amount] => 0
                [have_weekday_relative] => 0
                [have_special_relative] => 0
            )

        */              
       //si la fecha actual es mayor a la fecha de entrega de pedido quiere decir que el pedido ya fue entregado invert es 1
       //si la fecha actual es menor es decir la fecha de entrega aun no llega invert es 0
       //si los dias son iguales invert 0
       
       
       
       
       if($dif->invert){
           return true;
           //echo "Tu ultimo pedido ha sido finalizado, agrega a tu carrito para uno nuevo!";
       }else{
           return false;
       }
     

       
   }
   
   /**
    * 
    * @param string $numeroCuenta
    * @param string $fechaEntrega en formato YYYY-mm-dd
    */
   public function carrito_calcularCreditoCliente(string $numeroCuenta, string $fechaEntrega){
      $datosCliente = $this->clientes_consultarCliente($numeroCuenta);
      $grado=$datosCliente['grado'];
      //print_r($datosCliente);
      //print_r($grado);
      
      $SQL = "";
      if ($grado == "EMPRESARIA") {
            $SQL = "SELECT SUM(precio_empresaria*cantidad) FROM carritos_clientes WHERE fecha_entrega_pedido=? AND no_cuenta=? AND estado_producto LIKE 'CREDITO'";
        } else if ($grado == "SOCIA") {
            $SQL = "SELECT SUM(precio_socia*cantidad) FROM carritos_clientes WHERE fecha_entrega_pedido=? AND no_cuenta=? AND estado_producto LIKE 'CREDITO'";
        } else if ($grado == "VENDEDORA") {
            $SQL = "SELECT SUM(precio_vendedora*cantidad) FROM carritos_clientes WHERE fecha_entrega_pedido=? AND no_cuenta=? AND estado_producto LIKE 'CREDITO'";
        }
        
        
        $statement = $this->conexion->prepare($SQL);
        $statement->execute([$fechaEntrega,$numeroCuenta]);
        $resultado = $statement->fetch();
        //print_r($resultado);
        /*
         * Array
            (
                [SUM(precio_empresaria*cantidad)] => 3162
                [0] => 3162
            )
         */
        
        if(empty($resultado)){
            return 0;
        }else{
            return $resultado[0];
        }
        
        
    }

   
 /**
  * Queremos saber si podemos seguir agregando al carrito del cliente productos en credito
  * @param string $numeroCuenta
  * @param string $totalPorAgregar el importe que se desea agregar al carrito
  * @return array
  *         <p>[excedidoPor] => 262 si el credito se exderia esta es la cantidad si no 0
            <p>[limiteCredito] => 3700
            <p>[fechaEntrega] => 2021-05-21 la fecha de entrega con la que se esta consultado el pedido y credito en el carrito
            <p>[creditoActualOcupado] => 1962 el importe en piezas a credito
            <p>[creditoDisponible] => 0 si aun tiene credito disponible esta es la cantidad
            <p>[totalPorAgregar] => 2000 el importe que deseamos agregar al carrito
            <p>[totalFuturo] => 3962 el credito ocupado + el total por agregar que quedaria si se agregara
  */
    public function carrito_evaluarIngresoDeProductoPorCredito($numeroCuenta,$totalPorAgregar){
        $fechaHipoteticaEntrega = $this->carrito_crearFechaEntregaPedido($numeroCuenta);
        //$fechaHipoteticaEntrega = "2021-05-21";
        $creditoOcupado = $this->carrito_calcularCreditoCliente($numeroCuenta, $fechaHipoteticaEntrega);
        
        $datosCliente = $this->clientes_consultarCliente($numeroCuenta);
        $limiteCredito = $datosCliente['credito'];
        
        $creditoTotalHipotetico = $creditoOcupado + $totalPorAgregar;
        
        
        
        $resultado = ["excedidoPor"=>0,
            "limiteCredito"=>0,
            "fechaEntrega"=>"",
            "creditoActualOcupado"=>0,
            "creditoDisponible"=>0,
            "totalPorAgregar"=>0,
            "totalFuturo"=>0];
        
        if($creditoTotalHipotetico > $limiteCredito){
            
            $excedidoPor = $creditoTotalHipotetico-$limiteCredito;             
            
            
            $resultado["excedidoPor"] = $excedidoPor;
            $resultado["limiteCredito"] = $limiteCredito;
            $resultado["fechaEntrega"] = $fechaHipoteticaEntrega;
            $resultado["creditoActualOcupado"] = $creditoOcupado;
            $resultado["creditoDisponible"] = 0;
            $resultado["totalPorAgregar"] = $totalPorAgregar;
            $resultado["totalFuturo"] = $creditoTotalHipotetico;
        }else{
            $resultado["excedidoPor"] = 0;
            $resultado["limiteCredito"] = $limiteCredito;
            $resultado["fechaEntrega"] = $fechaHipoteticaEntrega;
            $resultado["creditoActualOcupado"] = $creditoOcupado;
            $resultado["creditoDisponible"] = $limiteCredito-$creditoTotalHipotetico;
            $resultado["totalPorAgregar"] = $totalPorAgregar;
            $resultado["totalFuturo"] = $creditoTotalHipotetico;
            
            
        }
        
        return $resultado;
        /*
         * Si se pasa, el credito
         * Array
        (
            [excedidoPor] => 262
            [limiteCredito] => 3700
            [fechaEntrega] => 2021-05-21
            [creditoActualOcupado] => 1962
            [creditoDisponible] => 0
            [totalPorAgregar] => 2000
            [totalFuturo] => 3962
        )
         */
        
       
    }
        
        //metodo para llenar temporalmente el campo asignado en la tabla carritos cleintes con la a1 o q3 o m1
       public function llenarAsignadoEnCarrito(){
        //para ser eficientes, vamos a consultar cada ruta en la tabla nombresZonas
        $statement = $this->crearConexionAdmin()->prepare("SELECT ruta,nombre FROM nombres_zonas WHERE ruta LIKE 'A%' || ruta LIKE 'Q%' || ruta LIKE 'M%'"); //con lique limpiamos las pruebas que tenemos z1, prueba google etc
        $statement->execute();
        $resultado = $statement->fetchall(PDO::FETCH_ASSOC);
        
        //print_r($resultado);
        /*
         * Array
(
    [0] => Array
        (
            [ruta] => A1
            [nombre] => ACAMBAY
        )

    [1] => Array
        (
            [ruta] => A1
            [nombre] => SAN JUAN DEL RIO
        )

    [2] => Array
        (
            [ruta] => A1
            [nombre] => PIEDRAS BLANCAS
        )

    [3] => Array
        (
            [ruta] => A1
            [nombre] => CANALEJAS
        )

    [4] => Array
        (
            [ruta] => A1
            [nombre] => CIUDAD HIDALGO
        )

    [5] => Array
        (
            [ruta] => A1
            [nombre] => 
        )

    [6] => Array
        (
            [ruta] => A1
            [nombre] => 
        )

    [7] => Array
        (
            [ruta] => A2
            [nombre] => SOLIS
        )

    [8] => Array
        (
            [ruta] => A2
            [nombre] => CONTEPEC
        )

    [9] => Array
        (
            [ruta] => A2
            [nombre] => DIOS PADRE)
    )
         */
        
        $update = $this->conexion->prepare("UPDATE carritos_clientes SET asignado=? WHERE ruta LIKE ?");
        foreach ($resultado as $value) {
            //una vez obtenemos las zonas vamos a filtrar que nombre tenga sus caracteres mayores a 2 para filtrar los campos vacios
            $nombre = $value['nombre'];
            $asignado = $value['ruta']; //A1 A2 Q3 etc
            if(strlen($nombre)>1){//si el nombre no esta vacio
                $update->execute([$asignado,$nombre]);
                echo "Actualizando rutas en carrito zona $nombre, asignado: $asignado \n";
            }           
            
        }
        
        
        
    }
   
   
}







