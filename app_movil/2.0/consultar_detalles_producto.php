<?php

/* 
 * Nos encargamos de consultar y devolverle al movil los datos
 * de un id de un producto, en esta primera consulta devolveremos 
 * un json array dentro con todos los campos de un solo id para que ese se muestre en las imagenes
 * 
 * y devolveremos otro json array con todos los colores disponibles de ese id en particular
 */
include_once './base_de_datos_Class.php';
include_once './utilitarios_Class.php';

$dataBase = new base_de_datos_Class();

$idProducto = filter_var($_REQUEST["ID_PRODUCTO"], FILTER_SANITIZE_STRING);
$numeroCuenta = filter_var($_REQUEST["CUENTA_CLIENTE"], FILTER_SANITIZE_STRING);


        


if ($numeroCuenta == "000") {  
    //si es un Invitado
    
    $respuesta["mensajeEntregaProducto"] = "";//cuando esten como invitados no mostramos las fechas de entrega
    $respuesta["detalle_principal"] = $dataBase->productos_consultarDatosGeneralesDetalladosPorIdProducto($idProducto);
}else{
    $fechaVisita = $dataBase->clientes_consultarFechaFuturaVisita($numeroCuenta);
    $fechaCierre = $dataBase->clientes_consultarFechaCierrePedido($numeroCuenta);
    $respuesta["mensajeEntregaProducto"] = utilitarios_Class::calcularMensajeEntregaProducto($fechaVisita, $fechaCierre); //si ordenas este producto ahora llegara en 6 dias en el pedido del 23-07 bla bla
    $respuesta["detalle_principal"] = $dataBase->productos_consultarDatosGeneralesDetalladosPorIdProducto($idProducto);
}





//print_r($respuesta);


echo json_encode($respuesta,true);