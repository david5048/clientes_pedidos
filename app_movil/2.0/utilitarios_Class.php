<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of utilitarios_Class
 *
 * @author david
 */

class utilitarios_Class {
    //put your code here
    const BLOQUEO = "bloqueo";
    
    
    /**
     * Consulta la fecha y hora del sistema con timezone mexico city
     * @return string Hora Fecha <p> hh:mm:ss dd-mm-yyyy
     */
    public static function fechaActualToText() {
        date_default_timezone_set('America/Mexico_City');
        //funcion para obtener la fecha actual del sistema y retornarla como texto concatenado    
        $fechaSolicitud = getdate();
        //print_r ($fechaSolicitud);
        $fechaTexto = $fechaSolicitud["hours"] . ":" . $fechaSolicitud["minutes"] . ":" . $fechaSolicitud["seconds"] . " " . $fechaSolicitud["mday"] . "-" . $fechaSolicitud["mon"] . "-" . $fechaSolicitud["year"];
        //echo $fechaTexto;
        return $fechaTexto;
    }
    
    /**
     * Consulta la fecha y hora del sistema con timezone mexico city
     * en formato que acepta las bases de datos SQL que es primero el año
     * luego mes y al final dia
     * @return string Fecha Hora <p> yyyy-mm-dd hh:mm:ss
     */
    public static function fechaActualParaBasesDeDatos() {
        date_default_timezone_set('America/Mexico_City');
        //funcion para obtener la fecha actual del sistema y retornarla como texto concatenado    
        $fechaSolicitud = getdate();
        //print_r ($fechaSolicitud);
        $fechaTexto = $fechaSolicitud["year"]. "-" . $fechaSolicitud["mon"]."-".$fechaSolicitud["mday"]  ." ".$fechaSolicitud["hours"] . ":" . $fechaSolicitud["minutes"] . ":" . $fechaSolicitud["seconds"];
        //echo $fechaTexto;
        return $fechaTexto;
    }
    
    
    
    
    
       
    
    /**
     * Consulta la fecha del sistema con timezone mexico city
     * @return string fecha <p> dd-mm-yyyy
     */
    public static function dameFecha_dd_mm_aaaa_ToText() {
        date_default_timezone_set('America/Mexico_City');
        //funcion para obtener la fecha actual del sistema y retornarla como texto concatenado    
        $fechaSolicitud = getdate();
        //print_r ($fechaSolicitud);
        $fechaTexto = $fechaSolicitud["mday"] . "-" . $fechaSolicitud["mon"] . "-" . $fechaSolicitud["year"];
        //echo $fechaTexto;
        return $fechaTexto;
    }
    
    /**
     * Consulta la fecha del sistema con timezone mexico city
     * @return string fecha Hora <p> dd-mm-yyyy----hh:mm
     */
    public static function fechaActual_dd_mm_aaaa_hh_mmToText() {
        date_default_timezone_set('America/Mexico_City');
        //funcion para obtener la fecha actual del sistema y retornarla como texto concatenado    
        $fechaSolicitud = getdate();
        //print_r ($fechaSolicitud);
        $fechaTexto = $fechaSolicitud["mday"] . "-" . $fechaSolicitud["mon"] . "-" . $fechaSolicitud["year"]."----".$fechaSolicitud["hours"] . ":" . $fechaSolicitud["minutes"];
        //echo $fechaTexto;
        return $fechaTexto;
    }
    
    /**
     * Consulta la fecha del sistema con timezone mexico city
     * @return string hora <p> hh:mm:ss
     * 
     */
    public static function horaActual_hh_mm_ss_ToText() {
        date_default_timezone_set('America/Mexico_City');
        //funcion para obtener la fecha actual del sistema y retornarla como texto concatenado    
        $fechaSolicitud = getdate();
        //print_r ($fechaSolicitud);
        $fechaTexto = $fechaSolicitud["hours"] . ":" . $fechaSolicitud["minutes"] . ":" . $fechaSolicitud["seconds"];
        //echo $fechaTexto;
        return $fechaTexto;
    }
    
    /**
     * Puedes enviar un string para obtener una instancia de date 
     * de este modo puede guardarse tambien directamente en la base de datos
     * o puedes cambiar el formato de este string 20-11-2020 a este 2020-11-20
     * y obtener un objeto date
     * 
     * @param String fecha en string a convertir
     * @return date una instancia a un objeto date 
     */
    public static function stringToFecha_yyyy_mm_dd($string){
        return date("Y-m-d", strtotime($string));
    }
    
    
    /**
     * Saber la hora del sistema con zona horaria "America/Mexico_City"
     * @return date Devuelve una instancia de un objeto date, con la hora actual
     */
    public static function fechaActualYYYY_mm_dd(){       
        $fecha = date_create("now", timezone_open("America/Mexico_City"));
        
        //print_r($fecha);
        
        return $fecha;
    }
    
 
    /**
     * Atencion este metodo afecta el objeto que le mandas, si vas a usar el objeto DateTime en otro lugar del codigo, este lo modifica
     * y tendras el valor modificado en todos lados, mejor usar el otro metodo de abajo del string, la documentacion de php lo dice que 
     * no es comun que un metodo afecte al objeto, deberia retornar un objeto independiente
     * @param DateTime $fecha
     * @param int $dias
     * @param bool $returnString true si quieres que te retorne un String como resultado <br> false si quieres que retorne un DateTimeObject
     * @return type
     */
    public static function restarDiasFecha(DateTime $fecha,int $dias, bool $returnString = false){
        //https://www.php.net/manual/es/function.date-add.php
        //https://www.php.net/manual/es/datetime.sub.php
        /*
         * NOTA IMPORTANTE
         * tener cuidado con este metodo, ya que modifica el objeto date que le pasas, algo muy raro, no te devuelve uno nuevo, modifica el que le pasaste
         * me causo problemas, y lo curioso es que tambien un comentario en la documentacion lo dice, jaja no es muy comun de un metodo asi
         * 
         */
        
                
        $auxiliar = 'P'.$dias.'D';
        $interval = new DateInterval($auxiliar);
        $fecha->sub($interval);
        
        
        if($returnString){
            return $fecha->format('d-m-Y'); //le damos el formato a la fecha
        }else{
            return $fecha;  
        }
             
        
    }
    
    /**
     * Usamos este metodo para no tener el problema de que cambia los valores en el objeto original
     * pasale un string mejor si tienes un objeto dateTime formatealo con dateTIme->format("d-m-Y")
     * para que pase un string a este metodo
     * @param String $fecha
     * @param int $dias
     * @return string 
     */
    public static function restarDiasFechaString(String $fecha,int $dias){
        //https://www.php.net/manual/es/function.date-add.php
        //https://www.php.net/manual/es/datetime.createfromformat.php
        
        
        $auxiliar = 'P'.$dias.'D';
        $fechaArestar = date_create($fecha, timezone_open("America/Mexico_City"));
        $fechaArestar->sub(new DateInterval($auxiliar));
        

        return $fechaArestar->format('d-m-Y');        //le damos el formato a la fecha
        
    }
    
    /**
     * Suma a una fecha los dias que desees
     * @param string $fecha
     * @param int $dias
     * @return string dd-mm-yyyy
     */
    public static function sumarDiasFecha(string $fecha,int $dias){
        //https://www.php.net/manual/es/function.date-add.php
        //https://www.php.net/manual/es/datetime.createfromformat.php
        
        
        $auxiliar = 'P'.$dias.'D';
        $fechaAsumar = date_create($fecha, timezone_open("America/Mexico_City"));
        $fechaAsumar->add(new DateInterval($auxiliar));
        

        return $fechaAsumar->format('d-m-Y');        //le damos el formato a la fecha
    }
    
    
        /**
     * Suma a una fecha los dias que desees
     * @param string $fecha
     * @param int $dias
     * @return string yyyy-mm-dd
     */
    public static function sumarDiasFechaYYY(string $fecha,int $dias){
        //https://www.php.net/manual/es/function.date-add.php
        //https://www.php.net/manual/es/datetime.createfromformat.php
        
        
        $auxiliar = 'P'.$dias.'D';
        $fechaAsumar = date_create($fecha, timezone_open("America/Mexico_City"));
        $fechaAsumar->add(new DateInterval($auxiliar));
        

        return $fechaAsumar->format('Y-m-d');        //le damos el formato a la fecha
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * Consulta las semanas totales que tiene un año por ejemplo 52 o 53 semanas
     * //lo puedes implementar facilmente en un bucle for
    /*
     * for($i=2000;$i<=2020;$i++)

        {

            echo "<br>".$i." - ".NumeroSemanasTieneUnAno($i);

        }
     *
     * 
     * @param type $year el año deseado
     * @return int el numero total de semanas del año consultado
     */
    public static function cuantasSemanasTieneUnAno($year) {
         //este codigo lo copiamos de aqui https://www.lawebdelprogramador.com/codigo/PHP/2558-Obtener-la-ultima-semana-de-un-ano.html
        //NO ME PREGUNTES COMO FUNCIONA PORQUE FUCK QUE NO LO SE XD EL CHISTE ES QUE
        //TE ENTREGA CUAL ES EL NUMERO TOTAL DE SEMANAS QUE TIENE UN AÑO POR EJEMPLO EL 2019 TIENE 52
        //PERO EL 2020 TIENE 53

        $date = new DateTime;



        //Establecemos la fecha segun el estandar ISO 8601 (numero de semana)

        $date->setISODate($year, 53);



        //Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52

        if ($date->format("W") == "53")
            return 53;
        else
            return 52;
    }
   
    public static function calcularPuntosGanados($ventaTotal){
        
        if($ventaTotal<500){
            return 0;
        }
        
        $escalonInicial = 500;
        $puntosGanados = 50;
        for ($i=250; $i<30000; $i+=250){
            
            //si la venta es 760
            if($ventaTotal>= $escalonInicial && $ventaTotal < ($escalonInicial + $i)){
                $puntosGanados += 50;
                break;
            }else{
                $puntosGanados +=50;
            }
            
        }
        
        
        return $puntosGanados;
        
    }
    
    public static function crearMensajesBrevesSoloTotalesFinales($infoTotales){
        //este metodo creara solo los ultimos mensajes que necesita el sistema para mostrar el totalPago al recibir el producto
        //respetando el orden de operacion4 del archivo editarPedido.php
        //lo hacemos porque no tiene caso encviar todos estos mensajes cuando el cliente edita cambia a inversion
        //y ahorita es necesario que cada que se edite algo en el caarrito se envie lo ultimo que es los pagos al recibir el producto
        //invluso cuando se consulta el carrito ya que añadimos esta vista. la operacion 4 envia mensajes largos proque tiene mostrar todos los dialogos antes de confirmar
        //en lo demas solo mostraremos los ultimos mensajes
        
        //convertimos los financiamientos de float a porcentaje
        $porcentajePagoCliente = $infoTotales['porcentaje_pago_cliente'] * 100;
        $fechaPagoDelCredito = $infoTotales['fecha_pago_del_credito'];

        
        
        
        //creamos un array que tendra todos los mensajes para mostrarse en el sistema        
        $mensajes[18]= "Al recibir tu pedido deberás liquidar al agente Kaliope:";
        $mensajes[19]= "Exceso de credito";
        $mensajes[20]= "$porcentajePagoCliente% credito";
        $mensajes[21]= "Inversion";
        $mensajes[22]= "por liquidar al recibir el pedido";
        $mensajes[23] = "Quedaran pendientes para pagarse el $fechaPagoDelCredito";
        
        return $mensajes;
        
    }
    
    /**
     * Dependiendo de la fecha actual del sistema y la fecha de cierre de pedido del cliente calculare
     * unos mensajes, si la fecha actual es antes de la fecha de cierre entonces marcamos un mensaje
     * si la fecha actual casi se aproxima a la fecha de cierre de pedido otro
     * si la fecha actual se pasa de la fecha de cierre cuando ya no puede añadir mas piezas al carrito entonces marcamos otro
     * @param string $fechaCierre
     */
    public static function calcularMensajeCierrePedidoCliente(string $fechaCierre, string $fechaVisita){
        
        $fechaActual = self::fechaActualYYYY_mm_dd();
        $fechaFinPedido = date_create($fechaCierre, timezone_open("America/Mexico_City"));  
        $fechaMas1Dia = self::sumarDiasFecha($fechaVisita, 1);
        //print_r($fechaActual);
        //print_r($fechaFinPedido);
        
        $diff = $fechaActual->diff($fechaFinPedido);
        //print_r($diff);
        /*es como si a la fecha finPedido le quitaras la fecha actual. si la fecha actual es mayor ya sea minutos invert se colocara en 1.
         * DateInterval Object
            (
                [y] => 0
                [m] => 0
                [d] => 0
                [h] => 1
                [i] => 19
                [s] => 5
                [f] => 0.998674
                [weekday] => 0
                [weekday_behavior] => 0
                [first_last_day_of] => 0
                [invert] => 0
                [days] => 0
                [special_type] => 0
                [special_amount] => 0
                [have_weekday_relative] => 0
                [have_special_relative] => 0
            )
         * 
         */

        
        
        //si la fecha actual es mayor a la de cierre de pedido
        if($diff->invert){
            
                return "Por ahora no puedes ingresar productos a tu carrito,\ntu pedido fue cerrado el $fechaCierre\nporque sera entregado el $fechaVisita\npodras iniciar un nuevo pedido el $fechaMas1Dia";
            
            
        }else{
            /*
             * si la fecha actual es menor a la de cierre buscamos por cuantois
             * dias es menor, si es menor por 1 dia pondremos un mensaje
             * si es menro por 2 dias ponemos otro
             * si es menor por 3 dias ponemos otro
             */
            
            $dias = $diff->days;
            
            if($dias<1){
                //si los dias son menores a 1 dia, es decir horas
               return "Hoy es el ultimo dia para agregar productos a tu carrito,\n tu pedido sera cerrado el $fechaCierre\npara ser entregado el $fechaVisita";
            }elseif($dias==1){
                return "Te queda 1 dia para agregar productos a tu carrito,\ntu pedido sera cerrado el $fechaCierre\npara ser entregado el $fechaVisita";
            }elseif ($dias>1) {
                return "Te quedan $dias dias para agregar productos a tu carrito,\nantes que tu pedido sea cerrado el $fechaCierre\npara ser entregado el $fechaVisita";
            }
        }
        
    }
    
     /**
     * Dependiendo de la fecha actual del sistema y la fecha de cierre de pedido del cliente calculare
     * unos mensajes para mostrar en la parte de detalles del producto donde dice cuando lo recibira
     * un resultado correcto de este metodo debe ser:<p> si la clienta es de una zona de queretaro viernes 23-07-2021
     * <p> los dias de bloqueo de una zona de queretaro son 6 dias, porque el dia sabado 17-07-2021 se surten los pedidos y se mandan a la sucursal
     * <p> entocnes fecha cierre llegara como 17-07 y fecha visita como 23-07
     * <p> por lo tanto a la clienta se le permitira añadir a su pedido del dia 23-07-2021 productos hasta el dia sabado 17
     * <p> si hoy es miercoles 14 o cualquier dia alejado 2 dias de la fecha de corte, el sistema debera decirle llegara el 23-07
     * <p> si hoy es jueves 15, el sistema debera decirle que tiene hoy y mañana para agregar el producto que llegara el 23-07
      * <p> si hoy es viernes 16 debera decirle que tiene solo hoy para agregar producto a su carrito del 23-07
      * <p> si ya hoy es sabado 17 debera decirle que su pedido llegara hasta su siguiente visita del 23-07 + 14 dias
      * <p> si hoy es despues de la fecha sigue indicandole que lo recibira el 23-07+14 dias
      * @param string $fechaVisita la fecha de visita del cliente en los movimientos en kaliopeAdmin
     * @param string $fechaCierre la fecha limite que el cliente tiene para agregar produtos
      * @return string "Si ordenas ahora tu producto se entergara dentro de 6 dias en el pedido del dia 22-07-2021"
     */
    public static function calcularMensajeEntregaProducto(string $fechaVisita, string $fechaCierre){
           
        
       
               
        $fechaSigienteEntrega = utilitarios_Class::sumarDiasFecha($fechaVisita, 14); //calculamos la fecha hipotetica donde le tocara visita al cliente despues de esta
        
        $stringFechaActual = self::fechaActualYYYY_mm_dd()->format("d-m-Y");
        //$stringFechaActual = "19-07-2021";
        $fActual = date_create($stringFechaActual, timezone_open("America/Mexico_City"));
        $fVisita = date_create($fechaVisita, timezone_open("America/Mexico_City"));
        $fCierre = date_create($fechaCierre, timezone_open("America/Mexico_City"));
        $fSiguienteEntrega = date_create($fechaSigienteEntrega, timezone_open("America/Mexico_City"));
        
        
        
        $diff = $fActual->diff($fCierre);     
        $diffRestantesParaVisita = $fActual->diff($fVisita);                        //los dias que faltan para la fecha de visita normal progrmada del cliente
        $diffRestantesParaEntregaFutura = $fActual->diff($fSiguienteEntrega);       //para saber cuantos dias faltan para el caso de que el pedido entre en la fecha hipotetica futura +14dias
        
        
        
        
        //cuantos dias quedan para que el pedido le llegue al cliente. quiero informarle al cliente:
        //tu producto llegara en 3 dias, en el carrito del 15-07-2021
        $diasParaVisita = $diffRestantesParaVisita->d;
        $diasParaSiguienteEntrega = $diffRestantesParaEntregaFutura->d;
        
        
        
        //DEPURACION COMENTAR Y DESCOMENTAR ESTOS ECOS
        /*
        echo"La fecha de visita mas cercana del cliente es:$fechaVisita \n"; //print_r($fVisita);
        echo"\nLa fecha de cierre del pedido de esa fecha es:$fechaCierre todo lo que agrege antes de esta fecha deberia entrar con el mismo pedido\n"; //print_r($fCierre);
        echo"\n\nLa fecha actual es: ". $fActual->format("d-m-Y G:i:s"). "\n"; //print_r($fActual);
        echo"\nLa fecha de sigiente entrega para nuevo producto es:".$fSiguienteEntrega->format("d-m-Y"); //print_r($fSiguienteEntrega);
        echo"\nLa diferencia de la fCierre-fActual es: \n";echo ($diff->invert?"La fecha actual es mayor que la de cierre. Ya se paso.\n": "La fecha actual aun no llega a la fecha de cierre quedan $diff->d dias y $diff->h horas\n");  print_r($diff); 
        */
        
        
        //si la fecha actual es mayor a la de cierre de pedido
        if($diff->invert){
            
                return "Tu pedido mas cercano del $fechaVisita esta siendo cerrado en el almacen\nSi ordenas ahora este producto se entergara dentro de $diasParaSiguienteEntrega dias en el pedido del dia $fechaSigienteEntrega"; 

            
        }else{            
            /*si la fecha actual es menor a la de cierre entendemos que el cliente aun puede agregar producto para que le llege al dia normal de visita
             */         
            
            $dias = $diff->days;
            if($dias==0){
                //Hoy es el dia de cierre del pedido ya no puede agregar productos
                return "Tu pedido mas cercano del $fechaVisita esta siendo cerrado en el almacen\nSi ordenas ahora este producto se entergara dentro de $diasParaSiguienteEntrega dias en el pedido del dia $fechaSigienteEntrega"; 
            }elseif($dias==1){
                return "¡Hoy es el ultimo dia para recibirlo en $diasParaVisita dias!\nTienes solo hoy para agregar este producto a tu carrito que llegara el $fechaVisita ¡apresurate!\n\nSi lo agregas mañana llegara $diasParaSiguienteEntrega dias despues en el pedido del $fechaSigienteEntrega";

            }elseif ($dias==2) {
                return "Tienes hoy y mañana\npara agregar este producto a tu carrito que llegara en $diasParaVisita dias el $fechaVisita ¡apresurate!\n\nSi esperas mas llegara $diasParaSiguienteEntrega dias despues en el pedido del $fechaSigienteEntrega";
            }elseif ($dias>2) {
                return ("Si ordenas ahora tu producto llegara en $diasParaVisita dias, se entregara en el pedido del dia $fechaVisita");
            }
            
        }
        
        
        
    }
    
    /**
     * Cuando el cliente agrege su producto al carrito quiero que le diga que el producto se ha agregado
     * a su carrito correctamente
     * si agrega al carrito en los dias de bloqueo le dira que se creara un nuevo pedido para la siguiente fecha
     * @param string $fechaVisita
     * @param string $fechaCierre
     * @return string $mensaje
     */
    public static function calcularMensajeAgregarCarrito(string $fechaVisita, string $fechaCierre){
        
    
        
        $stringFechaActual = utilitarios_Class::fechaActualYYYY_mm_dd()->format("d-m-Y");
        //$stringFechaActual = "23-07-2021";
        $stringFechaEntregaFutura = utilitarios_Class::sumarDiasFecha($fechaVisita, 14);
         
        $fActual = date_create($stringFechaActual, timezone_open("America/Mexico_City"));
        $fVisita = date_create($fechaVisita, timezone_open("America/Mexico_City"));
        $fCierre= date_create($fechaCierre, timezone_open("America/Mexico_City"));   
        $fEntregaFutura = date_create($stringFechaEntregaFutura, timezone_open("America/Mexico_City"));   
        
        
        $diff = $fActual->diff($fCierre);
        $dias = $diff->d;
        
        $diffEntregaFutura = $fActual->diff($fEntregaFutura);
        $diffVisita = $fActual->diff($fVisita);
        
        
        if($diff->invert){
            //si la fecha actual es mayor a la fecha de cierre
            if($diffVisita->d==0){
                //si la fecha actual es igual a la fecha de visita
                return "Tu carrito del $fechaVisita llegara ¡hoy!"
                    . "\n\nEste producto se agrego a un nuevo carrito que se entregara en $diffEntregaFutura->d dias"
                    . "\nel $stringFechaEntregaFutura";
            }elseif($diffVisita->d>0 && !$diffVisita->invert){
                return "Tu carrito del $fechaVisita esta en proceso de entrega, llegara ".($diffVisita->d>1? "en $diffVisita->d dias":"¡mañana!")
                    . "\n\nEste producto se agrego a un nuevo carrito que se entregara en $diffEntregaFutura->d dias"
                    . "\nel $stringFechaEntregaFutura";
            }else{
                //si aun no se reciben los datos en administracion y aun no se crea una fecha futura, no deberia pasar pero si ocurre
                return"Este producto se agrego a tu carrito que se entregara en $diffEntregaFutura->d dias"
                    . "\nel $stringFechaEntregaFutura";
            }
            
        }else{
            //si la fecha visita es menor o igual a fecha cierre
            if($dias==0){
                //si hoy es la fecha de cierre del pedido, debemos mostrar el mensaje de que se creara un nuevo carrito
                return "Tu carrito del $fechaVisita esta en proceso de entrega, llegara " .($diffVisita->d>1?"en $diffVisita->d dias.":"¡mañana!")
                    . "\n\nEste producto se agrego a un nuevo carrito que se entregara dentro de $diffEntregaFutura->d dias"
                    . "\nel dia $stringFechaEntregaFutura";
                
            }elseif($dias>0){
                //si aun tiene tiempo le mostraremos el mensaje
                return "Se ha agregado este producto a tu carrito.\nSe entregara en $diffVisita->d dias."
                        . "\n\nEl dia:$fechaVisita";
                
            }
            
        }
        
    }
    
    
    /**
     * Cuando el cliente consulte su carrito este mensaje debera aparecer en alguna parte de la ventana
     * Te quedan 2 dias para poder editar este carrito, sera enviado al almacen de kaliope para enviarlo a tu domicilio
     * te queda solo hoy para editar tu carrito, antes que sea enviado al almacen de kaliope para enviarlo a tu domicilio
     * Ya no puedes editar este carrito, se encuentra en el almacen de kaliope.
     * Este pedido se encuentra en curso a tu domicilio esperalo llegara en 3 dias!
     * este pedido a sido entregado. 
     * @param string $fechaEntregaPedido
     * @param int $diasBloqueo
     * @return array $respuesta  <p>["bloqueado"] = false-true;
                                <p>["alerta"] = 1-2-3-4;
                                <p>["mensaje"] = "¡Tienes solo hoy para editar tu carrito!\n\nMañana sera enviado al almacen Kaliope";
     */
    public static function calcularMensajeBloqueoCarrito(string $fechaEntregaPedido, int $diasBloqueo){
        /*
         * Para mostrar estos mensajes cambia un poco la cosa, no ocupare las fechas del cliente o la info del cliente
         * necesito basarme en la fecha de entrega del pedido, osea la informacion de la tabla del carrito, lo unico que 
         * ocupo del cliente son los dias de bloqueo, para calcular sobre la fechade entrega del pedido la fecha de cierre de ese pedido,
         * esto lo quiero asi porque se supone que este mensaje se mostrara en el pedido, debera estar fuera de fechas a las 
         * fechas actuales que lleve el cliente, porque por ejemplo podriamos estar generando este mensaje en un pedido de hace 2 meses
         * en el historial de pedidos, o el cliente podria tener ya una nueva fecha futura y eso alteraria los mensajes y los bloqueos de
         * que un pedido se pueda editar aun o ya no se pueda editar en la app.
         * 
         * Aparte de eso necesito que se envie otros campos aparte del mensaje por eso usare un array
         * estos campos extras 1 indicara si el pedido se puede editar o ya no
         * y el otro, indicara alertas, para que la app defina el color de letra o alguna animacion cuando el pedido ya esta por bloquearse
         */
        if(!defined('BLOQUEADO')) define('BLOQUEADO', "bloqueado");           //definimos unas constantes para usarlas como clave en el array. las puedo definir como const BLOQUEADO = "bloqueado"; pero esto tiene que hasta el nivel mas alto de la clase, ya que se crean en tiempo de ejecucion https://www.php.net/manual/es/language.constants.syntax.php
        if(!defined('ALERTA')) define('ALERTA', "alerta");
        if(!defined('MENSAJE')) define('MENSAJE', "mensaje");
        if(!defined('SI')) define('SI', true);
        if(!defined('NO')) define('NO', false);
        if(!defined('A_ALTA')) define('A_ALTA',1);
        if(!defined('A_MEDIA')) define('A_MEDIA',2);
        if(!defined('A_BAJA')) define('A_BAJA',3);
        if(!defined('A_OFF')) define('A_OFF',4);
        




        $respuesta= [BLOQUEADO=>false,
                    ALERTA=>"",
                    MENSAJE=>""];
    
        
        $stringFechaActual = utilitarios_Class::fechaActualYYYY_mm_dd()->format("d-m-Y");
        //$stringFechaActual = "22-07-2021";
        $stringFechaCierre = utilitarios_Class::restarDiasFechaString($fechaEntregaPedido, $diasBloqueo);
        /*
        echo "fecha actual: $stringFechaActual\n";
        echo "fecha Entrega: $fechaEntregaPedido\n";
        echo "fecha Cierre: $stringFechaCierre\n";
         */
        $fActual = date_create($stringFechaActual, timezone_open("America/Mexico_City"));
        $fEntrega = date_create($fechaEntregaPedido, timezone_open("America/Mexico_City"));
        $fCierre= date_create($stringFechaCierre, timezone_open("America/Mexico_City"));   
        
        
        $diff = $fActual->diff($fCierre);
        $dias = $diff->d;
        
        $diffEntrega = $fActual->diff($fEntrega);
        
        
        if($diff->invert){
            //si la fecha actual ya es mayor a la fecha de cierre del pedido
            
            if(!$diffEntrega->invert){
                //si la fecha es menor a la fecha de entrega del pedido
                if($diffEntrega->d>2){
                    //si faltan mas de 2 dias para su entrega
                    $respuesta[BLOQUEADO] = SI;
                    $respuesta[ALERTA] = A_BAJA;
                    $respuesta[MENSAJE] = "Ya no puedes editar.\nEste carrito se ha pasado al almacen Kaliope llegara en $diffEntrega->d dias a tu domicilio.";
                }else{
                    //si los dias de entrega son 2 o menos
                    $respuesta[BLOQUEADO] = SI;
                    $respuesta[ALERTA] = A_BAJA;
                    
                    if($diffEntrega->d==0){
                        $respuesta[MENSAJE] = "¡Tu pedido llega hoy! ¡esperalo!";
                    }else{
                        $respuesta[MENSAJE] = "Falta poco.\n". ($diffEntrega->d>1?"Tu pedido llegara a tu domicilio en $diffEntrega->d dias!.":"Tu pedido llega a tu domicilio ¡mañana!");
                    }                    
                }
                
                
            }else{
                //si la fecha es mayor a la fecha de entrega del pedido
                $respuesta[BLOQUEADO] = SI;
                $respuesta[ALERTA] = A_OFF;
                $respuesta[MENSAJE] = "Pedido Finalizado.\nAgrega producto para crear un nuevo carrito.";
                //IMPORTANTE EN ESTE MENSAJE INCLUIMOS LAS PALABRAS Pedido Finalizado
                //eso hara que la app en el carrito al recibir este mensaje, ya no muestre ese pedido y en lugar de eso muestre la bolsita vacia
                
            }
            
            
        }else{
            //si la fecha actual es menor o igual a la fecha de cierre del pedido
            if($dias==0){
                //si la fecha actual es igual a la fecha de cierre ya no podra editar
                $respuesta[BLOQUEADO] = SI;
                $respuesta[ALERTA] = A_BAJA;
                $respuesta[MENSAJE] = "Ya no puedes editar.\nEste carrito se ha pasado al almacen Kaliope lo recibiras ". ($diffEntrega->d>1?"en $diffEntrega->d dias":"mañana")." en tu domicilio";                
            }
            
            if($dias==1){
                //te queda solo hoy para editar tu pedido, mañana sera enviado al almacen
                $respuesta[BLOQUEADO] = NO;
                $respuesta[ALERTA] = A_ALTA;
                $respuesta[MENSAJE] = "¡Hoy es el ultimo dia!\nSi quieres agregar o eliminar algún producto de tu carrito hazlo ahora.\nMañana sera despachado para llevarlo a tu domicilio.";
            }
            
            if($dias==2){
                //te queda hoy y mañana para editar tu pedido, el 17-07-2021 sera enviado al almacen
                $respuesta[BLOQUEADO] = NO;
                $respuesta[ALERTA] = A_ALTA;
                $respuesta[MENSAJE] = "¡Tienes hoy y mañana!\nSi quieres agregar o eliminar algún producto de tu carrito hazlo pronto.\nEn 2 dias sera despachado y ya no podrás editar tu compra.";
            }
            
            if($dias>2 && $dias<6){
                //tienes nNumero de dias para editar este pedido antes que sea enviado al almacen
                $respuesta[BLOQUEADO] = NO;
                $respuesta[ALERTA] = A_MEDIA;
                $respuesta[MENSAJE] = "Tienes $dias dias para agregar o eliminar producto de tu carrito.\nDespues sera enviado al almacen Kaliope y no podrás agregar o eliminar productos.";
                //$respuesta[MENSAJE] = "Tienes $dias dias para añadir o eliminar producto de este carrito\nEl $stringFechaCierre sera enviado al almacen Kaliope y no podras agregar o eliminar productos";
            }
            
            if($dias>=6){
                //tienes nNumero de dias para editar este pedido antes que sea enviado al almacen
                $respuesta[BLOQUEADO] = NO;
                $respuesta[ALERTA] = A_BAJA;
                $respuesta[MENSAJE] = "Tienes $dias dias para añadir o eliminar producto de tu carrito";
            }
        }
        
        
        return $respuesta;
        
    }
    


}
 