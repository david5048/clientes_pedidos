<?php


include_once './base_de_datos_Class.php';
include_once './utilitarios_Class.php';
$dataBase = new base_de_datos_Class();
//http://localhost/PhpProject_clientes_pedidos/app_movil/2.0/recibir_pedido.php?CUENTA_CLIENTE=1386&ID_PRODUCTO=BD1013R&COLOR_SELECCIONADO=rgb(217,%20159,%20156)&TALLA_SELECCIONADA=CH&CANTIDAD_SELECCIONADA=2

$cuentaCliente = filter_var($_REQUEST["CUENTA_CLIENTE"], FILTER_SANITIZE_STRING);
$idProducto = filter_var($_REQUEST["ID_PRODUCTO"], FILTER_SANITIZE_STRING);
$colorSeleccionado = filter_var($_REQUEST["COLOR_SELECCIONADO"], FILTER_SANITIZE_STRING);
$tallaSeleccionada = filter_var($_REQUEST["TALLA_SELECCIONADA"], FILTER_SANITIZE_STRING);
$cantidadSeleccionada= filter_var($_REQUEST["CANTIDAD_SELECCIONADA"], FILTER_SANITIZE_STRING);






/* 
 * Debemos recibir los datos del cliente y los datos del producto seleccionado por el cliente
 * lo insertaremos a la tabla y una vez insertado descontaremos esas existencias de las existencias de ese producto
 * 
 */

$respuesta = ["estatus"=>"", "mensaje"=>""];

if ($cuentaCliente == "000") {  
    //si es un Invitado
    $respuesta["estatus"] = "Oh no!";
    $respuesta["mensaje"] = "No puedes agregar productos al carrito mientras eres Invitado";
    echo json_encode($respuesta,true);
    die();
}


$fechaVisita = $dataBase->clientes_consultarFechaFuturaVisita($cuentaCliente);
$fechaCierre = $dataBase->clientes_consultarFechaCierrePedido($cuentaCliente);
    
    try {

        //$dataBase->carrito_sePuedeAgregarProducto($cuentaCliente);      //este metodo retornara excepciones dependiendo de si se pueden agregar los productos
        
        //comprobamos el credito del cliente
        
        //consultamos el valor del producto que esta agregando a su carrito y consultamos la ocupacion del carrito actual del cliente
        $distribucionPorAgregar = $dataBase->producto_consultarPrecioDistribucion($cuentaCliente, $idProducto, $colorSeleccionado, $tallaSeleccionada);
        $totalHipoteticoPorAgregar = $distribucionPorAgregar * $cantidadSeleccionada;
        $creditoExedidoRespuesta = $dataBase->carrito_evaluarIngresoDeProductoPorCredito($cuentaCliente, $totalHipoteticoPorAgregar);
        //print_r($creditoExedidoRespuesta);
        /*
         * Array
           (
            [excedidoPor] => 262
            [limiteCredito] => 3700
            [fechaEntrega] => 2021-05-21
            [creditoActualOcupado] => 1962
            [creditoDisponible] => 0
            [totalPorAgregar] => 2000
            [totalFuturo] => 3962
            )         
         */
        
        if($creditoExedidoRespuesta["excedidoPor"]<150){
            
            
                if ($dataBase->productos_existenciasComprobar($idProducto, $colorSeleccionado, $tallaSeleccionada, $cantidadSeleccionada)) {

                if ($dataBase->carrito_agregarProducto($cuentaCliente, $idProducto, $colorSeleccionado, $tallaSeleccionada, $cantidadSeleccionada)) {

                    $dataBase->productos_existenciasDecrementarExistencias($idProducto, $colorSeleccionado, $tallaSeleccionada, $cantidadSeleccionada);                
                    $respuesta["estatus"] = "EXITO";
                    $respuesta["mensaje"] = utilitarios_Class::calcularMensajeAgregarCarrito($fechaVisita, $fechaCierre);
                    
                } else {
                    //si ocurrio algun error le informamos al dispositivo movil y retornamos la lista de productos que hay en su carrito
                    $respuesta["estatus"] = "Error del Servidor";
                    $respuesta["mensaje"] = "No se agrego correctamente el producto al carrito por favor vuelve a intentarlo";
                }

            } else {
                $cantidadDisponibles = $dataBase->productos_consultarExistenciasPorModeloColorTalla($idProducto, $colorSeleccionado, $tallaSeleccionada);            
                $respuesta["estatus"] = "Oh no!";                      
                $respuesta["mensaje"] = "No fue posible agregar el producto a tu carrito\ndebido a que no hay existencias suficientes.\n\nEstas agregando $cantidadSeleccionada"
                        . ($cantidadSeleccionada==1?" pieza": " piezas")
                        . " y tenemos $cantidadDisponibles disponibles\n\n"
                        . "Algun otro cliente agrego este producto a su carrito antes que tú";
            }
            
            
            
        }else{
            //el cliente ya no tiene credito disponible y esta excedido
            $respuesta["estatus"] = "Oh no!";
            $respuesta["mensaje"] = "Tu limite de credito ya fue excedido por $". $creditoExedidoRespuesta["excedidoPor"]. "\nPara continuar comprando dirigete a tu carrito y cambia la forma de pago a INVERSIONISTA.";
        }
        

        
        
        
        
    } catch (Exception $ex) {
        //en casod e alguan excepcion del metodo sePuedeAgregarProducto entonces le retornamos en emnsaje la excepcion al movil el motivo del porque no se puede agregar a suc arrito
        $respuesta["estatus"] = "Oh no!";
        $respuesta["mensaje"] = $ex->getMessage();
    }
    
    
    
    
    







echo json_encode($respuesta,true);



