<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once './base_de_datos_Class.php';
include_once './utilitarios_Class.php';
$dataBase = new base_de_datos_Class();
header('Content-type: application/json; charset=utf-8');


$cuentaCliente = filter_var($_REQUEST["CUENTA_CLIENTE"], FILTER_SANITIZE_STRING);



if ($cuentaCliente != "000") {
    
    $fechaVisita = $dataBase->clientes_consultarFechaFuturaVisita($cuentaCliente);           //para calcular los mensajes de entrega del pedido
    $fechaCierre = $dataBase->clientes_consultarFechaCierrePedido($cuentaCliente);
    $resultados["categorias"] = $dataBase->productos_consultarCategorias();
    $resultados["datosOffline"] = $dataBase->productos_consultarDatosGeneralesDetallados();
    $resultados["datosCliente"] = $dataBase->clientes_consultarClienteConFechaCierre($cuentaCliente);
    $resultados["mensajeEntregaProducto"] = utilitarios_Class::calcularMensajeEntregaProducto($fechaVisita, $fechaCierre); //si ordenas ahora este producto llegara en 6 dias en el pedido de bla bla bla
} else {
    //si la cuenta del cliente es igual a 000 significa que se entro como invitado a la app
    $resultados["categorias"] = $dataBase->productos_consultarCategorias();
    $resultados["datosOffline"] = $dataBase->productos_consultarDatosGeneralesDetallados();
    $resultados["datosCliente"] = array("nombre" => "INVITADO",
        "zona" => "INVITADO",
        "fecha" => utilitarios_Class::dameFecha_dd_mm_aaaa_ToText(),
        "grado" => "EMPRESARIA",
        "credito" => "6000",
        "dias" => "14",
        "puntos_disponibles" => "0");
    $resultados["mensajeEntregaProducto"] = "";
}



echo json_encode($resultados, true);

//echo "Hay una NUEVA version de la app KaliopeClientes disponible en la playStore por favor actualizala para seguir utilizando la app.";
