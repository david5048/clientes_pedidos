<?php

/* 
 * El telefono movil nos enviara el id del producto que esta seleccionado
 * y nos enviara el color que el cliente ha seleccionado en el selector
 * 
 * nosotros enviaremos solo las 3 imagenes de ese color para que las visualise el cliente
 * y ademas enviaremos que tallas hay disponibles de ese color y cuantas existencias de dicha talla
 */

include_once './base_de_datos_Class.php';
$dataBase = new base_de_datos_Class();

$idProducto = filter_var($_REQUEST["ID_PRODUCTO"], FILTER_SANITIZE_STRING);
$noColorSeleccionado = filter_var($_REQUEST["COLOR_SELECCIONADO"], FILTER_SANITIZE_STRING);


$respuesta = ["imagenesColor"=>$dataBase->productos_consutarDetallePorColor($idProducto, $noColorSeleccionado),
    "tallas"=>$dataBase->productos_consultarTallasPorColor($idProducto, $noColorSeleccionado)];


echo json_encode($respuesta, true);











