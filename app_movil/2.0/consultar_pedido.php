<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



include_once './base_de_datos_Class.php';
include_once './utilitarios_Class.php';
header('Content-type: application/json; charset=utf-8');
$dataBase = new base_de_datos_Class();

$cuentaCliente = filter_var($_REQUEST["CUENTA_CLIENTE"], FILTER_SANITIZE_STRING);
//$cuentaCliente = 1265;


$fechaEntregaCarrito = $dataBase->carrito_consultarPedidoMasReciente($cuentaCliente);//solo la usamos para calcular los mensajes del bloqueo, seria mejor que la usaramos para consultar todo lo demas
$diasBloqueo = $dataBase->clientes_consultarDiasAntesPedido($cuentaCliente);        //para los mensajes de bloqueo del pedido
$respuestaCarritoCLiente = $dataBase->carrito_consultarCarrito($cuentaCliente); //este metodo solito consulta el pedido mas resiente



if($respuestaCarritoCLiente == null){
    //el carrito esta vacio
    $respuesta["info"] = ["estatus"=>"FAIL", "MENSAJE"=> ["mensaje"=>"¡Oh no!\nTu carrito aun esta vacio\nagrega productos a tu carrito"]];
    $respuesta["carritoCliente"] = [];
    
}else{
    $respuesta["info"] = ["estatus"=>"EXITO", "MENSAJE"=> utilitarios_Class::calcularMensajeBloqueoCarrito($fechaEntregaCarrito, $diasBloqueo)];
    $respuesta["carritoCliente"] = $respuestaCarritoCLiente;
}
$totales = $dataBase->carrito_calcularTotales($cuentaCliente);

$respuesta["totales"] = $totales;
$respuesta["mensajesFinalTotales"] = utilitarios_Class::crearMensajesBrevesSoloTotalesFinales($totales);
echo json_encode($respuesta,true);


