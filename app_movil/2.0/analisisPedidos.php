<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once './utilitarios_Class.php';



try {
            $conexion = new PDO('mysql:host=127.0.0.1;dbname=kaliope_pedidos', 'kaliope_kaliope', 'ja2408922007');
            
        } catch (Exception $exc) {
            echo "Error".$exc->getMessage();
        }


$fechaInicio = filter_var($_REQUEST["1"], FILTER_SANITIZE_STRING);
$fechaFinal = filter_var($_REQUEST["2"], FILTER_SANITIZE_STRING);
//$fechaFinal = '2021-06-25';

//consultamos los pedidos totales
$rutas = ['A1',
    'A2',
    'A3',
    'A4',
    'Q1',
    'Q2',
    'Q3',
    'Q4',
    'M1',
    'M2',
    'M3',
    'M4'];
$sucursales = ['A','Q','M'];


$statement = $conexion->prepare("SELECT * FROM carritos_clientes WHERE fecha_entrega_pedido>=? AND fecha_entrega_pedido<=? GROUP BY nombre_cliente");
$statement->execute([$fechaInicio,$fechaFinal]);
$resultado = $statement->rowCount();
//print_r($resultado);
echo "Cantidad de Pedidos $resultado \n";



$statement = $conexion->prepare("SELECT SUM(cantidad), SUM(cantidad * precio_empresaria) FROM carritos_clientes WHERE fecha_entrega_pedido>=? AND fecha_entrega_pedido<=?");
$statement->execute([$fechaInicio,$fechaFinal]);
$resultado = $statement->fetch();
//print_r($resultado);
/*
 * Array
(
    [SUM(cantidad)] => 226
    [0] => 226
    [SUM(cantidad * precio_empresaria)] => 43187
    [1] => 43187
)
 */

$cantidadPiezas = $resultado[0];
$cantidadDinero = $resultado[1];
echo "Cantidad de piezas $cantidadPiezas, Cantidad Efectivo $cantidadDinero \n";





echo "\nPedidos por sucursal\n";
echo"Suc\tPed\tPz\t$\n"; 
foreach ($sucursales as $suc) {
    $statement = $conexion->prepare("SELECT * FROM carritos_clientes WHERE fecha_entrega_pedido>=? AND fecha_entrega_pedido<=? AND asignado LIKE '$suc%' GROUP BY nombre_cliente");
    $statement2 = $conexion->prepare("SELECT SUM(cantidad), SUM(cantidad * precio_empresaria) FROM carritos_clientes WHERE fecha_entrega_pedido>=? AND fecha_entrega_pedido<=? AND asignado LIKE '$suc%'");
    $statement->execute([$fechaInicio,$fechaFinal]);
    $statement2->execute([$fechaInicio,$fechaFinal]);
    $rowCount = $statement->rowCount();
    $resultado = $statement2->fetch();
    print_r($suc);
    echo"\t"; 
    print_r($rowCount);
    $piezas = $resultado[0];
    $dinero = $resultado[1];
    echo"\t$piezas\t$dinero\t"; 
    echo"\n";    
}






echo "\nPedidos por Ruta\n";
echo"Suc\tPed\tPz\t$\n"; 
$statement = $conexion->prepare("SELECT * FROM carritos_clientes WHERE fecha_entrega_pedido>=? AND fecha_entrega_pedido<=? AND asignado =? GROUP BY nombre_cliente");
$statement2 = $conexion->prepare("SELECT SUM(cantidad), SUM(cantidad * precio_empresaria) FROM carritos_clientes WHERE fecha_entrega_pedido>=? AND fecha_entrega_pedido<=? AND asignado=?");

foreach ($rutas as $rut) {
    
    $statement->execute([$fechaInicio,$fechaFinal,$rut]);
    $statement2->execute([$fechaInicio,$fechaFinal,$rut]);
    $numeroFilas = $statement->rowCount();
    $resultado = $statement2->fetch();
    //print_r($resultado);    
    $piezas = $resultado[0];
    $dinero = $resultado[1];
    print_r($rut);
    echo"\t";
    print_r($numeroFilas);
    echo"\t$piezas\t$dinero\t";   
    echo"\n";    
}






echo "\n\n\nPedidos por Dia\n";
$statement = $conexion->prepare("SELECT * FROM carritos_clientes WHERE fecha_entrega_pedido=? GROUP BY nombre_cliente");


for($i=0;$i<5;$i++){
    $fecha = utilitarios_Class::sumarDiasFechaYYY($fechaInicio, $i);
    echo "$fecha\t";
    $statement->execute([$fecha]);
    $resultado = $statement->rowCount();
    print_r($resultado);
    echo"\n";
}



echo "\n\n\nPedidos Detallados por Dia\n";
$statement = $conexion->prepare("SELECT * FROM carritos_clientes WHERE fecha_entrega_pedido=? AND asignado =? GROUP BY nombre_cliente");
$statement2 = $conexion->prepare("SELECT SUM(cantidad), SUM(cantidad * precio_empresaria) FROM carritos_clientes WHERE fecha_entrega_pedido=? AND asignado=?");
$statement3 = $conexion->prepare("SELECT SUM(cantidad), SUM(cantidad * precio_empresaria) FROM carritos_clientes WHERE fecha_entrega_pedido=? AND no_cuenta=?");

for($i=0;$i<5;$i++){
    $fecha = utilitarios_Class::sumarDiasFechaYYY($fechaInicio, $i);
    echo "\n$fecha\n";
    

foreach ($rutas as $rut) {
    
    $statement->execute([$fecha,$rut]);
    $rowCount = $statement->rowCount();
    $statement2->execute([$fecha,$rut]);    
    $infoClientes = $statement->fetchAll();
    $resultado = $statement2->fetch();
    //print_r($infoClientes);
    //echo"**************\t"; 
    echo"\t,,"; 
    print_r($rut);
    echo",\t"; 
    print_r($rowCount);
    echo",\t\t";
    //print_r($resultado);
    $piezas = $resultado[0];
    $dinero = $resultado[1];
    echo"$piezas,\t\t$dinero\t,";  
    //echo"**************\n"; 
    echo"\n"; 
    
    if(!empty($infoClientes)){
        foreach ($infoClientes as $cliente) {
        $numCuenta = $cliente['no_cuenta'];
        $nombre = $cliente['nombre_cliente'];
        $nombre = substr($nombre, 0,15);
        $grado = $cliente['grado_cliente'];
        $grado = substr($grado, 0,5);
        $zona = $cliente['ruta'];
        $zona = substr($zona, 0,5);
        
        $statement3->execute([$fecha,$numCuenta]);
        $totalesCliente = $statement3->fetch();
        
        $piezasCliente = $totalesCliente['0'];
        $totalCliente = $totalesCliente['1'];
        
        echo "$numCuenta,\t$nombre,\t\t$grado,\t$zona,\t\t$piezasCliente,\t$totalCliente\n";
    }
    }
    
    
    
  
    echo"\n";    
}
echo "\n\n\n\n";
}








    







