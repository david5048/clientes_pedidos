<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


header('Content-type: application/json; charset=utf-8');

include_once './utilitarios_Class.php';
include_once './base_de_datos_Class.php';

$dataBase = new base_de_datos_Class();

$usuario = filter_var($_REQUEST["usuario"], FILTER_SANITIZE_STRING);
$UUID = filter_var($_REQUEST["UUID"], FILTER_SANITIZE_STRING);
$modeloMovil = filter_var($_REQUEST["modeloDispositivo"], FILTER_SANITIZE_STRING);
$identificador = filter_var($_REQUEST["identificador"], FILTER_SANITIZE_STRING);


$fechaHora = utilitarios_Class::fechaActualParaBasesDeDatos();



//Si es uno significa que conectaremos enviando todas las credenciales desde un inicio de sesion nuevo
if($identificador==1){    
    $password = filter_var($_REQUEST["password"], FILTER_SANITIZE_STRING);
    
    $mantenerSesionIniciada = filter_var($_REQUEST["mantenerSesionIniciada"], FILTER_SANITIZE_STRING);
    $mantenerSesionIniciada = $mantenerSesionIniciada == "false" ? false : true;    //convertimos el string que se recibe a booleano
    
    try {
    $dataBase->usuarios_comprobarCredenciales($usuario, $password);
    $numeroCuenta = $dataBase->usuarios_consultarComprobarNumeroCuenta($usuario);
    $dataBase->usuarios_actualizarInfoInicioSesion($usuario, $mantenerSesionIniciada, $modeloMovil, $UUID, $fechaHora);
    
    
    
    
    //creamos la respuesta en un array que se enviara al movil en json
    //esto para no crearlo manualmente y hacernos bolas cuando se añade mucha informacion
    $respuesta=[];
                                                
    
    
    $respuesta["informacion"] = ["id"=>"inicio de sesion exitoso $UUID $mantenerSesionIniciada", "usuario"=>$usuario];
    //$respuesta["movimientosCliente"] = $dataBase->clientes_consultarCliente(6864);
    $respuesta["datosPersonales"] = $dataBase->clientes_consultarDatosCliente($numeroCuenta);
    
    //$respuesta["categorias"]=$dataBase->productos_consultarCategorias();
    //$respuesta["id_productos"]=$dataBase->productos_consultarIdUnicoDelProducto();
    //$respuesta["imagenPantallaPrincipal"] = $dataBase->productos_crearImagenPrincipal();
    //print_r($respuesta);
    
    echo json_encode($respuesta,true);
        
    //{"informacion":{"id":"inicio de sesion exitoso 123518546 1"}}
    
    
} catch (Exception $exc) {
    echo $exc->getMessage();
}
    
    
}else if($identificador==2){
    //Si el identificador es 2 significa que se esta conectando para comprobar que exista un inicio de secion por mantener en este dispositivo
    $respuesta = [];
    
    //consultamos en la tabla usuarios_sesiones si concuerdan el usuario el codigo de dispositivo unico
    if($dataBase->usuarios_comprobarMantenerSesion($usuario, $UUID, $fechaHora, $modeloMovil)){
        $respuesta["informacion"] = ["estatus"=>"EXITO","info"=>"Se encontro mantener sesion $UUID ", "usuario"=>$usuario];
        $dataBase->usuarios_actualizarHoraSesion($UUID, $fechaHora);
    }else{
        $respuesta["informacion"] = ["estatus"=>"FAIL","info"=>"No se encontro mantener sesion $UUID ", "usuario"=>$usuario];
    }
    
    echo json_encode($respuesta, true);
    
}else if($identificador==3){
    //Si el identificador es 3 significa que se esta conectando para cerrar la sesion
    $respuesta = [];
    
    //consultamos en la tabla usuarios_sesiones si concuerdan el usuario el codigo de dispositivo unico si es asi entonces lo eliminamos de la tabla mantener sesion
    if($dataBase->usuarios_comprobarMantenerSesion($usuario, $UUID, $fechaHora, $modeloMovil)){
        $respuesta["informacion"] = ["estatus"=>"EXITO","info"=>"Su sesion a finalizado con exito ", "usuario"=>$usuario];
        $dataBase->usuarios_eliminarSesionIniciada($usuario,$UUID);
    }else{
        $respuesta["informacion"] = ["estatus"=>"EXITO","info"=>"Su sesion a finalizado con exito ", "usuario"=>$usuario];
    }
    
    echo json_encode($respuesta, true);
    
}





