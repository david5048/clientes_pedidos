<?php

/* 
 * cuando el cliente modifique un producto de su pedido conectaremos a este archivo
 * recibiremos un codigo de operacion dependiendo de lo que quiere hacer por ejemplo
 * 1 -cambiar metodo de pago credito o inversionista
 * 2- confirmar producto true o false
 * 3- eliminar producto
 * 
 * el id unico del renglon de producto que vamos a editar
 * 
 * recibiremos tambien el numero de cuenta del cliente y buscaremos la manera de validar su secion con nuestra tabla
 * para proceder a editar el numero de pedido
 * 
 * devolveremos un json indicando que todo salio bien
 */

include_once './base_de_datos_Class.php';
$dataBase = new base_de_datos_Class();

$cuentaCliente = filter_var($_REQUEST["CUENTA_CLIENTE"], FILTER_SANITIZE_STRING);
$operacion = filter_var($_REQUEST["OPERACION"], FILTER_SANITIZE_STRING);
$idBaseDatosProducto = filter_var($_REQUEST["ID_PRODUCTO"], FILTER_SANITIZE_STRING);




if($operacion == 1){
    $metodoPago = filter_var($_REQUEST["METODO_PAGO"], FILTER_SANITIZE_STRING);
    //editamos la forma de pago
    if($dataBase->carrito_editarProductoMetodoPago($idBaseDatosProducto, $metodoPago)){
        $respuesta["resultado"] = "Exito";
        $respuesta["mensaje"] = "Hemos cambiado tu pago a $metodoPago";
    }else{
        $respuesta["resultado"] = "Error";
        $respuesta["mensaje"] = "No ha sido posible cambiar el metodo de pago del producto vuelve a intentarlo"; 
    }
    
    $totales = $dataBase->carrito_calcularTotales($cuentaCliente);
    
    $respuesta["totales"] = $totales;
    $respuesta["mensajesFinalTotales"] = utilitarios_Class::crearMensajesBrevesSoloTotalesFinales($totales);
    

    echo json_encode($respuesta, true);
}




if($operacion == 2){
    // significa que el cleinte esta eliminando un producto   
    
    if($dataBase->carrito_eliminarProducto($idBaseDatosProducto)){
        $respuesta["resultado"] = "Exito";
        $respuesta["mensaje"] = "Hemos eliminado el producto de tu carrito";  
        
    }else{
        $respuesta["resultado"] = "Error";
        $respuesta["mensaje"] = "No ha sido posible eliminar el producto por favor reintenta";  
    }
    
    /*
     * Agrego la parte de dias bloqueo, y los mensajes no me gusta mucho tenerlos qeu reenviar
     * pero ocurre que como en la talba no hay algo que indique pedido finalizado cuando el cliente
     * elimina todos los productos de un carrito, este metodo le devuelve el carrito previo al que elimino
     * y eso se vuelve algo confuso, entonces recalculamos los mensajes para que le devuelva a la app que este
     * pedido que le estamos devolviendo va marcado como pedido finalizado y asi no desplegara la app los items ni totales
     * sino la bolsita de carrito vacio y ver historial de pedidos
     * 
     */
    $fechaEntregaCarrito = $dataBase->carrito_consultarPedidoMasReciente($cuentaCliente);               //devolvera la ultima fecha del registro carritos
    $diasBloqueo = $dataBase->clientes_consultarDiasAntesPedido($cuentaCliente);
    $totales = $dataBase->carrito_calcularTotales($cuentaCliente);
    $carritoActualizado = $dataBase->carrito_consultarCarrito($cuentaCliente);
    
    
    $respuesta["totales"] = $totales;
    $respuesta["mensajesFinalTotales"] = utilitarios_Class::crearMensajesBrevesSoloTotalesFinales($totales);
    $respuesta["info"] = ["estatus"=>"EXITO", "MENSAJE"=> utilitarios_Class::calcularMensajeBloqueoCarrito($fechaEntregaCarrito, $diasBloqueo)];
    
    
    
    
    echo json_encode($respuesta, true);
    
    }
    
    
    


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

//DEPRECATED no se necesita mas la confirmacion del pedido
    if($operacion == 3){
        //Significa que vamos a confirmar los productos que el cliente tiene en su carrito
        
        
        //se ejecuta el metodo y nos retorna un array con contadores de cuantos productos no se confirmaron y cuantos eran totales
        $respuestaMetodo = $dataBase->carrito_confirmarProducto($cuentaCliente);
        //print_r($respuestaMetodo);
        /*
         * array(
            * contadorTotalPorConfirmar =>5
            * contadorAgotadosAntes =>2
         * )
         */
        
        
        
        $contadorTotal = $respuestaMetodo['contadorTotalPorConfirmar'];   
        $contadorAgotados = $respuestaMetodo['contadorAgotadosAntes'];
        $productosConfirmados = $contadorTotal - $contadorAgotados;

        
        //si hay piezas que no se confirmaron proque las existencias se agotaron mostraremos un mensaje al cliente
        if($contadorAgotados>0){
            
            if($productosConfirmados==0){
                $mensaje = "No hemos podido confirmar ninguno de tus productos porque las existencias se agotaron antes de que confirmaras tu pedido."
                        . "\n\nRecomendamos apresurarte a confirmar tu pedido lo mas pronto posible antes que otros clientes confirmen los suyos."
                        ." \n\nPodras verlos en tu carrito como AGOTADOS, pero recomendamos eliminarlos de él";
            }else if($productosConfirmados == 1){
                $mensaje = "Hemos confirmado 1 solo producto de un total de $contadorTotal sin confirmar que habia en tu carrito.\n\nHemos apartado las existencias del almacen Kaliope para surtir tu pedido."
                    . "\n\n$contadorAgotados ".($contadorAgotados>1?"productos no fueron confirmados ":"producto no fue confirmado ")." porque las existencias se agotaron antes de que confirmaras tu pedido."
                        . "\n\nPodras ver los productos AGOTADOS en tu carrito, pero recomendamos eliminarlos de él y apresurarte a confirmar tu pedido lo mas pronto posible.";
            }else if($productosConfirmados>1){
                $mensaje = "Hemos confirmado $productosConfirmados productos de un total de $contadorTotal sin confirmar que habia en tu carrito."
                    . "\n\n$contadorAgotados de ellos ".($contadorAgotados>1?"no fueron confirmados ":"no fue confirmado ")." porque las existencias se agotaron antes de que confirmaras tu pedido."
                      . "\n\nRecomendamos apresurarte a confirmar tu pedido lo mas pronto posible antes que otros clientes los confirmen."
                        . "\n\nPodras ver los productos AGOTADOS en tu carrito, pero recomendamos eliminarlos de él";
            }
            
        }else{
            $mensaje = "Hemos confirmado la totalidad de tus productos pendientes de confirmar que habia en tu carrito.\n\nHemos apartado las existencias del almacen Kaliope para surtir tu pedido.";
        }
        
        
        $totales = $dataBase->carrito_calcularTotales($cuentaCliente);
        $mensajesFinalTotales = utilitarios_Class::crearMensajesBrevesSoloTotalesFinales($totales);
        
        
        
        $respuesta=['resultado'=>"Exito",
            'mensaje'=>$mensaje,
            'carritoCliente'=>$dataBase->carrito_consultarCarrito($cuentaCliente),
            'totales'=>$totales,
            'mensajesFinalTotales' => $mensajesFinalTotales];
        
        echo json_encode($respuesta, true);
        
    }

    //DEPRECATED no se necesita mas la preconfirmacion pero la dejamos por amor proque la neta me la rife xD
    if($operacion == 4){
        /*si lo que queremos es hacer una preconfirmacion necesitamos obtener como dice la documentacion
         * Enviaremos todos los menasajes que debe mostrar la app, quiero que todo lo haga el ervidor
         * enviaremos un mensaje json con todos los datos que necesita la app para mostrar estos datos son:
         * Los productos que se confirmaran
         * mensaje 1 Esto apartara las existencias de los almacenes Kaliope.
         * mensaje 2 Recuerda confirmar lo mas pronto posible estas piezas para garantizar las existencias del producto.
         * 
         * mensaje3-> Al recibir tu a crédito tú pagaras el 50% y Kaliope te financia 14 días el 50% restante
         * mensaje4->Tu pedido actual tiene:
         * mensaje5->En productos a credito
         * mensaje6->Deberas dar:
         * mensaje7->al recibir tu pedido
         * porcentaje_ayuda_empresa -> 0.5
         * porcentaje_pago_cliente -> 0.5
         * total a credito -> 1500
         * cantidad a pagar cliente -> 750        
         * mensaje4 -> Con forme aumente tu historial Kaliope aumentaremos tu financiamiento
         * mensaje5 -> Recomendamos el modo de pago “Inversión” para que obtengas los mejores precios
         * 
         * limite_credito -> 1400
         * excedente -> 500               la cantidad por la que el cliente exede si es que lo hace su limite de credito
         * mensaje5-> Tu crédito Kaliope de $2000 será sobre pasado por: 
         * mensaje 6-> Tendrás que dar esta diferencia en efectivo al recibir tu pedido. 
         * mensaje 7->  Recomendamos que cambies el método de pago a “Inversión” en algún producto para que obtengas el costo mas bajo.
         * 
         * mensaje 8-> Tu pago por Inversión al recibir tu pedido será de:
         * total Inversion -> $2400
         * mensaje 9-> Felicidades has obtenido el precio mas bajo por producto.
         * 
         * Si total Inversion = 0 mostraremos estos mensajes
         * mensaje8-> No tienes ningún producto en “Inversión” recuerda que con esta forma de pago obtendrás los costos mas bajos de Kaliope
         * mensaje9-> Anímate a invertir para tu siguiente pedido y descubre Kaliope Inversiones!
         * 
         * mensaje 10-> Al recibir tu pedido deberás liquidar al agente Kaliope:
         * cantidad_en_credito -> 750
         * mensaje11-> En crédito Kaliope fecha de pago 09-05-2021 
         * por_liquidar_al_recibir = $3650
         * 
         * para obtener la fecha del pago de credito usaremos la fecha de entrega que tiene marcado el pedido y le sumaremos los dias de credito
         * 
         */
        $productosSinConfirmar = $dataBase->carrito_consultar_sin_confirmar($cuentaCliente);            //obtenemos la lista de productos sin confirmar
        /*
         * Array
        (
            [0] => Array
                (
                    [cantidad] => 1
                    [id_producto] => PD5898
                    [descripcion] => Pantalon Dama
                    [color] => AZUL
                    [precio_etiqueta] => 429
                    [imagen_permanente] => fotos/PD5898-AZUL-1.jpg
                    [estado_producto] => INVERSION
                )

            [1] => Array
                (
                    [cantidad] => 1
                    [id_producto] => SM5898
                    [descripcion] => Sudadera dama
                    [color] => Gris
                    [precio_etiqueta] => 339
                    [imagen_permanente] => fotos/SM5898-VERDE-1.jpg
                    [estado_producto] => CREDITO
                )
          [2] => Array
                (
                    [cantidad] => 1
                    [id_producto] => SM5898
                    [descripcion] => Sudadera dama
                    [color] => verde
                    [precio_etiqueta] => 339
                    [imagen_permanente] => fotos/SM5898-VERDE-1.jpg
                    [estado_producto] => CREDITO
                )


        )
         */
        
        //la clave de esta preconfirmacion la tiene este metodo, tiene toda la informacion que necesitamos
        $infoTotales = $dataBase->carrito_calcularTotales($cuentaCliente);
        /*
         *  Array
        (
            [nombre] => MONICA HERNANDEZ GARCIA
            [cuenta] => 4926
            [limite_credito] => 1400
            [grado] => VENDEDORA
            [dias] => 14
            [ruta] => EL PALMITO
            [porcentaje_apoyo_empresa] => 0.35
            [porcentaje_pago_cliente] => 0.65
            [numero_pedido] => 1
            [fecha_entrega] => 2021-05-15
            [fecha_pago_del_credito] => 29-05-2021
            [suma_cantidad] => 8
            [suma_credito] => 6
            [suma_inversion] => 2
            [cantidad_sin_confirmar] => 3
            [suma_productos_etiqueta] => 2822
            [suma_productos_inversion] => 560
            [suma_productos_credito] => 1800
            [suma_ganancia_cliente] => 462
            [diferencia_credito] => 400
            [cantidad_pagar_cliente_credito] => 1170
            [pago_al_recibir] => 2130
            [mensaje_diferencia_credito] => Tu total de credito ha pasado por $400 tu limite de credito, deberas dar esa diferencia en efectivo al recibir tu paquete, tambien puedes cambiar un producto a Inversion o eliminar productos
            [mensaje_todo_inversion] => Si pagaras todo tu pedido en Inversion ganarias $582
            [mensaje_resumido_puntos] =>  + 450 puntos Kaliope
            [mensaje_completo_puntos] => Tambien has ganado 450 puntos Kaliope, recueda que estos puntos se validaran con tu agente Kaliope y seran solo si realizas los pagos completos de este pedido.
            [mensaje_cantidad_sin_confirmar] => Tienes 3 productos sin confirmar, envialos!
        )
         */
        
        //convertimos los financiamientos de float a porcentaje
        $porcentajeFinanciamientoEmpresa = $infoTotales['porcentaje_apoyo_empresa'] * 100;
        $porcentajePagoCliente = $infoTotales['porcentaje_pago_cliente'] * 100;
        $diasCredito = $infoTotales['dias'];
        $fechaPagoDelCredito = $infoTotales['fecha_pago_del_credito'];
        $limiteDeCredito = $infoTotales['limite_credito'];

        
        
        
        //creamos un array que tendra todos los mensajes para mostrarse en el sistema
        $mensajes[1] = "Confirmaras estos productos:";
        $mensajes[2] = "Esto apartara las existencias de los almacenes Kaliope";
        $mensajes[3] = "Recuerda confirmar lo mas pronto posible estas piezas para garantizar las existencias del producto.";
        $mensajes[4] = "Al recibir tu a crédito tú pagaras el $porcentajePagoCliente% y Kaliope te financia $diasCredito días el $porcentajeFinanciamientoEmpresa% restante";
        $mensajes[5] = "Tu pedido actual tiene:";
        $mensajes[6] = "en productos a crédito";
        $mensajes[7] = "Tendras que pagar al recibir:";
        $mensajes[8] = "$porcentajePagoCliente% del pedido";
        $mensajes[9] = "Con forme aumente tu historial Kaliope aumentaremos tu financiamiento";
        $mensajes[10] = "Quedaran pendientes:";
        $mensajes[11] = "para el $fechaPagoDelCredito" ;
        $mensajes[12] = "Recomendamos el modo de pago “Inversión” para que obtengas los mejores precios";
        $mensajes[13] = "Tu crédito Kaliope de $$limiteDeCredito esta siendo sobre pasado por:";
        $mensajes[14] = "Tendrás que dar esta diferencia en efectivo al recibir tu pedido.";
        $mensajes[15] = "Recomendamos que cambies el método de pago a “Inversión” en algún producto para que obtengas el costo mas bajo.";
        $mensajes[16] = "Tu pago por Inversión al recibir tu pedido será de:";
        $mensajes[17]= "¡Felicidades! has obtenido el precio mas bajo en estos productos.";
        $mensajes[18]= "Al recibir tu pedido deberás liquidar al agente Kaliope:";
        $mensajes[19]= "Exceso de credito";
        $mensajes[20]= "$porcentajePagoCliente% credito";
        $mensajes[21]= "Inversion";
        $mensajes[22]= "por liquidar al recibir el pedido";
        $mensajes[23] = "En crédito Kaliope fecha de pago $fechaPagoDelCredito";
        
        if($infoTotales['diferencia_credito']<0){
            //si la diferencia de credito es menor a a0 significa que el cliente no ha sobrepasado su limite de credito y la catidad que marca en negativo es lo que aun puede usar
            //lo que haremos si aun no rebalsa su credito es borrar los mensajes que indican eso solo para ahorrar bytes
            $mensajes[13]="";
            $mensajes[14]="";
            $mensajes[15]="";
        }
        
        if($infoTotales['suma_productos_inversion']==0){
            $mensajes[16]= "No tienes ningún producto en “Inversión” recuerda que con esta forma de pago obtendrás los costos mas bajos de Kaliope";
            $mensajes[17]= "Anímate a invertir en tu siguiente producto y descubre Kaliope Inversiones!";
        }
        
        
        
                
        
        
        
        
        //creamos nuestro array que contendra todas las respuestas
        $respuesta["productos"] = $productosSinConfirmar;
        //$respuesta["totales"] = $infoTotales;                         //no tiene caso que volvamos a enviar los totales porque la app ya los tiene de cuando se entra la primera vez al carrito y son los mismos totales nada cambia
        $respuesta["mensajes"] = $mensajes;
        
        //print_r($respuesta);
        /*
         * Array
                (
                    [productos] => Array
                        (
                            [0] => Array
                                (
                                    [cantidad] => 1
                                    [id_producto] => BR1008
                                    [descripcion] => BRASSIER DAMA
                                    [color] => AZULMARINO
                                    [precio_etiqueta] => 189
                                    [imagen_permanente] => fotos/BR1008-AZULMARINO-1.jpg
                                    [estado_producto] => CREDITO
                                )

                            [1] => Array
                                (
                                    [cantidad] => 2
                                    [id_producto] => PT1001
                                    [descripcion] => Pantaleta Dama
                                    [color] => CIELO
                                    [precio_etiqueta] => 79
                                    [imagen_permanente] => fotos/PT1001-CIELO-1.jpg
                                    [estado_producto] => INVERSION
                                )

                            [2] => Array
                                (
                                    [cantidad] => 1
                                    [id_producto] => BD1002
                                    [descripcion] => BLUSA DAMA
                                    [color] => ROSA
                                    [precio_etiqueta] => 329
                                    [imagen_permanente] => fotos/BD1002-ROSA-1.jpg
                                    [estado_producto] => INVERSION
                                )

                        )

                    [totales] => Array
                        (
                            [nombre] => EVA MONDRAGON RIVAS
                            [cuenta] => 2070
                            [limite_credito] => 2400
                            [grado] => SOCIA
                            [dias] => 28
                            [ruta] => ACAMBAY
                            [porcentaje_apoyo_empresa] => 0.5
                            [porcentaje_pago_cliente] => 0.5
                            [numero_pedido] => 1
                            [fecha_entrega] => 2021-05-17
                            [fecha_pago_del_credito] => 14-06-2021
                            [suma_cantidad] => 5
                            [suma_credito] => 2
                            [suma_inversion] => 3
                            [cantidad_sin_confirmar] => 3
                            [suma_productos_etiqueta] => 795
                            [suma_productos_inversion] => 250
                            [suma_productos_credito] => 248
                            [suma_ganancia_cliente] => 297
                            [diferencia_credito] => -2152
                            [cantidad_pagar_cliente_credito] => 124
                            [pago_al_recibir] => 374
                            [mensaje_diferencia_credito] => Aun dispones de $2152 en tu credito Kaliope
                            [mensaje_todo_inversion] => Si pagaras todo tu pedido en Inversion ganarias $315
                            [mensaje_resumido_puntos] => +0 puntos Kaliope
                            [mensaje_completo_puntos] => No has ganado puntos Kaliope en este pedido, pedido minimo para puntos son $500
                            [mensaje_cantidad_sin_confirmar] => Tienes 3 productos sin confirmar, envialos!
                        )

                    [mensajes] => Array
                        (
                            [1] => Esto apartara las existencias de los almacenes Kaliope
                            [2] => Recuerda confirmar lo mas pronto posible estas piezas para garantizar las existencias del producto.
                            [3] => Al recibir tu a crédito tú pagaras el 50% y Kaliope te financia 28 días el 50% restante
                            [4] => Tu pedido actual tiene:
                            [5] => en productos a crédito
                            [6] => Deberás dar:
                            [7] => al recibir tu pedido
                            [8] => Con forme aumente tu historial Kaliope aumentaremos tu financiamiento
                            [9] => Recomendamos el modo de pago “Inversión” para que obtengas los mejores precios
                            [10] => 
                            [11] => 
                            [12] => Recomendamos que cambies el método de pago a “Inversión” en algún producto para que obtengas el costo mas bajo.
                            [13] => Tu pago por Inversión al recibir tu pedido será de:
                            [14] => Felicidades has obtenido el precio mas bajo por producto.
                            [15] => Al recibir tu pedido deberás liquidar al agente Kaliope:
                            [16] => Exceso de credito
                            [17] => 50% credito
                            [18] => Inversion
                            [19] => por liquidar al recibir el pedido
                            [20] => En crédito Kaliope fecha de pago 14-06-2021
                        )

                )
         * 
         */
        echo json_encode($respuesta, true);
        
        
        
    }
    
    
    
    
    
    









