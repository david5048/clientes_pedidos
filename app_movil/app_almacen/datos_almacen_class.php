<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


include_once '../2.0/base_de_datos_Class.php';
include_once '../2.0/utilitarios_Class.php';

class datos_almacen_class extends base_de_datos_Class{


    
    public function consultarTodosLosPedidos() {
        $statement = $this->conexion->prepare("SELECT * FROM carritos_clientes GROUP BY no_cuenta DESC");
        $statement->execute();
        $resultado = $statement->fetchAll();

        return $resultado;
    }
/**
 * Consluta los pedidos que deben ser surtidos
 * @return type
 */
   public function pedidos_por_surtir() {

        $consulta = $this->getConexion()->prepare(
                "SELECT DISTINCT fecha_entrega_pedido FROM carritos_clientes WHERE almacen IS NULL ORDER BY fecha_entrega_pedido ASC"
        );
        $consulta->execute();
        $fechasEntrega = $consulta->fetchAll(PDO::FETCH_ASSOC);


        $respuesta = [];
        foreach ($fechasEntrega as $fecha) {

            $consulta = $this->conexion->prepare("SELECT fecha_entrega_pedido, no_cuenta, nombre_cliente, credito_cliente, grado_cliente, ruta, plataforma, SUM(cantidad) AS sumaCantidad FROM carritos_clientes WHERE fecha_entrega_pedido=? AND almacen IS NULL GROUP BY nombre_cliente ORDER BY nombre_cliente ASC");
            $consulta->execute([$fecha["fecha_entrega_pedido"]]);
            $pedidos_por_surtir = $consulta->fetchAll(PDO::FETCH_ASSOC);

            foreach ($pedidos_por_surtir as $pedido) {
                $cuentaCliente = $pedido["no_cuenta"];
                $fechaEntrega = $pedido["fecha_entrega_pedido"];
                $diasCierre = $this->clientes_consultarDiasAntesPedido($cuentaCliente);
                $sucursal = $this->sucursal_consultarSucursal($pedido["ruta"]);    
                $asignado = $this->sucursal_consultarAsignado($pedido["ruta"]);//A1 M3 Q2 etc
                $fActual = date_create(utilitarios_Class::fechaActualYYYY_mm_dd()->format("d-m-Y"), timezone_open("America/Mexico_City"));
                $fCierre = date_create(utilitarios_Class::restarDiasFechaString($fechaEntrega, $diasCierre), timezone_open("America/Mexico_City"));
                $diffDiasRestantesParaCierre = $fActual->diff($fCierre);
                
                
                
                $pedido["fechaCierre"] = $fCierre->format("d-m-Y");
                $pedido["diasAntes"] = $diasCierre;
                $pedido["diasRestantesParaPoderSurtir"] = $diffDiasRestantesParaCierre->d;
                $pedido["bloqueo"]=false;
                $pedido["mensajesAlAlmacenista"] = "";
                $pedido["sucursal"] = $sucursal;
                $pedido["asignado"] = $asignado;

                
                
                if(!$diffDiasRestantesParaCierre->invert){
                    $diasTemporal = $diffDiasRestantesParaCierre->d;
                    if($diasTemporal>0){
                        $pedido["bloqueo"]=false;
                        $pedido["mensajesAlAlmacenista"] = "Aun no deberias surtir este pedido ".($diasTemporal>1?"faltan\n $diasTemporal dias\n":"falta\n 1 dia\n");
                    }else{
                        $pedido["bloqueo"]=true;
                        $pedido["mensajesAlAlmacenista"] = "Ya puedes surtir este pedido!!";
                    }
                    
                }else{
                    $pedido["bloqueo"]=true;
                    $pedido["mensajesAlAlmacenista"] = "Ya puedes surtir este pedido!!";
                }

                array_push($respuesta, $pedido);
            }
        }

        return $respuesta;
    }

 
        
    public function surtidos_y_enviados2() {

        /*
         * PRIMERO DEBEMOS CONSULTAR TODAS LAS FECHAS QUE EXISTAN DONDE LOS PRODUCTOS YA FUERON SURTIDOS
         * ORDENADOS DE LA MAS NUEVA A LA MAS VIEJA
         */

        $consultaFechas =  $this->getConexion()->prepare(
                "SELECT DISTINCT fecha_entrega_pedido FROM carritos_clientes ORDER BY fecha_entrega_pedido DESC LIMIT 30"
        );
        $consultaFechas->execute();
        $arrayFechasSurtidas = $consultaFechas->fetchAll(PDO::FETCH_ASSOC);        
        /*
         *  Array
          (
          [0] => Array
          (
          [fecha_entrega_pedido] => 2021-07-23
          )

          [1] => Array
          (
          [fecha_entrega_pedido] => 2021-07-22
          )

          [2] => Array
          (
          [fecha_entrega_pedido] => 2021-07-21
          )

          [3] => Array
          (
          [fecha_entrega_pedido] => 2021-07-20
          )

          [4] => Array
          (
          [fecha_entrega_pedido] => 2021-07-19
          )
          )
         */


        $respuesta = [];
        foreach ($arrayFechasSurtidas as $fecha) {
            //por cada fecha ya surtida vamos a consultar por cliente en esa fecha consultada y la metemos al array
            $consulta = $this->conexion->prepare(
                    "SELECT fecha_entrega_pedido, no_cuenta, nombre_cliente, credito_cliente, grado_cliente, ruta, plataforma, SUM(cantidad) AS sumaCantidad FROM carritos_clientes WHERE almacen IS NOT NULL AND fecha_entrega_pedido=? GROUP BY nombre_cliente"
            );
            $consulta->execute([$fecha["fecha_entrega_pedido"]]);
            $pedidosSurtidos = $consulta->fetchAll(PDO::FETCH_ASSOC);
            //print_r($pedido);
            /*
             * Array
              (
              [0] => Array
              (
              [fecha_entrega_pedido] => 2021-07-23
              [no_cuenta] => 8440
              [nombre_cliente] => ALBA JUDITH CRUZ MADRIGAL
              [credito_cliente] => 2000
              [grado_cliente] => VENDEDORA
              [ruta] => ERONGARICUARO
              [producto_confirmado] => true
              [sumaCantidad] => 2
              )

              [1] => Array
              (
              [fecha_entrega_pedido] => 2021-07-23
              [no_cuenta] => 8441
              [nombre_cliente] => ALICIA VALLEJO MENDOZA
              [credito_cliente] => 1500
              [grado_cliente] => VENDEDORA
              [ruta] => SANTA FE DE LA LAGUNA
              [producto_confirmado] => true
              [sumaCantidad] => 7
              )

              )
             * Array
              (
              [0] => Array
              (
              [fecha_entrega_pedido] => 2021-07-22
              [no_cuenta] => 7576
              [nombre_cliente] => CECILIA MELGAREJO AREVALO
              [credito_cliente] => 2800
              [grado_cliente] => EMPRESARIA
              [ruta] => VILLA MADERO
              [producto_confirmado] => true
              [sumaCantidad] => 3
              )

              [1] => Array
              (
              [fecha_entrega_pedido] => 2021-07-22
              [no_cuenta] => 8646
              [nombre_cliente] => LUCIA ARREOLA
              [credito_cliente] => 1500
              [grado_cliente] => VENDEDORA
              [ruta] => HUIRAMBA
              [producto_confirmado] => true
              [sumaCantidad] => 1
              )
              )
             */
            /*
              SI abras notado el inconveniente es que nos devuelve cada vez un array en cada fecha, entonces solo es un problemita
              porque al meterno en otro array creara un array tridimencional, donde el primer index corresponde a 1 fecha, y dentro de esa fecha habra un cliente
              y dentro de ese cliente sus datos.
              no es dificil de manejar pero quiero que a la app le lelge lo mas simple. entonces las 2 opciones son
              consultar en otro metodo los numeros de cuenta unicos ya confirmados y luego en otro bucle for crear consultas independientes para la fehca y el cliente
              e ir llenando la respuesta. o en otro bucle for recorremos ahora la respuesta de clientes que nos entrega y se las metemos a un array independiente
              para que todos los datos devueltos queden en un solo array bidimencional */


            foreach ($pedidosSurtidos as $pedido) {
                $cuentaCliente = $pedido["no_cuenta"];
                $fechaEntrega = $pedido["fecha_entrega_pedido"];
                $diasCierre = $this->clientes_consultarDiasAntesPedido($cuentaCliente);
                $sucursal = $this->sucursal_consultarSucursal($pedido["ruta"]);
                $asignado = $this->sucursal_consultarAsignado($pedido["ruta"]);
                
                $fActual = date_create(utilitarios_Class::fechaActualYYYY_mm_dd()->format("d-m-Y"), timezone_open("America/Mexico_City"));
                $fCierre = date_create(utilitarios_Class::restarDiasFechaString($fechaEntrega, $diasCierre), timezone_open("America/Mexico_City"));
                $diffDiasRestantesParaCierre = $fActual->diff($fCierre);
                
                
                
                $pedido["fechaCierre"] = $fCierre->format("d-m-Y");
                $pedido["diasAntes"] = $diasCierre;
                $pedido["diasRestantesParaPoderSurtir"] = $diffDiasRestantesParaCierre->d;
                $pedido["bloqueo"]=false;
                $pedido["mensajesAlAlmacenista"] = "Pedido Surtido";
                $pedido["sucursal"] = $sucursal;
                $pedido["asignado"] = $asignado;

                
                

                array_push($respuesta, $pedido);
            }
        }


        return $respuesta;
    }


    
    
    
    
    
    
    function surtirPedido($numeroCuenta, $fechaPedido) {

   

        $statement = $this->conexion->prepare(
                "UPDATE carritos_clientes SET almacen = 'surtido' WHERE no_cuenta='$numeroCuenta' AND fecha_entrega_pedido = '$fechaPedido'"
        );
        
        return $statement->execute();
       

      
    
}
    
    
    
    public function consultarDetallePedido(String $numeroCuenta, String $fechaPedido){
        $statement = $this->conexion->prepare("SELECT * FROM carritos_clientes WHERE no_cuenta=? AND fecha_entrega_pedido=?");
        $statement->execute([$numeroCuenta, $fechaPedido]);
        $resultado = $statement->fetchAll(PDO::FETCH_ASSOC);
        
    if(empty($resultado)) {echo "No se encuentran pedidos del cliente $numeroCuenta en la fecha de $fechaPedido metodo consultarDetallePedido";}
        
        for ($i=0; $i<sizeof($resultado); $i++) {
            $producto = $resultado[$i];
            $gradoCliente = $producto["grado_cliente"];
            $precioEtiqueta = $producto["precio_etiqueta"];
            $id_producto = $producto["id_producto"];
            $noColorSeleccionado = $producto["no_color"];
            $tallaSeleccionada = $producto["talla"];
            
        
            
            $ganancia =0;
            $gananciaInversion=0;
            
            if($gradoCliente == "VENDEDORA"){
                $ganancia = $precioEtiqueta - $producto["precio_vendedora"];
                
            }else if($gradoCliente == "SOCIA"){
                $ganancia = $precioEtiqueta - $producto["precio_socia"];

            }else{
                $ganancia = $precioEtiqueta - $producto["precio_empresaria"];
                
            }
            
            $gananciaInversion = $precioEtiqueta - $producto["precio_inversionista"];
            
                    
            
             
            
            
            $producto["ganancia"] = $ganancia;
            $producto["ganancia_inversion"] = $gananciaInversion;
  
            
         
            $resultado[$i] = $producto;             //ingresamos el nuevo array con 2 items mas agregados al array principal
            
        }
        
        //print_r($resultado);
        /*
         * Array
            (
                [0] => Array
                    (
                        [id] => 100
                        [no_pedido] => 2
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
         *              [ganancia] => 21
         *              [ganacia_inversion] => 32
                    )

                [1] => Array
                    (
                        [id] => 101
                        [no_pedido] => 2
                        [fecha_entrega_pedido] => 2020-08-25
                        [no_cuenta] => 4926
                        [nombre_cliente] => MONICA HERNANDEZ GARCIA
                        [credito_cliente] => 1700
                        [grado_cliente] => VENDEDORA
                        [puntos_disponibles] => 100
                        [id_producto] => SM5898
                        [descripcion] => Sudadera dama
                        [talla] => M
                        [cantidad] => 2
                        [color] => Rosa
                        [no_color] => rgb(240, 74, 141)
                        [precio_etiqueta] => 339
                        [precio_vendedora] => 298
                        [precio_socia] => 295
                        [precio_empresaria] => 291
                        [precio_inversionista] => 280
                        [imagen_permanente] => fotos/SM5898-ROSA-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
         *              [ganancia] => 21
         *              [ganacia_inversion] => 32

                        )
        )
         */
        
        return $resultado;
    }
    
    
     
    
    
   /**
     * 
     * @param string $numeroCuenta
     * @return array[] <p> nombreCliente
     *                  <p>cuenta
     * <p>limite_credito
     * <p>grado
     * <p>dias
     * <p>ruta
     * <p>porcentaje_apoyo_empresa
     * <p>porcentaje_pago_cliente
     * <p>numero_pedido
     * <p>fecha_entrega
     * <p>fecha_pago_del_credito
     * <p>suma_cantidad
     * <p>suma_credito
     * <p>suma_inversion
     * <p>cantidad_sin_confirmar
     * <p>suma_productos_etiqueta
     * <p>suma_productos_inversion
     * <p>suma_productos_credito
     * <p>suma_ganancia_cliente
     * <p>diferencia_credito
     * <p>cantidad_pagar_cliente_credito
     * <p>pago_al_recibir
     * <p>mensaje_diferencia_credito
     * <p>mensaje_todo_inversion
     * <p>mensaje_resumido_puntos
     * <p>mensaje_completo_puntos
     * <p>mensaje_cantidad_sin_confirmar
     */
    public function carrito_calcularTot($numeroCuenta, $fechaPedido) {
        
        
        $datos_pedido = $this->consultarDetallePedido($numeroCuenta, $fechaPedido);      
        //print_r($datos_pedido);  
        /*
         * [1] => Array
                    (
                       [id] => 374
                        [no_pedido] => 1
                        [fecha_entrega_pedido] => 2021-06-16
                        [no_cuenta] => 8631
                        [nombre_cliente] => JUDITH VALDEZ BERNAL
                        [credito_cliente] => 1500
                        [grado_cliente] => VENDEDORA
                        [ruta] => SAN LORENZO
                        [id_producto] => PZ3003V
                        [descripcion] => PANTS COMPLETO
                        [talla] => UNT
                        [cantidad] => 1
                        [color] => VERDE
                        [no_color] => rgb(92, 146, 115)
                        [precio_etiqueta] => 439
                        [precio_vendedora] => 372
                        [precio_socia] => 366
                        [precio_empresaria] => 360
                        [precio_inversionista] => 342
                        [imagen_permanente] => fotos/PZ3003-VERDE-1.jpg
                        [producto_confirmado] => false
                        [estado_producto] => CREDITO
                        [seguimiento_producto] => Producto sin confirmar
                        [almacen] => 
                    )
         */       

        $datos_cliente = $this->clientes_consultarClienteConFechaCierre($numeroCuenta);     
        
        //print_r($datos_cliente);
        /*
         Array
            (
                [nombre] => MONICA HERNANDEZ GARCIA
                [zona] => EL PALMITO
                [fecha] => 25-08-2020                   Es la fecha de visita siguiente a menos que se cree una fecha futura como el cachorrito sabe hacerlo xD
                [grado] => VENDEDORA
                [credito] => 1700
                [dias] => 14
                [puntos_disponibles] => 100
            )
         */
       
        
        $porcentaje_financiacion = $this->usuarios_consultar_porcentaje_financiacion($numeroCuenta);
        $porcentaje_empresa = $porcentaje_financiacion["porcentaje_apoyo_empresa"];             //lo que financiara la emrpesa del total del credito 0.6  = 60%
        $porcentaje_cliente = $porcentaje_financiacion["porcentaje_pago_cliente"];              //lo que el cliente debera pagar al recibir su producto a credito 0.4 = 40%
        /*
         * Array
            (
                [porcentaje_apoyo_empresa] => 0.5
                [porcentaje_pago_cliente] => 0.5
            )
         */

        $suma_cantidad = 0;
        $suma_cantidad_credito = 0;                                             //la cantidad de piezas en credito
        $suma_cantidad_inversion = 0;                                           //la cantidad de piezas en inversion
        $suma_productos_etiqueta = 0;                                           //la suma del costo en etiqueta
        $suma_productos_credito = 0;                                            //el importe de los produictos a credito
        $suma_productos_inversion = 0;                                          //el importe de productos de inversion
        $suma_ganancia_cliente = 0;                                       //la ganancia del cliente por todo el pedido, dependiendo de su grado, y de cuantos productos tiene en inversion
        $suma_ganancia_todo_inversion = 0;                                  //calcularemos la ganancia de todos los productos sin importar credito, esto para crearle un mensaje donde diga Si cambiaras todo tu pedido a inversion ganarias $550 en lugar de 400
        $credito_cliente = $datos_pedido[0]['credito_cliente'];
        $nombre_cliente = $datos_pedido[0]['nombre_cliente'];
        $grado_cliente = $datos_pedido[0]['grado_cliente'];
        $fecha_entrega = $datos_pedido[0]['fecha_entrega_pedido'];
        $zona = $datos_pedido[0]['ruta'];
        $sucursal = $this->sucursal_consultarSucursal($zona);
        $asignado = $this->sucursal_consultarAsignado($zona);

        $dias_credito = $datos_cliente["dias"];
        
       $contadorProductosSinConfirmar = 0;




        

        foreach ($datos_pedido as $producto) {

            $cantidad = $producto['cantidad'];
            $precio_etiqueta = $producto['precio_etiqueta'];
            $precio_vendedora = $producto['precio_vendedora'];
            $precio_socia = $producto['precio_socia'];
            $precio_empresaria = $producto['precio_empresaria'];
            $precio_inversionista = $producto['precio_inversionista'];
            $grado_cliente = $producto['grado_cliente'];

            if ($producto['estado_producto'] == 'AGOTADO') {
                //si el producto esta clasificado como agotado asignamos en 0 y no se sumara a ningun total
                $cantidad = 0;
                $precio_vendedora = 0;
                $precio_socia = 0;
                $precio_empresaria = 0;
            }
            
            
            $suma_cantidad += $cantidad;
            $importe_temporal_precio_etiqueta = $cantidad * $precio_etiqueta;           //en cada producto pueden ser 1 o 2 o 3 piezas, este total se le quitara el precio de distribucion para conocer la ganancia del cliente

            $suma_productos_etiqueta += $importe_temporal_precio_etiqueta;
            
            
            

            if ($producto['estado_producto'] == 'CREDITO') {

                $suma_cantidad_credito += $cantidad;                

                //dependiendo de que grado sea el cliente, calcularemos el total de su importe a credito, e iremos sumando tambien su ganancia
                
                switch ($grado_cliente) {
                    case 'VENDEDORA':
                        
                        $importe_temporal_precio_dis = $cantidad * $precio_vendedora;
                        $suma_productos_credito += $importe_temporal_precio_dis;
                        $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
                        break;
                    case 'SOCIA':
                        
                        $importe_temporal_precio_dis = $cantidad * $precio_socia;
                        $suma_productos_credito += $importe_temporal_precio_dis;                      
                        $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
                        break;
                    case 'EMPRESARIA':
                        $importe_temporal_precio_dis = $cantidad * $precio_empresaria;
                        $suma_productos_credito += $importe_temporal_precio_dis;                       
                        $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
                        break;
                    default:
                        $suma_productos_credito = 0;
                        break;
                }
            }
            
            
            

            if ($producto['estado_producto'] == 'INVERSION') {
                $suma_cantidad_inversion += $cantidad;
                
                $importe_temporal_precio_dis = $precio_inversionista * $cantidad;
                $suma_productos_inversion += $importe_temporal_precio_dis;        
                
                $suma_ganancia_cliente += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
            }
            
            //sumamos todo producto sin importar sea credito o inversion al total de inversion esto para resolver lo del mensaje para motivar al cleinte a cambiarse a inversion
            $importe_temporal_precio_dis = $precio_inversionista * $cantidad;
            $suma_ganancia_todo_inversion += $importe_temporal_precio_etiqueta - $importe_temporal_precio_dis;
            
          
        }
        
        
        
        // Calculamos si su ccredito a sobrepasado su limite de credito y en base a ello le enviaremos mensajes determinados      
        
        $diferencia_credito = $suma_productos_credito - $credito_cliente;

        if ($diferencia_credito < 0) {     
            // si el valor es negativo significa que el cliente NO ha sobre pasado su limite de credito
            $temporal = $diferencia_credito *- 1;
            $mensaje_diferencia_credito = "Aun dispones de $$temporal en tu credito Kaliope";

        }else if($diferencia_credito > 0){
            //si el importe en credito ha rebasado el limite de credito del cliente obtendremos un valor positivo en la diferencia de credito
            
            if($diferencia_credito<=100){
                $mensaje_diferencia_credito = "Tu total de credito ha pasado por $$diferencia_credito tu limite de credito, le recomendamos cambiar alguna pieza a Inversion o elimine productos";
            }else if($diferencia_credito>100){
                $mensaje_diferencia_credito = "Tu total de credito ha pasado por $$diferencia_credito tu limite de credito, deberas dar esa diferencia en efectivo al recibir tu paquete, tambien puedes cambiar un producto a Inversion o eliminar productos";
            }
        }
        
        
        
         
        
     
        
        
        
        //calculamos la cantidad que el cliente debe de pagar acorde a su porcentarje de financiacion REDONDEAMOS los numeros ya que en ocaciones saca 525.99999999542
        /*
         * Tenemos una manera erronea de calcular esto, pasa lo siguiente si el el cliente rebalsa su limite por ejemplo su credito es de 2400
         * y el cliente agrega 3102 en credito la diferencia de credito sera 702 que el cliente debera cubrir
         * ahora el financiamiento del cliente por ejemplo el 70% sera de 3102*.7=2171.5 quedando pendientes 930.6
         * el sistema le dira que debe de pagar 2171.5+702 = 2873, esto ya no es el 70% y ademas se supne que pagara el exceso que son 702 por lo tanto ya no deberian quedar 930.6 financiados
         * 
         * para corregir esto haremos lo siguiente, si el limite de credito no fue revalsado todo se queda tal cual esta bien calculado
         * pero si el limite de credito fue revalsado entonces ahora en lugar de multiplicar el total del credito por el 70% solo vamos a multiplicar
         * el limite de credito actual. es decir tendra que pagar el 70% de sus 2400, son 1680 quedando un restante de 2400*30% = 720 para pagarce en 15 dias
         * y ahora si tendra que generar un pago por su diferencia de 702 entonces el cliente ahora si tendra el 70% y las sumas concuerdan
         *      2400*70% = 1680 
         * +    2400*30% = 720
         * +    702 diferencia credito
         * _______________________________
         * 3102 que es el total de la mercancia a credito
         */
        if($diferencia_credito<=0){
            //si no hay diferencia de credito o el credito no ha sido revalsado dejamos todo el calculo por el total del credito
            $cantidadPagarClienteCredito = round(($suma_productos_credito*$porcentaje_cliente),0,PHP_ROUND_HALF_DOWN) ;
            $cantidadFinanciarEmpresa = round(($suma_productos_credito*$porcentaje_empresa),0,PHP_ROUND_HALF_DOWN) ;
        }else{
            //si el credito ya fue revalsado es decir tiene valores positivos usamos el limite de credito del cliente
            $cantidadPagarClienteCredito = round(($credito_cliente*$porcentaje_cliente),0,PHP_ROUND_HALF_DOWN) ;
            $cantidadFinanciarEmpresa = round(($credito_cliente*$porcentaje_empresa),0,PHP_ROUND_HALF_DOWN) ;
        }
        
        //calculamos el total de pago que el cliente debe dar al recibir el pedido cuidado porque la diferencia de credito sale en negativo si el cliente tiene aun credito disponible
        //entonces solo deberemos sumarlo si el valor es mayor a 0 es decir sobrepaso su limite de credito
        if($diferencia_credito>0){
            $pagoAlRecibir = $suma_productos_inversion + $diferencia_credito + $cantidadPagarClienteCredito;
        }else{
            $pagoAlRecibir = $suma_productos_inversion + $cantidadPagarClienteCredito;
        }
        
        //calculamos la fecha de vencimiendo del credito con la fecha de entrega del pedido sumando los dias de credito
        $fechaPagoDelCredito = utilitarios_Class::sumarDiasFecha($fecha_entrega,$dias_credito);
       
        


        $respuesta = [
            'nombre' => $nombre_cliente,
            'cuenta' => $numeroCuenta,
            'limite_credito' => $credito_cliente,
            'grado' => $grado_cliente,
            'dias' => $dias_credito,
            'ruta' => $zona,
            'sucursal'=>$sucursal,
            'asignado'=>$asignado,
            'porcentaje_apoyo_empresa' => $porcentaje_empresa,
            'porcentaje_pago_cliente' => $porcentaje_cliente,
            'fecha_entrega' => $fecha_entrega,
            'fecha_pago_del_credito' => $fechaPagoDelCredito,
            'suma_cantidad' => $suma_cantidad,
            'suma_credito' => $suma_cantidad_credito,
            'suma_inversion' => $suma_cantidad_inversion,
            'cantidad_sin_confirmar' => $contadorProductosSinConfirmar,
            'suma_productos_etiqueta' => $suma_productos_etiqueta,
            'suma_productos_inversion' => $suma_productos_inversion,
            'suma_productos_credito' => $suma_productos_credito,
            'suma_ganancia_cliente' => $suma_ganancia_cliente,
            'diferencia_credito' => $diferencia_credito,
            'cantidad_pagar_cliente_credito' => $cantidadPagarClienteCredito,
            'cantidad_financiar_empresa' => $cantidadFinanciarEmpresa,
            'pago_al_recibir' => $pagoAlRecibir,
            'mensaje_diferencia_credito' => $mensaje_diferencia_credito,            
        ];

        //print_r($respuesta);
        /*
         * Array
        (
            [nombre] => MONICA HERNANDEZ GARCIA
            [cuenta] => 4926
            [limite_credito] => 1400
            [grado] => VENDEDORA
            [dias] => 14
            [ruta] => EL PALMITO
            [porcentaje_apoyo_empresa] => 0.35
            [porcentaje_pago_cliente] => 0.65
            [numero_pedido] => 1
            [fecha_entrega] => 2021-05-15
            [suma_cantidad] => 8
            [suma_credito] => 6
            [suma_inversion] => 2
            [cantidad_sin_confirmar] => 3
            [suma_productos_etiqueta] => 2822
            [suma_productos_inversion] => 560
            [suma_productos_credito] => 1800
            [suma_ganancia_cliente] => 462
            [diferencia_credito] => 400
            [cantidad_pagar_cliente_credito] => 1170
            [pago_al_recibir] => 2130
            [mensaje_diferencia_credito] => Tu total de credito ha pasado por $400 tu limite de credito, deberas dar esa diferencia en efectivo al recibir tu paquete, tambien puedes cambiar un producto a Inversion o eliminar productos
                    )
         */
        return $respuesta;
    }

}
