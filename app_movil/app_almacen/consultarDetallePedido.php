<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



include_once './datos_almacen_class.php';
$dataBase = new datos_almacen_class();

$numeroCuenta = filter_var($_REQUEST["NUMERO_CUENTA"], FILTER_SANITIZE_STRING);
$fechaPedido = filter_var($_REQUEST["FECHA_PEDIDO"], FILTER_SANITIZE_STRING);

$totales = $dataBase->carrito_calcularTot($numeroCuenta, $fechaPedido);

$respuesta["totales"] = $totales;
$respuesta["detalles"] = $dataBase->consultarDetallePedido($numeroCuenta, $fechaPedido);
$respuesta["mensajesFinalTotales"] = utilitarios_Class::crearMensajesBrevesSoloTotalesFinales($totales);

echo json_encode($respuesta, true);
