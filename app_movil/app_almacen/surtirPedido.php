<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once './datos_almacen_class.php';
$dataBase = new datos_almacen_class();

$numeroCuenta = filter_var($_REQUEST["NUMERO_CUENTA"], FILTER_SANITIZE_STRING);
$fechaPedido = filter_var($_REQUEST["FECHA_PEDIDO"], FILTER_SANITIZE_STRING);

if($dataBase->surtirPedido($numeroCuenta, $fechaPedido)){
    $respuesta["estatus"]="EXITO";
    $respuesta["mensaje"]="El pedido ha sido surtido exitosamente";
    
    echo json_encode($respuesta, true);
}else{
    $respuesta["estatus"]="FAIL";
    $respuesta["mensaje"]="El pedido no se ha podido surtir";
    echo json_encode($respuesta, true);
}

