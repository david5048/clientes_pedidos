<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once './datos_almacen_class.php';
include_once '../2.0/utilitarios_Class.php';
header('Content-type: application/json; charset=utf-8');
$dataBase = new datos_almacen_class();

$filtro = filter_var($_REQUEST["FILTRO"], FILTER_SANITIZE_STRING);


if($filtro == "CONFIRMADOS"){
    $resultado["pedidos"]= $dataBase->pedidos_por_surtir();    
}

if($filtro == "SIN_CONFIRMAR"){
    $resultado["pedidos"] = $dataBase->carritos_sin_confirmar();
}

if($filtro == "SURTIDOS"){
    $resultado["pedidos"] = $dataBase->surtidos_y_enviados2();
}




echo json_encode($resultado, true);

